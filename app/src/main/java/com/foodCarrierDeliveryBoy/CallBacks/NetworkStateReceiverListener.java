package com.foodCarrierDeliveryBoy.CallBacks;

public interface NetworkStateReceiverListener {

    void onInternetConnectionEstablished();

    void onInternetConnectionNotFound();


}
