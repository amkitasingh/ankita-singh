package com.foodCarrierDeliveryBoy.CallBacks;

public interface LogoutCallBacks {

    void onLogOutSuccess();

    void onLogOutError(String message);
}
