package com.foodCarrierDeliveryBoy.CallBacks;

import com.google.android.gms.maps.model.LatLng;


public interface RideStatusCallbacks {

    void onDriverAssigned();

    void onDriverReached();

    void onRideStarted();

    void onRideCompleted(String totalCost);

    void onRideCancelledByDriver();

    void onStatusUnknown();

    void onDriverLocationUpdated(LatLng driverCurrentLocation);

    void onTrackError(String errorMessage);

    void onReferEarn();

    void onRideTransferred();

    void onRideExtended(String rideID);
}
