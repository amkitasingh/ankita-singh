package com.foodCarrierDeliveryBoy.CallBacks;

public interface GPSCallBack {

    void onGPSFound();

    void onGPSnotFound();
}
