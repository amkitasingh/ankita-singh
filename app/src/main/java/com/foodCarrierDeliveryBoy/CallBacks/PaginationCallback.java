package com.foodCarrierDeliveryBoy.CallBacks;


public interface PaginationCallback{
    public void loadNextPage();
}