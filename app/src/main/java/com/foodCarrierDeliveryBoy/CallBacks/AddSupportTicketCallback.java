package com.foodCarrierDeliveryBoy.CallBacks;


import com.foodCarrierDeliveryBoy.Models.AddTicketModel;

public interface AddSupportTicketCallback {
    void onLoadSuccess(AddTicketModel result);
    void onError(String s);
}