package com.foodCarrierDeliveryBoy.FirebaseNotification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;


public class NotificationsBroadCast extends BroadcastReceiver {
    private NotificationCallBacks callBacks;
    public static final int ORDER_CREATED = -1;
    public static final int ORDER_MERCHANT_CANCEL= -2;
    public static final int ORDER_MERCHANT_ACCEPT = -3;
    public  static final int ORDER_REQUEST=-4;
    public static final int ORDER_REJECT=-5;
    public static final int ORDER_CANCEL=-6;

    Bundle notificationBundle;
    String notificationType;
    String orderID;
    String title, body;

    @Override
    public void onReceive(Context context, Intent intent) {
        notificationBundle = intent.getExtras();
        if (notificationBundle != null) {
            notificationType = notificationBundle.getString("notification_type");
            orderID = notificationBundle.getString("order_id");
            title = notificationBundle.getString("title");
            body = notificationBundle.getString("body");
            Log.d("Notification", "onReceive: "+notificationBundle +"\n"+notificationType+"\n"+orderID);

            if (callBacks != null && !ValidationHelper.isNull(notificationType)) {
                switch (notificationType) {
                    case "order_created":
                        notifyState(callBacks, ORDER_CREATED, orderID, title);
                        break;

                    case "order_merchant_cancel":
                        notifyState(callBacks, ORDER_MERCHANT_CANCEL, orderID, title);
                        break;

                    case "order_merchant_accept":
                        notifyState(callBacks, ORDER_MERCHANT_ACCEPT, orderID, title);
                        break;

                    case "order_request":
                        notifyState(callBacks, ORDER_REQUEST, orderID, title);
                        break;

                    default:
                        break;
                }
            }
        }
    }
    private void notifyState(NotificationCallBacks callBack, int notificationType, String orderID, String titile) {
        switch (notificationType) {

            case ORDER_CREATED:
                callBack.onOrderCreated(orderID,titile);
                return;
            case ORDER_MERCHANT_CANCEL:
                callBack.onOrderMerchantCancel(orderID, title);
                break;

            case ORDER_MERCHANT_ACCEPT:
                callBack.onOrderMerchantAccept(orderID, titile);
                break;

            case  ORDER_REQUEST:
                callBack.onOrderRequest(orderID, title);
                break;

            default:
                break;
        }
    }



    public void removeListener() {
        this.callBacks = null;
    }

    public void addListener(NotificationCallBacks callback) {
        this.callBacks =callBacks;

    }

    public interface NotificationCallBacks {

        void onOrderCreated(String orderID,String title);

        void onOrderMerchantCancel(String orderID,String title);

        void onOrderMerchantAccept(String orderID,String title);

        void onOrderRequest(String orderID, String title);
    }

}
