package com.foodCarrierDeliveryBoy.FirebaseNotification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.foodCarrierDeliveryBoy.Activity.ActivityDashboard;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;

import java.util.Map;


/**
 * Created by tecorb on 20/3/17.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String CHANNEL_ID = "com.chareotDelivery";
    CharSequence name = "Product";
    String description = "Notifications";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.d("notii",s);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override

    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);



       sendNotification(remoteMessage);
    }

    public void sendNotification(RemoteMessage remoteMessage){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;

            Intent notificationIntent = new Intent(AppConstants.FILTER_NOTIFICATION_BROADCAST);
            Map<String, String> nData = remoteMessage.getData();
            if (nData != null && nData.size() > 0) {
                Log.d("notiii","greateerr");
                Log.d("Notification", "From: " + nData.toString());
                for (String key : nData.keySet()) {
                    notificationIntent.putExtra(key, nData.get(key));
                }
                Log.d("notiii","notii");
                sendBroadcast(notificationIntent);
            }

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            if (notificationManager!=null)
                notificationManager.createNotificationChannel(mChannel);

            Intent intent = new Intent(this, ActivityDashboard.class);
            intent.putExtra("notification_type", remoteMessage.getData().get("notification_type"));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,CHANNEL_ID);
            mBuilder.setSmallIcon(R.drawable.notification_icon);
            if (remoteMessage.getNotification() != null) {
                mBuilder.setContentTitle(remoteMessage.getNotification().getTitle());
                mBuilder.setContentText(remoteMessage.getNotification().getBody());
            }else {
                mBuilder.setContentTitle(remoteMessage.getData().get("title"));
                mBuilder.setContentText(remoteMessage.getData().get("body"));
            }
            mBuilder.setColor(getColor(R.color.orange));
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setCategory(NotificationCompat.CATEGORY_PROMO);
            mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            mBuilder.setSound(defaultSoundUri);
            mBuilder.setAutoCancel(true);

            //NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            // notificationId is a unique int for each notification that you must define
            notificationManager.notify((int) SystemClock.currentThreadTimeMillis(), mBuilder.build());

        } else {

            Intent notificationIntent = new Intent(AppConstants.FILTER_NOTIFICATION_BROADCAST);
            Map<String, String> nData = remoteMessage.getData();
            if (nData != null && nData.size() > 0) {
                Log.d("Notification", "From: " + nData.toString());
                for (String key : nData.keySet()) {
                    notificationIntent.putExtra(key, nData.get(key));
                }
                Log.d("notiii","smaller");
                sendBroadcast(notificationIntent);
            }

            // Create an explicit intent for an Activity in your app
            Intent intent = new Intent(this, ActivityDashboard.class);
            intent.putExtra("notification_type", remoteMessage.getData().get("notification_type"));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            if (remoteMessage.getNotification() != null) {
                mBuilder.setContentTitle(remoteMessage.getNotification().getTitle());
                mBuilder.setContentText(remoteMessage.getNotification().getBody());
            }else {
                mBuilder.setContentTitle(remoteMessage.getData().get("title"));
                mBuilder.setContentText(remoteMessage.getData().get("body"));
            }
            mBuilder.setColor(getResources().getColor(R.color.orange));
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setCategory(NotificationCompat.CATEGORY_PROMO);
            mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            mBuilder.setSound(defaultSoundUri);
            mBuilder.setAutoCancel(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            // notificationId is a unique int for each notification that you must define
            notificationManager.notify((int) SystemClock.currentThreadTimeMillis(), mBuilder.build());
        }


    }

}