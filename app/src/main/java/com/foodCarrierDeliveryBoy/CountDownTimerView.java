package com.foodCarrierDeliveryBoy;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.CountDownTimer;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;


import org.apache.commons.lang3.time.DurationFormatUtils;


public class CountDownTimerView extends AppCompatTextView {

    private int maxTime, interval; //in seconds

    private Callbacks timerCallBacks;

    private CountDownTimer countDownTimer;

    public void startTimer(){
        if(countDownTimer!=null) {
            countDownTimer.cancel();
            countDownTimer.start();
            if(timerCallBacks!=null)timerCallBacks.onCountDownStart();
        }
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public CountDownTimerView(Context context) {
        this(context, null);
    }

    public CountDownTimerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CountDownTimerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.CountDownTimerView);
        maxTime = array.getInt(R.styleable.CountDownTimerView_maxTimeInSeconds, 60);
        interval = array.getInt(R.styleable.CountDownTimerView_intervalInSeconds, 1);

        array.recycle();

        if(context instanceof Callbacks){
            timerCallBacks = (Callbacks)context;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(countDownTimer==null)initTimer();
    }

    void initTimer(){

        countDownTimer = new CountDownTimer(maxTime*1000, interval*1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                try{
                    String remainingTime = DurationFormatUtils.formatDuration(millisUntilFinished,
                            "mm:ss");
                    setText(remainingTime);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                setText(DurationFormatUtils.formatDuration(0,
                        "mm:ss"));
                if(timerCallBacks!=null)timerCallBacks.onCountDownFinish();
            }
        };
    }

    public interface Callbacks{

        void onCountDownFinish();

        void onCountDownStart();
    }
}
