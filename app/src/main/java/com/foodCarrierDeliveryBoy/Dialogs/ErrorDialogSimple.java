package com.foodCarrierDeliveryBoy.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.foodCarrierDeliveryBoy.R;


public class ErrorDialogSimple {
    public static AlertDialog show(Context context, String message) {
        Activity activity = (Activity) context;
        if (activity == null || activity.isFinishing()) return null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        builder.setCancelable(false);
        builder.setTitle(R.string.app_name);
        if (message!=null) {
            builder.setMessage(message);
        }else {
            builder.setMessage("Something s went wrong");
        }
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.show();
    }
}