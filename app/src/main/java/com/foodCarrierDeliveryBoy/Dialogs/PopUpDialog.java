package com.foodCarrierDeliveryBoy.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.CallBacks.BaseCallBacks;
import com.foodCarrierDeliveryBoy.Models.ModelOrderLocDetail;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;

import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.GPSTracker;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.PopUpDialogBinding;

import java.util.HashMap;


public class PopUpDialog extends BaseActivity {


    private PopUpDialogBinding binding;
    private Context context;
    private Dialog dialog;

    int counter;
    int progress;

    private ModelOrderLocDetail model;
    private Callback callback;
    private GPSTracker gpsTracker;
    private String orderId;

    CountDownTimer countDownTimer;



    public PopUpDialog(Context context, ModelOrderLocDetail model, Callback callback) {

        this.context = context;
        gpsTracker = new GPSTracker(context);
        this.model = model;
        this.callback = callback;



    /*   Button endYes = (Button) dialog.findViewById(R.id.end_yes);
        endYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AmountAlertDialog();

            }
        });*/
        showImage();

    }

    private void showImage() {
        dialog = new Dialog(context);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.pop_up_dialog, null, false);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.R.style.Theme_Translucent_NoTitleBar_Fullscreen));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (screenWidth - (screenWidth / 100));//To make the dialog fill up_to 90% of the screenwidth.
        lp.height = (screenHeight - (screenHeight / 100));

        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
        orderDetail();

        binding.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (callback != null) {
                    // callback.onClickonAcceptOrder("popUpDialog");
                    hitAcceptOrder();
                    dialog.cancel();
                }

            }
        });
        /*  binding.rejectButton.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                if (active) {
                    callback.onClickonRejectOrder("home");
                    dialog.cancel();
                }
            }
        });*/
        // binding.progressbar.setMaxProgress(20);
        countDounTimer();

        binding.buttonRight.setOnTouchListener(new OnSwipeTouchListener(context) {


            public void onSwipeRight() {
                // hitAcceptOrder();

                if (callback != null) {
                    // callback.onClickonAcceptOrder("popUpDialog");

                    hitAcceptOrder();
                    dialog.cancel();

                }
            }

            public void onSwipeLeft() {
                super.onSwipeLeft();
                hitRejectOrder();
                 dialog.cancel();
                //  new PopUpStackOrderDialog(context,this);

                // startActivity(IntentHelper.getOrderScreen(context));

            }
        });

    }
    void orderDetail() {

       /* binding.userName.setText(ValidationHelper.optional(model.getBranch().getDelivery_time()));
        binding.estTIme.setText(ValidationHelper.optional(model.getBranch().getRestaurant_name()));
        binding.tvAddress.setText(ValidationHelper.optional(model.getBranch().getAddress()));


        binding.estDistance.setText(ValidationHelper.optional(model.getOrder().getDelivery_time()));
        binding.tvAddressDetail.setText(ValidationHelper.optional(model.getOrder().getAddress_type()));
        binding.estAddressName.setText(ValidationHelper.optional(model.getOrder().getBuilding()));
        binding.tvCost.setText(ValidationHelper.optional(String.valueOf(model.getOrder().getDelivery_amount())));
*/

    }

    void hitAcceptOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderId);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ACCEPT_ORDER),
                UrlFactory.getDefaultHeaders(), (BaseCallBacks) callback);
    }

    void hitRejectOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderId);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_REJECT_ORDER),
                UrlFactory.getDefaultHeaders(), (BaseCallBacks) callback);
    }

    private void close() {
        try {
            if (dialog != null && dialog.isShowing()) dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(int viewId, View view) {

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        dismissLoader();
        if (responseObj instanceof ModelSuccess) {
            ModelSuccess ModelAccept = (ModelSuccess) responseObj;
            if (ModelAccept.getCode() == 200) {
                startActivity(IntentHelper.getOrderScreen(context));

            }

        } else if (responseObj instanceof ModelSuccess) {
            ModelSuccess ModelReject = (ModelSuccess) responseObj;
            if (ModelReject.getCode() == 200) {
                startActivity(IntentHelper.getDashboard(context));

            }
        }

    }

    @Override
    public void onOrderRequest(String orderID, String title) {

    }

    public interface Callback {

        void onClickonAcceptOrder(String fromScreen);

        void onClickonRejectOrder(String fromScreen);
    }


    void countDounTimer() {
        counter = 20;
        progress = 0;
        countDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {

                progress = progress + 1;
                binding.progressTimeTV.setText(counter + "s Left to Accept");
                binding.progressbar.setProgress(progress);
                counter--;
            }

            @Override
            public void onFinish() {
             /*   binding.tvButtonResendOtp.setText(getResources().getString(R.string.send_otp));
                binding.otpCountDownTimer.setVisibility(View.GONE);
                binding.tvButtonResendOtp.setVisibility(View.VISIBLE);*/
                dialog.cancel();
            }

        }.start();
    }


    static class OnSwipeTouchListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener(Context ctx) {
            gestureDetector = new GestureDetector(ctx, new GestureListener());
        }
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }



        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 300;
            private static final int SWIPE_VELOCITY_THRESHOLD = 300;


            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                            result = true;
                        }
                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        result = true;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }
        public void onSwipeRight() {
        }

        public void onSwipeLeft() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }
    }
}