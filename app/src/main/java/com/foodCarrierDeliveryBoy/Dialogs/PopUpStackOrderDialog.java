package com.foodCarrierDeliveryBoy.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Models.ModelOrderLocDetail;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.PopUpStackOrderDialogBinding;

import java.util.HashMap;

public class PopUpStackOrderDialog extends BaseActivity {
    private Context context;
    CountDownTimer countDownTimer;
    int counter, progress;
    private Callback callback;
    private Dialog dialog;
    private String orderId;
    ModelOrderLocDetail   modelOrder;
    PopUpStackOrderDialogBinding binding;


    public PopUpStackOrderDialog(Context context, Callback callback) {
        this.context = context;
        this.callback = callback;
      /*  Button endYes = (Button) dialog.findViewById(R.id.end_yes);
        endYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AmountAlertDialog();

            }
        });*/
        showImage();


    }


    private void showImage() {
        dialog = new Dialog(context);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.pop_up_stack_order_dialog, null, false);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        dialog.setContentView(binding.getRoot());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.R.style.Theme_Translucent_NoTitleBar_Fullscreen));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (screenWidth - (screenWidth / 100));//To make the dialog fill up_to 90% of the screenwidth.
        lp.height = (screenHeight - (screenHeight / 100));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        countDounTimer();
        apiHitOrderDetail(orderId);

        binding.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hitAcceptOrder();
            }
        });


        binding.buttonRight.setOnTouchListener(new OnSwipeTouchListener(context) {

            public void onSwipeRight() {
                hitAcceptOrder();
                Toast.makeText(context, "Right Swipe", Toast.LENGTH_SHORT).show();

            }

            public void onSwipeLeft() {
                hitRejectOrder();
                Toast.makeText(context, "Left Swipe", Toast.LENGTH_SHORT).show();


            }
        });

    }


    void hitAcceptOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderId);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ACCEPT_ORDER),
                UrlFactory.getAppHeaders(), this);
    }

    void hitRejectOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderId);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_REJECT_ORDER),
                UrlFactory.getAppHeaders(), this);
    }

    private void orderDetail() {
        binding.ViewName.setText(ValidationHelper.optional(modelOrder.getBranch().getRestaurant_name()));
        binding.tvAddress.setText(ValidationHelper.optional(modelOrder.getBranch().getAddress()));
        binding.estDistance.setText(ValidationHelper.optional(modelOrder.getOrder().getDelivery_time()));
        binding.tvTime2.setText(ValidationHelper.optional(String.valueOf(modelOrder.getOrder().getDelivery_amount())));
    }

    void apiHitOrderDetail(String orderId) {
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderId);
        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ORDER_LOC_DETAIL),
                UrlFactory.getAppHeaders(), this);
    }
    @Override
    public void onClick(int viewId, View view) {

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        dismissLoader();
        if (responseObj instanceof ModelSuccess) {
            ModelSuccess ModelAccept = (ModelSuccess) responseObj;
            if (ModelAccept.getCode() == 200) {
                startActivity(IntentHelper.getOrderScreen(context));
            }

        } else if (responseObj instanceof String) {
            Toast.makeText(context, "OrderRejected", Toast.LENGTH_SHORT).show();
            dialog.cancel();

        } else if (responseObj instanceof ModelOrderLocDetail) {
            modelOrder = (ModelOrderLocDetail) responseObj;
            Log.d("tag", "onTaskSuccessAddress: " + modelOrder.getBranch().getAddress());
            if (modelOrder.getCode() == 200) {
                orderDetail();

            }
        }

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }


    private void close() {
        try {
            if (dialog != null && dialog.isShowing()) dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onOrderRequest(String orderID, String title) {

    }

    public interface Callback {
        void onClickOnAcceptButton();
    }

    void countDounTimer() {
        counter = 20;
        progress = 0;

        countDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {

                progress = progress + 1;
                binding.progressTimeTV.setText(counter + "s Left to Accept");
                binding.progressbar.setProgress(progress);
                counter--;
            }

            @Override
            public void onFinish() {
             /*   binding.tvButtonResendOtp.setText(getResources().getString(R.string.send_otp));
                binding.otpCountDownTimer.setVisibility(View.GONE);
                binding.tvButtonResendOtp.setVisibility(View.VISIBLE);*/
                dialog.cancel();
            }

        }.start();
    }


    static class OnSwipeTouchListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener(Context ctx) {
            gestureDetector = new GestureDetector(ctx, new OnSwipeTouchListener.GestureListener());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }


        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 300;
            private static final int SWIPE_VELOCITY_THRESHOLD = 700;

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                            result = true;
                        }
                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        result = true;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }

        public void onSwipeRight() {
        }

        public void onSwipeLeft() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }
    }
}



