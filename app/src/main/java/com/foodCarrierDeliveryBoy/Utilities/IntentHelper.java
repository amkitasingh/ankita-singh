package com.foodCarrierDeliveryBoy.Utilities;

import android.content.Context;
import android.content.Intent;

import androidx.databinding.library.baseAdapters.BuildConfig;

import com.foodCarrierDeliveryBoy.Activity.ActivityAboutUs;
import com.foodCarrierDeliveryBoy.Activity.ActivityBankAccount;
import com.foodCarrierDeliveryBoy.Activity.ActivityContactUs;
import com.foodCarrierDeliveryBoy.Activity.ActivityDashboard;
import com.foodCarrierDeliveryBoy.Activity.ActivityEarnings;
import com.foodCarrierDeliveryBoy.Activity.ActivityEditProfile;
import com.foodCarrierDeliveryBoy.Activity.ActivityFAQ;
import com.foodCarrierDeliveryBoy.Activity.ActivityForgetPassword;
import com.foodCarrierDeliveryBoy.Activity.ActivityHelpCenter;
import com.foodCarrierDeliveryBoy.Activity.ActivityLogin;
import com.foodCarrierDeliveryBoy.Activity.ActivityModeOfTransport;
import com.foodCarrierDeliveryBoy.Activity.ActivityOrderLeftMenu;
import com.foodCarrierDeliveryBoy.Activity.ActivityOrdersDeliverd;
import com.foodCarrierDeliveryBoy.Activity.ActivityReportIssue;
import com.foodCarrierDeliveryBoy.Activity.ActivityResetPassword;
import com.foodCarrierDeliveryBoy.Activity.ActivityTicket;
import com.foodCarrierDeliveryBoy.Activity.ActivityViewOrder;
import com.foodCarrierDeliveryBoy.Activity.ActivityViewWebsite;
import com.foodCarrierDeliveryBoy.Fragments.FragmentOrderScreen;
import com.foodCarrierDeliveryBoy.Activity.ActivitySignUp;
import com.foodCarrierDeliveryBoy.Activity.ActivityVerification;
import com.foodCarrierDeliveryBoy.Fragments.FragmentPickUpOrder;
import com.foodCarrierDeliveryBoy.R;


public class IntentHelper {

    public static Intent getDashboard(Context context) {
        return new Intent(context, ActivityDashboard.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getLogin(Context context) {
        return new Intent(context, ActivityLogin.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getOtpVerification(Context context) {
        return new Intent(context, ActivityVerification.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getForgotPassword(Context context) {
        return new Intent(context, ActivityForgetPassword.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getResetPassword(Context context) {
        return new Intent(context, ActivityResetPassword.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getSignUp(Context context) {
        return new Intent(context, ActivitySignUp.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getEditProfile(Context context) {
        return new Intent(context, ActivityEditProfile.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    public static Intent getModeTransport(Context context) {
        return new Intent(context, ActivityModeOfTransport.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getOrderScreen(Context context) {
        return new Intent(context, FragmentOrderScreen.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getOrderDetailScreen(Context context) {
        return new Intent(context, ActivityViewOrder.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getPickOrderScreen(Context context) {
        return new Intent(context, FragmentPickUpOrder.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getDeliveredScreen(Context context) {
        return new Intent(context, ActivityOrdersDeliverd.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getEarningScreen(Context context) {
        return new Intent(context, ActivityEarnings.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getBankAccountScreen(Context context) {
        return new Intent(context, ActivityBankAccount.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }
    public static Intent getHelpScreen(Context context) {
        return new Intent(context, ActivityHelpCenter.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getFaqScreen(Context context) {
        return new Intent(context, ActivityFAQ.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getAllTicketScreen(Context context) {
        return new Intent(context, ActivityTicket.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getCreateTicketScreen(Context context) {
        return new Intent(context, ActivityReportIssue.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getOrderHistoryScreen(Context context) {
        return new Intent(context, ActivityOrderLeftMenu.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getAboutUsScreen(Context context) {
        return new Intent(context, ActivityAboutUs.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getContactUsScreen(Context context) {
        return new Intent(context, ActivityContactUs.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
    public static Intent getViewWebsite(Context context) {
        return new Intent(context, ActivityViewWebsite.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }




    public static void shareAPP(Context context, String description) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
            String shareMessage = description+"\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "Share Via"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
