package com.foodCarrierDeliveryBoy.Utilities;

public interface AppConstants {

    String FILTER_NOTIFICATIONS = "com.chareotdelivery";
    String LOCATION_NOTIFICATION_BROADCAST = "com.chareotdeliverylocation";
    String CHANNEL_ID = "com.example";
    String FILTER_NOTIFICATION_BROADCAST = "com.chareotdelivery";
    String CHANNEL_NAME = "Notification";
    String CHANNEL_DESCRIPTION = "Example Partner Notifications";

    String kResponseCode = "code";
    String kResponseMsg = "message";
    String kRESPONSE_ERROR = "errors";
    String kPage = "page";
    String kPerPage = "per_page";

    int kResponseOK = 200;
    int kResponseSignUp = 201;
    int kResponseNotMatch = 422;
    int kResponseNotFoundToken = 322;
    int kResponseUserUpdated = 205;
    int kResponseSessionExpire = 345;
    //int kResponseSessionExpire = 403;
    int kResponseDLNotExit = 425;
    int kResponseContactExist = 200;
    int kResponseContactNotExist = 404;
    int kResponsePendingRide = 303;
    int KResponseUserNotExist = 301;
    int kResponseUpdate = 101;
    int kResponseForceUpdate = 102;
    int kResponseNotAccess = 401;
    int kUpdateDevice = 205;
    int kRESPONSE_NO_TOKEN = 403;
    int KResponseUserAlreadyExist = 402;

    int KResponseRecordNotFound = 400;
    String kPayTabsSuccess_100="100";
    String kSourceToken = "source_token";
    String kIndividual = "individual";


    String URL_SIGN_IN = "session/login";
    String URL_FORGET_PASSWORD_TOKEN = "user/password/token";

    String URL_CREATE_GUEST="session/guest";
    String URL_LOGOUT = "session/logout";
    String URL_CONTACT_EXIST="user/contact/check";
    String URL_PASSWORD_RESET = "user/password/reset";
    String URL_SIGN_UP = "user/signup";



    String URL_ADD_VEHICLE ="transporter/create/vehicle" ;
    String URL_ALL_MODES ="transporter/vehicle/list" ;

    String URL_ONLINE_OFFLINE = "transporter/online/update";
    String URL_CHECK_ONLINE = "transporter/check/status";


    String URL_ORDER_LOC_DETAIL = "transporter/screen/order/details";
    String URL_ACCEPT_ORDER = "transporter/screen/order/update";
    String URL_VIEW_ORDER = "transporter/order/details";
    String URL_ORDER_DELIVERED = "transporter/order/delivered";
    String URL_ALL_ACCEPTED_ORDERS = "transporter/accepted/orders";
    String URL_PICK_UP_ORDER ="transporter/order/pickup";
    String URL_REJECT_ORDER="transporter/reject/order/request";
    String URL_CANCEL_ORDER="/transporter/cancel/order";
    String URL_TRANSPORTER_AT_RESTURANT="transporter/at/restaurant";


    String URL_FAQ ="faq/list" ;
    String URL_LOCATION_UPDATE="transporter/location/update";
    String URL_ALL_TICKET = "issue/tracker";
    String URL_ALL_MESSAGES_CHAT = "support/reply/list";
    String URL_SEND_MESSAGE_CHAT = "create/support/reply";

    String URL_EARNINGS ="transporter/today/earning";
    String URL_CHANGE_PASSWORD = "session/password/update";
    String URL_NOTIFICATION = "notifications/list";

    String URL_UPDATE_DEVICE_TOKEN = "session/device/update";
    String URL_CONTACT_US = "contact/us";

    String URL_SELECT_VEHICLE = "transporter/select/vehicles";
    String URL_CREATE_TRANSPORT = "transporter/created/vehicles";

    String URL_ORDER_HISTORY = "transporter/order/histories";
    String URL_UPDATE_PROFILE = "edit/profile";


    //*******************Sign Up key****************//


    String key_contact = "contact";
    String key_country_code = "country_code";
    String key_password = "password";
    String key_role = "role";
    String key_email="email";
    String key_input_role="transporter";
    String DEVICE_TYPE_INPUT="android";
    String DEVICE_TYPE="device_type";
    String DEVICE_ID="device_id";
    String key_per_page = "per_page";
    String key_page = "page";
    String key_is_contact_verified = "is_contact_verified";
    String key_is_contact_verified_input="true";
    String kGuestToken="guest_token";
    String LOGOUT_BY="logout_by";
    String SINGLE="single";
    String OTP="otp";


    String key_name = "name";
    String key_title="title";

    String key_password_token = "password_token";
    String key_old_password = "old_password";
    String key_new_password = "new_password";
    ;
    String Order_id = "order_id";

    String branch_id = "branch_id";

}
