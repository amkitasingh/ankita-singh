package com.foodCarrierDeliveryBoy.Utilities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.foodCarrierDeliveryBoy.Application.App;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class AppMethods {

    public static String getTimezone() {
        TimeZone tz = TimeZone.getDefault();
        return tz.getID() + "";
    }

    public static void hideKeyboard(View view) {
        Context context = App.getInstance();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard() {
        Context context = App.getInstance();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static Drawable getDrawable(int drawableId) {
        return ContextCompat.getDrawable(App.getInstance(), drawableId);
    }
/*
    public static void loadThumbnail(@NonNull ImageView driverImageView, String url) {
        Glide.with(driverImageView.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.logo_icon)
                        .error(R.drawable.logo_icon)
                        .transform(new CircleTransform()))
                .into(driverImageView);
    }*/

    public static void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(
                Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void showSneckbar(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public static String formatTime(String date, String outFormat) {
        if (date == null) return "n/a";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat(outFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }

    public static String getFormattedDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());

    }

    public static String getConversionDate(long time) {


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date(time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        return String.valueOf(formatter.format(date));


    }


    public static String getNewConversionDate(long time) {


        SimpleDateFormat formatter = new SimpleDateFormat("E, MMM dd yyyy");
        Date date = new Date(time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        return String.valueOf(formatter.format(date));


    }


    public static String getTime(long time) {

        SimpleDateFormat forApiTime = new SimpleDateFormat("hh:mm a");
        Date date1 = new Date(time);
        String selectedTime = forApiTime.format(date1);
        return selectedTime;


    }

    private static float dpFromPx(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }


    public static int getDrawerDragDistance(Context context) {
        int totalWidthInDp = (int) dpFromPx(context, context.getResources().getDisplayMetrics().widthPixels);
        double widthForContentView = totalWidthInDp * 0.35;
        return totalWidthInDp - (int) widthForContentView;
    }


}
