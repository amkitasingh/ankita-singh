package com.foodCarrierDeliveryBoy.Utilities;

import android.content.Context;
import android.content.SharedPreferences;


public class GuestPrefrenceManager {
    private static GuestPrefrenceManager instance;
    private static final String PREF_NAME = "ChileAppPRwe";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final String KEY_GUEST_TOKEN = "KEY_GUEST_TOKEN";


    private Context context;
    int PRIVATE_MODE = 0;

    public GuestPrefrenceManager(Context context) {


        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }


    public static GuestPrefrenceManager getInstance(Context context) {
        if (instance == null) {
            instance = new GuestPrefrenceManager(context);
        }
        return instance;
    }

    public void saveGuestToken(String guestToken){
        editor.putString(KEY_GUEST_TOKEN,guestToken);
        editor.apply();
        editor.commit();
    }

    public String getGuestToken(){
        return preferences.getString(KEY_GUEST_TOKEN,"");
    }




}



