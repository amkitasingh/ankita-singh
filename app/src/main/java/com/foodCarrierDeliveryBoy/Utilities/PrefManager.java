package com.foodCarrierDeliveryBoy.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.foodCarrierDeliveryBoy.Models.UserModel;

public class PrefManager {

    private static PrefManager instance;
    private static final String PREF_NAME = "ChileAppPR";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final String KEY_REMEMBER_ME = "remember_me";
    private static final String KEY_IS_LOGGED_IN = "is_Login";
    private static final String KEY_USER_DETAILS = "user";
    private static final String KEY_TRANSPORT_MODE = "bike";




    private Context context;
    int PRIVATE_MODE = 0;

    public PrefManager(Context context) {


        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }


    public static PrefManager getInstance(Context context) {
        if (instance == null) {
            instance = new PrefManager(context);
        }
        return instance;
    }

    public void setKeyIsLoggedIn(boolean value) {
        setBooleanValue(KEY_IS_LOGGED_IN, value);
    }

    public boolean getKeyIsLoggedIn() {
        return getBooleanValue(KEY_IS_LOGGED_IN);
    }

    public void setKeyRememberMe(boolean value) {
        setBooleanValue(KEY_REMEMBER_ME, value);
    }

    public boolean getKeyRememberMe() {
        return getBooleanValue(KEY_REMEMBER_ME);
    }

    public UserModel getUserDetail() {
        Gson gson = new Gson();
        String json = preferences.getString(KEY_USER_DETAILS, "");
        return gson.fromJson(json, UserModel.class);
    }

    public void setUserDetail(UserModel user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(KEY_USER_DETAILS, json);
        editor.commit();
    }

    public void setUserLogin(boolean isSignedIn) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isSignedIn);
        editor.commit();
    }


    private boolean getBooleanValue(String key) {
        return this.preferences.getBoolean(key, false);
    }

    private void setBooleanValue(String key, boolean value) {
        this.editor.putBoolean(key, value);
        this.editor.commit();
    }

    public String getStringValue(String key) {
        return this.preferences.getString(key, "");
    }

    public void setStringValue(String key, String value) {
        this.editor.putString(key, value);
        this.editor.commit();
    }


    private int getIntValue(String key) {
        return this.preferences.getInt(key, 0);
    }

    private void setIntValue(String key, int value) {
        this.editor.putInt(key, value);
        this.editor.commit();
    }

    private long getLongValue(String key) {
        return this.preferences.getLong(key, 0);
    }

    private void setLongValue(String key, long value) {
        this.editor.putLong(key, value);
        this.editor.commit();
    }

    public void setLatitudeValue(String key, String value) {
        this.editor.putString(key, value);
        this.editor.commit();
    }

    public String getLattitudeValue(String key) {
        return this.preferences.getString(key, null);
    }

    public void setLongitudeValue(String key, String value) {
        this.editor.putString(key, value);
        this.editor.commit();
    }

    public String getLongitudeValue(String key) {
        return this.preferences.getString(key, null);
    }

    public void clearPrefs() {
        this.editor.clear();
        this.editor.commit();
    }

    public void removeFromPreference(String key) {
        this.editor.remove(key);
        this.editor.commit();
    }
    public void setSecurityToken(String value) {
        setStringValue(KEY_USER_DETAILS, value);
    }


    public String getSecurityToken() {
        return getStringValue(KEY_USER_DETAILS);
    }

    public void setModeOfTransport(String value) {
        setStringValue(KEY_TRANSPORT_MODE, value);
    }


    public String getModeOfTransport() {
        return getStringValue(KEY_TRANSPORT_MODE);
    }

}
