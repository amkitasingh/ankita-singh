package com.foodCarrierDeliveryBoy.Utilities;



import androidx.databinding.library.baseAdapters.BuildConfig;

import com.foodCarrierDeliveryBoy.Application.App;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.TimeZone;



public class UrlFactory {

    private static final String URL_DEV = "http://45.58.45.156:4100/";    // Development
    private static final String URL_PROD = "http://18.141.218.46:3000/";   // Live
    private static final String API_VERSION = "api/v0/";
    private static String fireBaseDataBase_Development_url="https://tecch-6591c-default-rtdb.firebaseio.com/";
    private static String fireBaseDataBase_live_url="https://chareot-d47c5.firebaseio.com";


    private static final boolean MODE_DEVELOPMENT = true;

    public static boolean isModeDevelopment() {
        return MODE_DEVELOPMENT;
    }
    public static String getBaseUrl(){

        if(isModeDevelopment()){
            return  URL_DEV;
        }else {
            return URL_PROD;
        }

        //return isModeDevelopment() ? URL_DEV : URL_PROD;
    }

    public static String getBaseUrlWithApiVersioning(){
        return getBaseUrl().concat(API_VERSION);
    }

    private static final String kHeaderDevice = "deviceType";
    private static final String kHeaderAccessToken = "SessionToken";
    private static final String vHeaderDevice = "android";
    private static final String kTimezone = "Timezone";
    private static final String KDeviceToken = "deviceToken";


    public static String generateUrl(String urlSuffix){
        return getBaseUrl().concat(urlSuffix);
    }

    public static String generateUrlWithVersion(String urlSuffix){
        return getBaseUrl().concat(API_VERSION).concat(urlSuffix);
    }

    public static String getFireBaseUrl() {

        return isModeDevelopment() ? fireBaseDataBase_Development_url : fireBaseDataBase_live_url;
    }

    public static HashMap<String, String> getDefaultHeaders(){
        HashMap<String, String> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Content-Type", "application/json");
        defaultHeaders.put("Accept","application/json");
        defaultHeaders.put(kHeaderDevice, vHeaderDevice);
        defaultHeaders.put(kTimezone, getTimezone());
        defaultHeaders.put("language", "english");
        defaultHeaders.put("AppKey", "Poru13feqwopto2qMpt43");

        defaultHeaders.put("GuestToken", GuestPrefrenceManager.getInstance(App.getInstance()).getGuestToken());
        defaultHeaders.put("currentVersion", BuildConfig.VERSION_NAME);
        return defaultHeaders;
    }

    public static HashMap<String, String> getGuestHeaders()   {
        HashMap<String, String> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Content-Type", "application/json");
        defaultHeaders.put(kHeaderDevice, vHeaderDevice);
        defaultHeaders.put(kTimezone, getTimezone());
        defaultHeaders.put("language", "english");
        defaultHeaders.put("AppKey", "Poru13feqwopto2qMpt43");
        return defaultHeaders;
    }

    public static HashMap<String, String> getAppHeaders(){
        HashMap<String, String> appHeaders = getDefaultHeaders();
        appHeaders.put(kHeaderAccessToken, App.getInstance().getAccessToken());
        return appHeaders;
    }



    private static String getTimezone() {
        TimeZone tz = TimeZone.getDefault();
        return tz.getID() + "";
    }

    /*public static String getLanguage() {
        if (new LanguagePref(App.getInstance()).getLanguage()!=null && new LanguagePref(App.getInstance()).getLanguage().equalsIgnoreCase("ar")) {
            return new LanguagePref(App.getInstance()).getLanguage();
        } else {
            return "en";
        }
    }*/

    public static String getDeviceToken(){
        return  FirebaseInstanceId.getInstance().getToken();
    }
}
