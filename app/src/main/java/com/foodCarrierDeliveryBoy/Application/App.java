package com.foodCarrierDeliveryBoy.Application;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

public class App extends MultiDexApplication {

    private Context appContext;

    private Gson gson;
    private RequestQueue requestQueue;

    private static App applicationInstance;

    public static App getInstance() {
        return applicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationInstance = this;
        appContext = this;
        gson = new Gson();
        FirebaseApp.initializeApp(appContext);

        requestQueue = Volley.newRequestQueue(this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg =token;
                        Log.d("notiiiifii", msg);

                    }
                });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public Gson getGson() {
        return gson;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public Drawable getAppDrawable(int drawableID) {
        return ContextCompat.getDrawable(appContext, drawableID);
    }

    public String getAccessToken() {
        try {
            if (PrefManager.getInstance(appContext).getUserDetail() != null) {
                return PrefManager.getInstance(appContext).getUserDetail().getUser().getSession_token();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public String getModeOfTransport() {
        try {
            if (PrefManager.getInstance(appContext).getUserDetail() != null) {
                return PrefManager.getInstance(appContext).getUserDetail().getUser().getSelected_vehicle().getName();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public void setModeOfTransport(String name) {

            if (PrefManager.getInstance(appContext).getUserDetail() != null) {
                PrefManager.getInstance(appContext).getUserDetail().getUser().getSelected_vehicle().setName(name);
            }

    }
}