package com.foodCarrierDeliveryBoy.Network;


import android.content.Context;

import com.foodCarrierDeliveryBoy.Application.App;
import com.foodCarrierDeliveryBoy.Utilities.AppMethods;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static PrefManager prefManager;
    private static Context context;

    public static RetrofitInterface getClient() {
        if (retrofit == null) {
            context = App.getInstance().getApplicationContext();
            prefManager= PrefManager.getInstance(context);
            OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
            okHttpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request=null;
                    if (prefManager.getKeyIsLoggedIn()) {

                        request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("deviceType","android")
                                .header("SessionToken",App.getInstance().getAccessToken())
                                .header("x-auth", App.getInstance().getAccessToken())
                                .header("timezone", AppMethods.getTimezone())
                                .method(original.method(), original.body())
                                .build();
                    }
                    else {
                        request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("deviceType","android")
                                .header("app_language","english")
                                . header("deviceToken", FirebaseInstanceId.getInstance().getToken())
                                .method(original.method(), original.body())
                                .build();
                    }
                    return chain.proceed(request);
                }
            });
            okHttpClient.connectTimeout(30000, TimeUnit.SECONDS);
            okHttpClient.readTimeout(30000, TimeUnit.SECONDS);
            okHttpClient.writeTimeout(30000, TimeUnit.SECONDS);

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(UrlFactory.getBaseUrlWithApiVersioning())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient.build())
                    .build();
        }
        return retrofit.create(RetrofitInterface.class);
    }
}
