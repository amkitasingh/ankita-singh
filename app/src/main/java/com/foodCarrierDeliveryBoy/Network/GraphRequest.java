package com.foodCarrierDeliveryBoy.Network;


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.foodCarrierDeliveryBoy.Dialogs.ErrorDialog;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class GraphRequest {
    private RequestQueue requestQueue;
    private Context context;
    private ProgressDialog loader;

    public GraphRequest(RequestQueue volleyRequestQueue, Context mContext) {
        this.context = mContext;
        this.requestQueue = volleyRequestQueue;
        this.loader = new ProgressDialog(context);
    }
    public void makeGraphRequest(final Location data) {
        if (!((Activity) context).isFinishing()) {
            if (loader != null) {
                // loader.showDialog(2);
            }
        }
        JSONObject object = new JSONObject();
        try {
            //input your API parameters
            object.put("full_address",null);
            object.put("latitude",data.getLatitude());
            object.put("longitude" , data.getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest graphJob = new JsonObjectRequest(Request.Method.POST,
                UrlFactory.generateUrlWithVersion(AppConstants.URL_LOCATION_UPDATE),object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        try {
                            // JSONObject jsonObject = new JSONObject(response);
                            // Log.i("API===", response);
                            //loader.dismiss();
                            if (jsonObject.getInt("code") == 200) {
                                Log.d("response", "onResponse: "+jsonObject);
                               /* if (resultCallback != null) {
                                    resultCallback.notifySuccess(data, response, loader);
                                }*/
                            } else if (jsonObject.getInt("code") == 345) {
//                                if (resultCallback != null) {
//                                    resultCallback.notifySessionExpired(response, loader);
//                                }
                            } else if (jsonObject.getInt("code") == 102) {
                                /*if (resultCallback != null) {
                                    resultCallback.forceUpdate(jsonObject.getString("message"), loader);
                                }*/
                            } else if (jsonObject.getInt("code") == 101) {
                                /*if (resultCallback != null) {
                                    resultCallback.normalUdapte(jsonObject.getString("message"), loader);
                                }*/
                            }else if (jsonObject.getInt("code") == 340) {
                                ErrorDialog.show(context,  jsonObject.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "onErrorResponse: "+error);
                        error.printStackTrace();
                       /* if (resultCallback != null) {
                            resultCallback.notifyError(error, loader);
                        }*/
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return UrlFactory.getAppHeaders();
            }
        };

        requestQueue.add(graphJob);
    }
}
