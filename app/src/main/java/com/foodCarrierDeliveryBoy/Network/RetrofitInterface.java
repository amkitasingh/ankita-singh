package com.foodCarrierDeliveryBoy.Network;


import com.foodCarrierDeliveryBoy.Models.AddTicketModel;
;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface RetrofitInterface {

 /*   @Multipart
    @POST(AppConstants.URL_ADD_VEHICLE)
    Call<ModelSuccess> addNewVehicle(@Part("VehicleMaker") RequestBody vehicleMaker,
                                     @Part("regNumber") RequestBody regNumber,
                                     @Part("vechcleTypeId") RequestBody typeid,
                                     @Part("driving_licenseNo") RequestBody liscenceNo,
                                     @Part("expiry_date") RequestBody date,
                                     @Part("categoryId") RequestBody categoryId,
                                     @Part MultipartBody.Part[] images);*/

    @POST("create/ticket")
    Call<AddTicketModel> createTicket(@Body RequestBody file);



}