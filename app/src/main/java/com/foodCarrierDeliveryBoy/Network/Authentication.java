package com.foodCarrierDeliveryBoy.Network;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.databinding.library.baseAdapters.BuildConfig;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foodCarrierDeliveryBoy.Application.App;
import com.foodCarrierDeliveryBoy.CallBacks.AuthenticationCallBacks;
import com.foodCarrierDeliveryBoy.CallBacks.BaseCallBacks;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.NetworkUtils;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Authentication {

    public static void post(@NonNull final String url, @NonNull final HashMap<String, String> params,
                            @NonNull final BaseCallBacks callBacks, final boolean useDefaultHeaders) {

        if (NetworkUtils.isNetworkAvailable()) {
            String full_url = UrlFactory.generateUrlWithVersion(url);
            HashMap<String, String> headers;
            if (useDefaultHeaders) {
                headers = UrlFactory.getAppHeaders();
            } else {
                headers = UrlFactory.getDefaultHeaders();
            }
            hit(full_url, params, headers, Request.Method.POST, callBacks);

        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void get(@NonNull final String url, @NonNull final BaseCallBacks callBacks,
                           final boolean useDefaultHeaders) {

        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
            Log.d("TAG", "logout ");
            String full_url = UrlFactory.generateUrlWithVersion(url);
            HashMap<String, String> headers;
            if (useDefaultHeaders) {
                headers = UrlFactory.getDefaultHeaders();
            } else {
                headers = UrlFactory.getAppHeaders();
            }
            print(url, "hitting", "get type", headers.toString());
            hit(full_url, null, headers, Request.Method.GET, callBacks);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void getWithoutLoader(@NonNull final String url, @NonNull final BaseCallBacks callBacks,
                                        final boolean useDefaultHeaders) {

        if (NetworkUtils.isNetworkAvailable()) {
            //callBacks.showLoader();
            String full_url = UrlFactory.generateUrlWithVersion(url);
            HashMap<String, String> headers;
            if (useDefaultHeaders) {
                headers = UrlFactory.getDefaultHeaders();
            } else {
                headers = UrlFactory.getAppHeaders();
            }
            print(url, "hitting", "get type", headers.toString());
            hit(full_url, null, headers, Request.Method.GET, callBacks);

        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void getWithAppVersion(@NonNull final String url, @NonNull final BaseCallBacks callBacks,
                                         final boolean useDefaultHeaders) {

        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
            String full_url = UrlFactory.generateUrlWithVersion(url);
            HashMap<String, String> headers;
            if (useDefaultHeaders) {
                headers = UrlFactory.getDefaultHeaders();
            } else {
                headers = UrlFactory.getAppHeaders();
            }
            print(url, "hitting", "get type", headers.toString());
            hit(full_url, null, headers, Request.Method.GET, callBacks);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void postWithAppVersion(@NonNull final String url, @NonNull final HashMap<String, String> params, @NonNull final BaseCallBacks callBacks,
                                          final boolean useDefaultHeaders) {

        if (NetworkUtils.isNetworkAvailable()) {

            String full_url = UrlFactory.generateUrlWithVersion(url);
            HashMap<String, String> headers;
            if (useDefaultHeaders) {
                headers = UrlFactory.getDefaultHeaders();
            } else {
                headers = UrlFactory.getAppHeaders();
            }
            hit(full_url, params, headers,Request.Method.POST, callBacks);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void print(String url, String response, String params, String headers) {
        if (BuildConfig.DEBUG) {
            try {
                Log.i(url, response+"\n\n"+"params: "+params+ "\n\n"+"headers: "+headers+"\n\n");
            }catch (Exception e){  // call kro
                e.printStackTrace();
            }


        }
    }

    public static void objectRequestAPi(Context context, final HashMap<String, String> params, final String URL, final HashMap<String, String> headers,
                                        final BaseCallBacks callBacks) {
        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(URL, response.toString(), params != null ? params.toString() : "get type", headers.toString());
                }
                try {
                    if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseRecordNotFound
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(URL, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactNotExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } /*else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    }*/ else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserAlreadyExist) {
                        callBacks.onNeedToSendSignUp();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire();
                    } else if (response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (response.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (response.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.error_title_text));
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });
        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(request);
    }



    public static void hit(@NonNull final String url, final HashMap<String, String> params,
                           @NonNull final HashMap<String, String> headers, int method,
                           @NonNull final BaseCallBacks callBacks) {

        StringRequest request = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (UrlFactory.isModeDevelopment()) {
                        print(url, response, params != null ? params.toString() : "get type", headers.toString());
                    }
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {

                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {

                                callBacks.onTaskSuccess(r);

                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {

                return headers;
            }
        };


        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

    public static void homePageListingAPi(@NonNull final String url, final HashMap<String, String> params,
                                          @NonNull final HashMap<String, String> headers,
                                          @NonNull final BaseCallBacks callBacks) {

        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }
        String full_url = UrlFactory.generateUrlWithVersion(url);

        StringRequest request = new StringRequest(Request.Method.POST,full_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (UrlFactory.isModeDevelopment()) {
                        print(url, response, params != null ? params.toString() : "get type", headers.toString());
                    }
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {

                return headers;
            }
        };


        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }



    public static void objectRequestUserDNotExist(Context context, final HashMap<String, String> params, final String URL, final HashMap<String, String> headers,
                                                  final BaseCallBacks callBacks) {

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(URL, response.toString(), params != null ? params.toString() : "get type", headers.toString());
                }
                try {
                    if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactNotExist
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseRecordNotFound
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(URL, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } /*else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserNotExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    }*/ else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserAlreadyExist) {
                        callBacks.onNeedToSendSignUp();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire();
                    } else if (response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (response.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (response.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.error_title_text));
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(request);

    }


    public static void updateUserDetails(String urll,  String f_name, String l_Name, String email, String imagePath,
                                         @NonNull final BaseCallBacks callBacks) {


        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        final String url = UrlFactory.generateUrlWithVersion(urll);

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            public void onResponse(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }

            public String getBodyContentType() {
                return "multipart/form-data; boundary=MINE_BOUNDARY";
            }
        };


        request.addStringParam("fname", f_name);
        request.addStringParam("lname", l_Name);
        request.addStringParam("email",email);
        if (imagePath != null) {
            if (android.util.Patterns.WEB_URL.matcher(imagePath).matches()) {
                request.addStringParam("image", imagePath);
            } else {
                request.addFile("image", imagePath);
            }
        }
        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

    public static void addVehicleApi(String vehicleCategoryId,String vehicleMaker, String registrationNumber, String dlNumber,String dlExpiry,
                                     String imageFront,String imageBack,
                                     @NonNull final BaseCallBacks callBacks, final String Url) {

        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST,
                Url, new Response.Listener<String>() {
            public void onResponse(String response) {
                try {
                    if (UrlFactory.isModeDevelopment()) {
                        print(Url, response.toString(), "get type","");
                    }
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(Url, response);
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                } else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }
        };

        request.addStringParam("vehicle_id", vehicleCategoryId);
        request.addStringParam("vehicle_maker", vehicleMaker);
        request.addStringParam("registration_number", registrationNumber);
        request.addStringParam("dl_number", dlNumber);
        request.addStringParam("dl_expiry", dlExpiry);

        if (imageFront != null) {
            if (android.util.Patterns.WEB_URL.matcher(imageFront).matches()) {
                request.addStringParam("dl_front_image", imageFront);
            } else {
                request.addFile("dl_front_image", imageFront);
            }
        }
        if (imageBack != null) {
            if (android.util.Patterns.WEB_URL.matcher(imageBack).matches()) {
                request.addStringParam("dl_back_image", imageBack);
            } else {
                request.addFile("dl_back_image", imageBack);
            }
        }
        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }












    public static void updateDateOFBirth(Context context, final AuthenticationCallBacks callBacks,
                                         final String Url,
                                         final JSONObject jsonObject,final HashMap<String, String> headers) {


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Url,jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                print(Url, response.toString(), jsonObject.toString(), UrlFactory.getDefaultHeaders().toString());
                try {
                    JSONObject resObj = new JSONObject(response.toString());
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(Url, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onSuccessCallback((boolean) r);
                            } else {
                                callBacks.onSuccessCallback(r);
                            }
                        } else {
                            callBacks.onSuccessCallback("");
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onErrorCallBack(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onErrorCallBack(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onErrorCallBack(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onErrorCallBack(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print(Url, String.valueOf(""), jsonObject.toString(), UrlFactory.getDefaultHeaders().toString());
                if (callBacks != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_parsing_error));
                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        App.getInstance().getRequestQueue().getCache().clear();
        Volley.newRequestQueue(context).add(request);

    }

    public static void checkUsers(final Context context, final String url,
                                  final AuthenticationCallBacks callBacks,
                                  final HashMap<String, String> paramsession,final HashMap<String,String> headers) {

        final JSONObject loginParams = new JSONObject(paramsession);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, loginParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                print(url, String.valueOf(response), paramsession.toString(), UrlFactory.getDefaultHeaders().toString());
                try {
                    JSONObject resObj = new JSONObject(String.valueOf(response));
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(url, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onSuccessCallback((boolean) r);
                            } else {
                                callBacks.onSuccessCallback(r);
                            }
                        } else {
                            callBacks.onSuccessCallback("");
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactNotExist) {
                        callBacks.onErrorCallBack(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onErrorCallBack(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onErrorCallBack(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onErrorCallBack(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print(url, "", paramsession.toString(), UrlFactory.getDefaultHeaders().toString());
                if (callBacks != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_parsing_error));
                    }
            }
        }) {
            public Map<String, String> getHeaders() {
                return headers;
            }
        };


        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(jsonRequest);
    }

    public static void apiOfLogin(Context context, final String url,
                                  final AuthenticationCallBacks callBacks, final JSONObject jsonObject,
                                  final HashMap<String, String> headers) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                print(url, String.valueOf(response), jsonObject.toString(), UrlFactory.getDefaultHeaders().toString());
                try {
                    JSONObject resObj = new JSONObject(String.valueOf(response));
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(url, response.toString());

                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onSuccessCallback((boolean) r);
                            } else {
                                callBacks.onSuccessCallback(r);
                            }
                        } else {
                            callBacks.onSuccessCallback("");
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onErrorCallBack(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onErrorCallBack(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onErrorCallBack(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onErrorCallBack(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print(url, "", jsonObject.toString(), UrlFactory.getDefaultHeaders().toString());
                if (callBacks != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_server_not_respond));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onErrorCallBack(App.getInstance().getString(R.string.txt_parsing_error));
                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        Volley.newRequestQueue(context).add(request);
    }

    public static void objectRequestAccount(Context context, final String url,
                                            final BaseCallBacks callBacks, final JSONObject jsonObject,
                                            final HashMap<String, String> headers) {
        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject resObj = new JSONObject(String.valueOf(response));
                        if (UrlFactory.isModeDevelopment()) {
                            print(url, String.valueOf(response), jsonObject != null ? jsonObject.toString() : "get type", headers.toString());
                        }
                        if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseDLNotExit
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice
                                || resObj.getInt(AppConstants.kResponseCode) == 425) {
                            Object r = ResponseParser.parse(url, String.valueOf(response));
                            if (r != null) {
                                if (r instanceof Boolean) {
                                    callBacks.onTaskSuccess((boolean) r);
                                } else {
                                    callBacks.onTaskSuccess(r);
                                }
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                            callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                            callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onAppNeedLogin();
                        } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                            }
                        } else {
                            if (resObj.has(AppConstants.kResponseMsg)) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }
                    } catch (JSONException e) {
                        callBacks.onTaskError(null);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
            Volley.newRequestQueue(context).add(request);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void objectRequestLandingPageApi(Context context, final String url,
                                                   final BaseCallBacks callBacks, final JSONObject jsonObject,
                                                   final HashMap<String, String> headers,boolean isShowLoader) {
        if (NetworkUtils.isNetworkAvailable()) {
            if (isShowLoader) callBacks.showLoader();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject resObj = new JSONObject(String.valueOf(response));
                        if (UrlFactory.isModeDevelopment()) {
                            print(url, String.valueOf(response), jsonObject != null ? jsonObject.toString() : "get type", headers.toString());
                        }
                        if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseDLNotExit
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice
                                || resObj.getInt(AppConstants.kResponseCode) == 425) {
                            Object r = ResponseParser.parse(url, String.valueOf(response));
                            if (r != null) {
                                if (r instanceof Boolean) {
                                    callBacks.onTaskSuccess((boolean) r);
                                } else {
                                    callBacks.onTaskSuccess(r);
                                }
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                            callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                            callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onAppNeedLogin();
                        } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                            }
                        } else {
                            if (resObj.has(AppConstants.kResponseMsg)) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }
                    } catch (JSONException e) {
                        callBacks.onTaskError(null);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
            Volley.newRequestQueue(context).add(request);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void sendMessage(Context context, final String url,
                                   final BaseCallBacks callBacks, final JSONObject jsonObject,
                                   final HashMap<String, String> headers, boolean isLoaderEnable) {
        if (NetworkUtils.isNetworkAvailable()) {
            if (isLoaderEnable) callBacks.showLoader();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject resObj = new JSONObject(String.valueOf(response));
                        if (UrlFactory.isModeDevelopment()) {
                            print(url, String.valueOf(response), jsonObject != null ? jsonObject.toString() : "get type", headers.toString());
                        }
                        if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                            Object r = ResponseParser.parse(url, String.valueOf(response));
                            if (r != null) {
                                if (r instanceof Boolean) {
                                    callBacks.onTaskSuccess((boolean) r);
                                } else {
                                    callBacks.onTaskSuccess(r);
                                }
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                            callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                            callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onAppNeedLogin();
                        } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                            }
                        } else {
                            if (resObj.has(AppConstants.kResponseMsg)) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }
                    } catch (JSONException e) {
                        callBacks.onTaskError(null);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
            Volley.newRequestQueue(context).add(request);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void objectRequestWithoutLoader(Context context, final String url,
                                                  final BaseCallBacks callBacks, final JSONObject jsonObject,
                                                  final HashMap<String, String> headers) {
        if (NetworkUtils.isNetworkAvailable()) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject resObj = new JSONObject(String.valueOf(response));
                        if (UrlFactory.isModeDevelopment()) {
                            print(url, String.valueOf(response), jsonObject != null ? jsonObject.toString() : "get type", headers.toString());
                        }
                        if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                            Object r = ResponseParser.parse(url, String.valueOf(response));
                            if (r != null) {
                                if (r instanceof Boolean) {
                                    callBacks.onTaskSuccess((boolean) r);
                                } else {
                                    callBacks.onTaskSuccess(r);
                                }
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                            callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                            callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onAppNeedLogin();
                        } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                            }
                        } else {
                            if (resObj.has(AppConstants.kResponseMsg)) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }
                    } catch (JSONException e) {
                        callBacks.onTaskError(null);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
            Volley.newRequestQueue(context).add(request);
        } else {
            callBacks.onInternetNotFound();
        }
    }


    public static void objectRequestUserExist(Context context, final HashMap<String, String> params, final String URL, final HashMap<String, String> headers,
                                              final BaseCallBacks callBacks) {

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(URL, response.toString(), params != null ? params.toString() : "get type", headers.toString());
                }
                try {
                    if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactNotExist
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseRecordNotFound
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(URL, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } /*else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    }*/ else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserAlreadyExist) {
                        callBacks.onNeedToSendSignUp();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire();
                    } else if (response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (response.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (response.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.error_title_text));
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(request);

    }

    public static void addCar(Context context, final String url,String vehicle_id,String makerName, String vehicleNumber,
                              String DLNumber,String DlExpiryDate,String dlFrontImage, String dlBackImage,
                              final BaseCallBacks callBacks) {

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has(AppConstants.kResponseCode) &&
                            jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(url, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (jsonObject.has(AppConstants.kResponseCode) &&
                            jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(jsonObject.getString(AppConstants.kResponseMsg));
                    }   else if (jsonObject.has(AppConstants.kResponseCode) &&
                            jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserAlreadyExist) {
                        callBacks.onNeedToSendSignUp();
                    } else if (jsonObject.has(AppConstants.kResponseCode) &&
                            jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire();
                    } else if (jsonObject.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (jsonObject.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(jsonObject.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (jsonObject.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(jsonObject.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return UrlFactory.getAppHeaders();
            }
        };


        request.addStringParam("vehicle_id",vehicle_id);
        request.addStringParam("vehicle_maker",makerName);
        request.addStringParam("registration_number",vehicleNumber);
        request.addStringParam("dl_number",DLNumber);
        request.addStringParam("dl_expiry",DlExpiryDate);

        if (dlFrontImage!=null){
            if (Patterns.WEB_URL.matcher(dlFrontImage).matches()){
                request.addStringParam("dl_front_image",dlFrontImage);
            }else {
                request.addFile("dl_front_image",dlFrontImage);
            }
        }

        if (dlBackImage!=null){
            if (Patterns.WEB_URL.matcher(dlBackImage).matches()){
                request.addStringParam("dl_back_image",dlBackImage);
            }else {
                request.addFile("dl_back_image",dlBackImage);
            }
        }


        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(request);




    }


    public static void objectRequestRCS(Context context, final HashMap<String, String> params, final String URL, final HashMap<String, String> headers,
                                        final BaseCallBacks callBacks) {
        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(URL, response.toString(), params != null ? params.toString() : "get type", headers.toString());
                }
                try {
                    if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseRecordNotFound
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(URL, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactNotExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } /*else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    }*/ else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserAlreadyExist) {
                        callBacks.onNeedToSendSignUp();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire();
                    } else if (response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (response.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (response.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.error_title_text));
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });
        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(request);
    }





    public static void objectRequestRCSTwo(Context context, final HashMap<String, String> params, final String URL, final HashMap<String, String> headers,
                                           final BaseCallBacks callBacks) {
        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                print(URL, response.toString(), params != null ? params.toString() : "get type", headers.toString());

                try {
                    if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || response.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(URL, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserNotExist) {
                        callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                    }  else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.KResponseUserAlreadyExist) {
                        callBacks.onNeedToSendSignUp();
                    } else if (response.has(AppConstants.kResponseCode) &&
                            response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire();
                    } else if (response.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (response.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (response.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(response.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print(URL, error.toString(), params != null ? params.toString() : "post type", headers.toString());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.error_title_text));
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
            }
        });
        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(request);
    }
    public static void searchFilterApi(final Context context, final String url,
                                       final BaseCallBacks callBacks, final JSONObject jsonObject,
                                       final HashMap<String, String> headers, boolean isLaderEnable) {
        if (UrlFactory.isModeDevelopment()) {
            print(url, "Params:" + jsonObject, "", "");
        }
        if (NetworkUtils.isNetworkAvailable()) {
            if (isLaderEnable && !((Activity) context).isFinishing()) callBacks.showLoader();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject resObj = new JSONObject(String.valueOf(response));
                        if (UrlFactory.isModeDevelopment()) {
                            print(url, String.valueOf(response), jsonObject != null ? jsonObject.toString() : "get type", headers.toString());
                        }
                        if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                                || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                            Object r = ResponseParser.parse(url, String.valueOf(response));
                            if (r != null) {
                                String page = "";
                                page = jsonObject.getString("page");
                                if (page != null) {
                                    if (page.equals("1")) {
                                        if (r instanceof Boolean) {
                                            callBacks.onTaskSuccess((boolean) r);
                                        } else {
                                            callBacks.onTaskSuccess(r);

                                        }
                                    } else {
                                        if (r instanceof Boolean) {
                                            callBacks.onLoadMore((boolean) r);
                                        } else {
                                            callBacks.onLoadMore(r);
                                        }
                                    }
                                }

                            } else {
                                callBacks.onTaskError(null);
                            }

                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                            callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                            callBacks.onAppNeedLogin();
                        } else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                        }else if (resObj.has(AppConstants.kResponseCode) &&
                                resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                            callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                        } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                            if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                            }
                        } else {
                            if (resObj.has(AppConstants.kResponseMsg)) {
                                callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                            } else {
                                callBacks.onTaskError(null);
                            }
                        }
                    } catch (JSONException e) {
                        callBacks.onTaskError(null);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
            Volley.newRequestQueue(context).add(request);
        } else {
            callBacks.onInternetNotFound();
        }
    }

    public static void object(final Context context, final String url,
                              final BaseCallBacks callBacks,
                              final JSONObject jsonObject) {
        // callBacks.showLoader();
        Log.d("responseee",url);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("responsee",String.valueOf(response));

                print(url, String.valueOf(response), jsonObject.toString(), UrlFactory.getDefaultHeaders().toString());
                try {
                    JSONObject resObj = new JSONObject(String.valueOf(response));
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {


                        Object r = ResponseParser.parse(url, response.toString());
                        Log.d("responseeEnd",String.valueOf(r));
                        if (r != null) {
                            Log.d("aaaaasss",String.valueOf(resObj.getInt("code")));
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                Log.d("resss",r.toString());
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskSuccess("");
                        }
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print(url, error.getMessage(), jsonObject.toString(), UrlFactory.getDefaultHeaders().toString());
                if (callBacks != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }
        };


        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(jsonRequest);
    }

    public static void stringRequest(final Context context, final String url,
                                     final BaseCallBacks callBacks,
                                     final HashMap<String,String> parms) {
        StringRequest jsonRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                print(url, String.valueOf(response), parms.toString(), UrlFactory.getDefaultHeaders().toString());
                try {
                    JSONObject resObj = new JSONObject(String.valueOf(response));
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(url, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskSuccess("");
                        }
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print(url, error.getMessage(), parms.toString(), UrlFactory.getDefaultHeaders().toString());
                if (callBacks != null)
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof AuthFailureError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                    } else if (error instanceof ServerError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                    } else if (error instanceof NetworkError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                    } else if (error instanceof ParseError) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                    }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }

            public Map<String, String> getParams() {
                return parms;
            }
        };


        Volley.newRequestQueue(context).getCache().clear();
        Volley.newRequestQueue(context).add(jsonRequest);
    }

    public static void multiPartRequest(String kID, String kImage, String id, String imagePath, String Url, @NonNull final BaseCallBacks callBacks) {

        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        final String url = UrlFactory.generateUrlWithVersion(Url);

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            public void onResponse(String response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(url, String.valueOf(response), "", "");
                }
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                } else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }
        };
        request.addStringParam(kID, id);
        if (imagePath != null) {
            Log.i("Path", imagePath);
            if (android.util.Patterns.WEB_URL.matcher(imagePath).matches()) {
                request.addStringParam(kImage, imagePath);
            } else {
                request.addFile(kImage, imagePath);
            }
            Log.i("Path", imagePath);
        }
        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

    public static void signUpDetailValidation( String Url, @NonNull final BaseCallBacks callBacks,String business_name,
                                               String registration_no,
                                               String phone,String business_registered) {
        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        final String url = Url;

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            public void onResponse(String response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(url, String.valueOf(response), "", "");
                }
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactNotExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                } else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }
        };
        request.addStringParam("registation_no",registration_no);
        request.addStringParam("phone",phone);
        request.addStringParam("business_name",business_name);
        request.addStringParam("business_registered",business_registered);
        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }




    public static void objectApi(Context context, final String url,
                                 final BaseCallBacks callBacks, final JSONObject jsonObject,
                                 final HashMap<String, String> headers, boolean isLaderEnable) {
        if (isLaderEnable) callBacks.showLoader();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject resObj = new JSONObject(response.toString());
                    if (UrlFactory.isModeDevelopment()) {
                        print(url, String.valueOf(response), jsonObject != null ? jsonObject.toString() : "get type", headers.toString());
                    }
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kUpdateDevice) {
                        Object r = ResponseParser.parse(url, response.toString());
                        if (r != null) {
                            if (r instanceof Boolean) {
                                callBacks.onTaskSuccess((boolean) r);
                            } else {
                                callBacks.onTaskSuccess(r);
                            }
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));

                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }/*else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }*/
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                return headers;
            }
        };

        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

    public static void carRegistrationPhotoReq(String kID, String kImage, String id,
                                               String imagePath, String kImageType, String
                                                       imageType, String Url,
                                               @NonNull final BaseCallBacks callBacks) {

        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        final String url = UrlFactory.generateUrlWithVersion(Url);

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            public void onResponse(String response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(url, String.valueOf(response), "", "");
                }
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                } else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }
        };
        request.addStringParam(kID, id);
        request.addStringParam(kImageType, imageType);
        if (imagePath != null) {
            if (android.util.Patterns.WEB_URL.matcher(imagePath).matches()) {
                request.addStringParam(kImage, imagePath);
            } else {
                request.addFile(kImage, imagePath);
            }
        }
        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

    public static void postTypeWithLoader(@NonNull final String url,
                                          @NonNull final HashMap<String, String> headers, final HashMap<String, String> params, int method,
                                          @NonNull final BaseCallBacks callBacks) {
        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject resObj = new JSONObject(response.toString());
                    if (UrlFactory.isModeDevelopment()) {
                        print(url, response.toString(), "get type", headers.toString());
                    }
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(url, response.toString());
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseContactExist) {
                        callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    }else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseNotAccess) {
                        callBacks.onAppNeedLogin();
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        if (resObj.getString(AppConstants.kResponseMsg).equalsIgnoreCase("Please activate your car first does not exists !!")) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                        }
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_server_not_respond));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                return headers;
            }
        };

        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

    public static void ProfilemultiPartRequest(String imagepath,String Url,
                                               @NonNull final BaseCallBacks callBacks,String phone,String id_no,String name,String email,String role) {


        if (NetworkUtils.isNetworkAvailable()) {
            callBacks.showLoader();
        } else {
            callBacks.onInternetNotFound();
            return;
        }

        final String url = UrlFactory.generateUrlWithVersion(Url);

        SimpleMultiPartRequest request = new SimpleMultiPartRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            public void onResponse(String response) {
                if (UrlFactory.isModeDevelopment()) {
                    print(url, String.valueOf(response), "", "");
                }
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseOK
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSignUp
                            || resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUserUpdated) {
                        Object r = ResponseParser.parse(url, response);
                        if (r != null) {
                            callBacks.onTaskSuccess(r);
                        } else {
                            callBacks.onTaskError(null);
                        }
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseUpdate) {
                        callBacks.onAppNeedUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseForceUpdate) {
                        callBacks.onAppNeedForceUpdate(resObj.getString(AppConstants.kResponseMsg));
                    } else if (resObj.has(AppConstants.kResponseCode) &&
                            resObj.getInt(AppConstants.kResponseCode) == AppConstants.kResponseSessionExpire) {
                        callBacks.onSessionExpire(resObj.getString(AppConstants.kResponseMsg));
                    } else {
                        if (resObj.has(AppConstants.kResponseMsg)) {
                            callBacks.onTaskError(resObj.getString(AppConstants.kResponseMsg));
                        } else {
                            callBacks.onTaskError(null);
                        }
                    }
                } catch (JSONException e) {
                    callBacks.onTaskError(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof AuthFailureError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                } else if (error instanceof ServerError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.text_server_error));
                } else if (error instanceof NetworkError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_connection_error));
                } else if (error instanceof ParseError) {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_parsing_error));
                } else {
                    callBacks.onTaskError(App.getInstance().getString(R.string.txt_went_wrong));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                return UrlFactory.getAppHeaders();
            }
        };





        request.addStringParam("phone", phone);
        Log.e("phone",phone);
        request.addStringParam("user_id", id_no);
        request.addStringParam("email", email);
        Log.e("user_id",id_no);
        request.addStringParam("role", role);

        request.addStringParam("name", name);
        Log.e("name",name);


        if (imagepath != null) {
            if (android.util.Patterns.WEB_URL.matcher(imagepath).matches()) {
                request.addStringParam("image", imagepath);
                request.addStringParam("image", imagepath);
                Log.e("image",imagepath);
            } else {
                request.addFile("image", imagepath);
                Log.e("image",imagepath);
            }
            Log.i("Path", imagepath);
        }
        App.getInstance().getRequestQueue().getCache().clear();
        App.getInstance().getRequestQueue().add(request);
    }

}