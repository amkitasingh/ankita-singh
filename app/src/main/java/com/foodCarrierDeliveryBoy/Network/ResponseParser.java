package com.foodCarrierDeliveryBoy.Network;


import com.foodCarrierDeliveryBoy.Application.App;
import com.foodCarrierDeliveryBoy.Models.AllTicketModel;
import com.foodCarrierDeliveryBoy.Models.ContactUsModel;
import com.foodCarrierDeliveryBoy.Models.CreatedVehicleListModel;
import com.foodCarrierDeliveryBoy.Models.EarningModel;
import com.foodCarrierDeliveryBoy.Models.Faqs;
import com.foodCarrierDeliveryBoy.Models.GuestModel;
import com.foodCarrierDeliveryBoy.Models.LogoutModel;
import com.foodCarrierDeliveryBoy.Models.ModelAcceptedOrders;
import com.foodCarrierDeliveryBoy.Models.ModelAllModes;
import com.foodCarrierDeliveryBoy.Models.ModelChatAllMessage;
import com.foodCarrierDeliveryBoy.Models.ModelContactExist;
import com.foodCarrierDeliveryBoy.Models.ModelOnlineOffline;
import com.foodCarrierDeliveryBoy.Models.ModelOrderLocDetail;
import com.foodCarrierDeliveryBoy.Models.ModelStatus;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Models.ModelViewOrder;
import com.foodCarrierDeliveryBoy.Models.NotificationModel;
import com.foodCarrierDeliveryBoy.Models.OrderHistoryDetailModel;
import com.foodCarrierDeliveryBoy.Models.PasswordTokenModel;
import com.foodCarrierDeliveryBoy.Models.SelectVehicleModel;
import com.foodCarrierDeliveryBoy.Models.TransportCreatedModel;
import com.foodCarrierDeliveryBoy.Models.UserModel;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;

public class ResponseParser {

    public static Object parse(String url, String response) {

        url = url.replaceAll(UrlFactory.getBaseUrlWithApiVersioning(), "");

        if (url.contains("?user_id")) {
            url = "events/";
        } else if (url.contains("?page"))
            url = "events?";

        try {
            switch (url) {

                /*========= parse model like bottom ================*/

                /*case HRAppConstants.URL_LOGIN:
                    return App.getInstance().getGson().fromJson(response, LoginModel.class);*/
                case AppConstants.URL_UPDATE_PROFILE:
                case AppConstants.URL_SIGN_IN:
                case AppConstants.URL_SIGN_UP:
                    return App.getInstance().getGson().fromJson(response, UserModel.class);

                case AppConstants.URL_CREATE_GUEST:
                    return App.getInstance().getGson().fromJson(response, GuestModel.class);


                case AppConstants.URL_FORGET_PASSWORD_TOKEN:
                    return App.getInstance().getGson().fromJson(response, PasswordTokenModel.class);

                case AppConstants.URL_CONTACT_EXIST:
                    return App.getInstance().getGson().fromJson(response, ModelContactExist.class);


                case AppConstants.URL_REJECT_ORDER:
                    return "URL_REJECT_ORDER";


                case AppConstants.URL_TRANSPORTER_AT_RESTURANT:
                    return "URL_TRANSPORTER_AT_RESTURANT";
                case AppConstants.URL_CHANGE_PASSWORD:
                case AppConstants.URL_PASSWORD_RESET:
                case AppConstants.URL_ACCEPT_ORDER:
                case AppConstants.URL_CANCEL_ORDER:
                case AppConstants.URL_ORDER_DELIVERED:

                    return App.getInstance().getGson().fromJson(response, ModelSuccess.class);

                case AppConstants.URL_ADD_VEHICLE:
                    return App.getInstance().getGson().fromJson(response, TransportCreatedModel.class);


                case AppConstants.URL_ALL_MODES:
                    return App.getInstance().getGson().fromJson(response, ModelAllModes.class);

                case AppConstants.URL_LOGOUT:
                    return App.getInstance().getGson().fromJson(response, LogoutModel.class);


                case AppConstants.URL_ONLINE_OFFLINE:
                    return App.getInstance().getGson().fromJson(response, ModelOnlineOffline.class);


                case AppConstants.URL_CHECK_ONLINE:
                    return App.getInstance().getGson().fromJson(response, ModelStatus.class);


                case AppConstants.URL_VIEW_ORDER:
                    return App.getInstance().getGson().fromJson(response, ModelViewOrder.class);

                case AppConstants.URL_ORDER_LOC_DETAIL:
                    return App.getInstance().getGson().fromJson(response, ModelOrderLocDetail.class);

                case AppConstants.URL_ALL_ACCEPTED_ORDERS:
                    return App.getInstance().getGson().fromJson(response, ModelAcceptedOrders.class);

                case AppConstants.URL_FAQ:
                    return App.getInstance().getGson().fromJson(response, Faqs.class);

                case AppConstants.URL_ALL_TICKET:
                    return App.getInstance().getGson().fromJson(response, AllTicketModel.class);

                case AppConstants.URL_SEND_MESSAGE_CHAT:
                    return "success";

                case AppConstants.URL_ALL_MESSAGES_CHAT:
                    return App.getInstance().getGson().fromJson(response, ModelChatAllMessage.class);

                case AppConstants.URL_PICK_UP_ORDER:
                    return "pickedUpSuccessfully";

                case AppConstants.URL_EARNINGS:
                    return App.getInstance().getGson().fromJson(response, EarningModel.class);

                case AppConstants.URL_NOTIFICATION:
                    return App.getInstance().getGson().fromJson(response, NotificationModel.class);

                case AppConstants.URL_UPDATE_DEVICE_TOKEN:
                    return "Success";

                case AppConstants.URL_CONTACT_US:
                    return App.getInstance().getGson().fromJson(response, ContactUsModel.class);

                case AppConstants.URL_SELECT_VEHICLE:
                    return App.getInstance().getGson().fromJson(response, SelectVehicleModel.class);

                case AppConstants.URL_CREATE_TRANSPORT:
                    return App.getInstance().getGson().fromJson(response, CreatedVehicleListModel.class);
                case AppConstants.URL_ORDER_HISTORY:
                    return App.getInstance().getGson().fromJson(response, OrderHistoryDetailModel.class);


                default:
                    return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
