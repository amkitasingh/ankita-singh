package com.foodCarrierDeliveryBoy.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.foodCarrierDeliveryBoy.Activity.ActivityDashboard;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import static com.google.firebase.messaging.RemoteMessage.PRIORITY_HIGH;

public class BackgroundLocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ChildEventListener {

    private GoogleApiClient mGoogleApiClient;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseTrack;
    public static Location CURRENT_LOCATION = null;

    String userId = "";
    String onlineOffline = "";
    String TAG = "firebase";
    public ArrayList<String> listOfGroup;
    private static final int ID_SERVICE = 101;
    private GetAssignOrderFirebase callback;

    public void setCallbacks(GetAssignOrderFirebase callbacks) {
        this.callback = callbacks;
    }


    public class LocalBinder extends Binder {
        public BackgroundLocationService getService() {
            return BackgroundLocationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        userId = String.valueOf(PrefManager.getInstance(this).getUserDetail().getUser().getId());

        Intent dashboard = new Intent(this, ActivityDashboard.class);
        dashboard.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, dashboard, 0);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager == null) return;
        String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
                createNotificationChannel(notificationManager) : "";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.logo_icon)
                .setColor(ContextCompat.getColor(this, R.color.black))
                .setPriority(PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Searching for GPS")
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .build();
        startForeground(ID_SERVICE, notification);
        initProcess();
    }

    private void initProcess() {

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();

        listOfGroup = new ArrayList<>();

        FirebaseApp.initializeApp(this);
        mDatabaseTrack = FirebaseDatabase.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference()
                .child("notifications").child("transporter_".concat(userId));
        mDatabase.keepSynced(false);
        mDatabase.addChildEventListener(this);

        //userId = HRPrefManager.getInstance(this).getUserDetail().getId();
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "com.HLSDriver.notification";
        String channelName = getString(R.string.app_name);
        NotificationChannel channel = new NotificationChannel(channelId, channelName,
                NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        //super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            CURRENT_LOCATION = location;
            Intent locationIntent = new Intent(AppConstants.LOCATION_NOTIFICATION_BROADCAST);
            locationIntent.putExtra("lat", location.getLatitude());
            locationIntent.putExtra("lng", location.getLongitude());
            Log.d(TAG, "onLocationChanged "+location.getLatitude() +"\n"+location.getLongitude());
            sendBroadcast(locationIntent);
            ActivityDashboard.getDashboard().getBookingRequest().makeGraphRequest(location);
            setLocation(location.getLatitude(), location.getLongitude());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public IBinder onBind(Intent intent) {
        //rideID = intent.getStringExtra("ride_id");
        return new LocalBinder();
    }

    @Override
    public void onConnected(Bundle bundle) {

        // Request location updates using static settings
        //Intent intent = new Intent(this, LocationReceiver.class);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(4000);
        locationRequest.setFastestInterval(2000);
        //locationRequest.setSmallestDisplacement(10);

        LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient,
                locationRequest, this); // This is the changed line.

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null && mLastLocation.getLatitude() != 0.0 && mLastLocation.getLongitude() != 0.0) {

            Intent locationIntent = new Intent(AppConstants.LOCATION_NOTIFICATION_BROADCAST);
            locationIntent.putExtra("lat", mLastLocation.getLatitude());
            locationIntent.putExtra("lng", mLastLocation.getLongitude());
            sendBroadcast(locationIntent);
            Log.d("Location Service", "=========>" + mLastLocation);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mGoogleApiClient.connect();
    }

    public void setLocation(double latitude, double longitude) {
        try {
           /* if (latitude != 0.0 && longitude != 0.0) {
                DatabaseReference refAvailable = mDatabaseTrack.child("track").child("transporter_" + userId).child("location");
                refAvailable.child("lat").setValue(latitude);
                refAvailable.child("lng").setValue(longitude);
            }*/
            if (listOfGroup.size() > 0 && userId != null && !userId.equals("")) {
                for (int i = 0; i < listOfGroup.size(); i++) {
                    final String b = listOfGroup.get(i);

                    /*if (latitude != 0.0 && longitude != 0.0) {
                        DatabaseReference refAvailable = mDatabase.child(b).child("track").child("transporter_" + userId).child("location");
                        refAvailable.child("lat").setValue(latitude);
                        refAvailable.child("lng").setValue(longitude);
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        try {
            listOfGroup.add(dataSnapshot.getKey());
            Log.d(TAG, "onChildAdded:==>" + dataSnapshot.getKey());
            String commentKey = dataSnapshot.getKey();
            if (commentKey != null && commentKey.equals("newOrder")) {
                if (callback != null) {
                    callback.getAssignOrderFirebase(String.valueOf(dataSnapshot.getValue()));
                }
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        String commentKey = dataSnapshot.getKey();
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        Log.d(TAG, "onChildRemoved:==>" + dataSnapshot.getKey());
        String key = dataSnapshot.getKey();

        try {
            for (int i = 0; i < listOfGroup.size(); i++) {
                if (listOfGroup.get(i).equalsIgnoreCase(key)) {
                    listOfGroup.remove(i);
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        String commentKey = dataSnapshot.getKey();
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Log.d(TAG, "onChildMoved:==>" + dataSnapshot.getKey());
        String commentKey = dataSnapshot.getKey();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Log.w(TAG, "postComments:onCancelled", databaseError.toException());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        //if (mDatabase != null) mDatabase.removeEventListener(this);
    }

}
