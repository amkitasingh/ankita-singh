package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class ModelAllModes {

    /**
     * code : 200
     * message : success
     * vehicles : [{"id":2,"name":"bike","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231062/offer/vpjgojbpswhlhs8bmydf.jpg","range":25},{"id":3,"name":"car","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231090/offer/povnmwgvkuprngo6dsjr.png","range":25}]
     */

    private int code;
    private String message;
    private List<VehiclesBean> vehicles;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VehiclesBean> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehiclesBean> vehicles) {
        this.vehicles = vehicles;
    }

    public static class VehiclesBean {
        /**
         * id : 2
         * name : bike
         * image : http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231062/offer/vpjgojbpswhlhs8bmydf.jpg
         * range : 25.0
         */

        private int id;
        private String name;
        private String image;
        private double range;
        private  boolean isSelected= false;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public double getRange() {
            return range;
        }

        public void setRange(double range) {
            this.range = range;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
