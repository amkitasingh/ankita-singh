package com.foodCarrierDeliveryBoy.Models;

public class ModelStatus {

    /**
     * code : 200
     * message : success
     * is_online : true
     */

    private int code;
    private String message;
    private boolean is_online;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIs_online() {
        return is_online;
    }

    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }
}
