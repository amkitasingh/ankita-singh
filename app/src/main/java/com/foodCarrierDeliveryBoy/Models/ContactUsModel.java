package com.foodCarrierDeliveryBoy.Models;

public class ContactUsModel {


    /**
     * code : 200
     * message : success
     * contact_us : {"email":"Support@slider.sg","country_code":"(+65)","contact":"876 543 21","address":"B- 458, Changi Business Park, Singapore"}
     */

    private int code;
    private String message;
    private ContactUsBean contact_us;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ContactUsBean getContact_us() {
        return contact_us;
    }

    public void setContact_us(ContactUsBean contact_us) {
        this.contact_us = contact_us;
    }

    public static class ContactUsBean {
        /**
         * email : Support@slider.sg
         * country_code : (+65)
         * contact : 876 543 21
         * address : B- 458, Changi Business Park, Singapore
         */

        private String email;
        private String country_code;
        private String contact;
        private String address;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
