package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class Faqs {

    /**
     * code : 200
     * message : success
     * faqs : [{"id":1,"question":"What is Rocket App?","answer":"Rocket App is a leading online food delivery service that operates in Kuwait, Saudi Arabia, UAE, Bahrain, Oman, Qatar and Jordan."},{"id":2,"question":"What does Rocket App do?","answer":"We simply take your submitted order and send it to the restaurant through a completely automated process, so you don\u2019t have to deal with all the hassle of ordering and we make sure that you receive your order on time, every-time!"},{"id":3,"question":"Why should I use Rocket App than using a phone?","answer":"Rocket App is the one huge food court for many restaurants, so you don\u2019t need to go through the hassle of finding restaurants numbers, waiting on hold or getting a busy signal while dialing a restaurant number or getting the wrong order due to miscommunication over the phone! Besides by using Rocket App you can view menus with pictures of all your favorite restaurants in our easy to use website and apps."},{"id":4,"question":"How much will it cost me to use Rocket App services?","answer":"The only extra charges that might be applied are the restaurant delivery fees."},{"id":6,"question":"Do you have Debit Card / Credit Cards services?","answer":"Yes, most of the restaurants on Rocket App support online payment options for Debit Card/Credit Card. Here you can find all online payment methods we provide based on countries:\r\n\r\n- UAE: MasterCard, Visa and Visa Checkout. \r\n- Qatar: MasterCard and Visa.\r\n- Kuwait: MasterCard, Visa and K-Net.\r\n- KSA: MasterCard, Visa and SADAD.\r\n- Bahrain: Benefit (Debit Card), MasterCard and Visa.\r\n- Oman: MasterCard and Visa.\r\n- Jordan: Online payment options are coming soon."},{"id":7,"question":"Do you have special offers?","answer":"Yes. You can view the latest restaurant promotions and discount coupon in offers tab."},{"id":9,"question":"What is Visa Checkout?","answer":"Visa Checkout makes online shopping more enjoyable by making it easy to complete your purchase. Simply enter your username and password whenever you see the Visa Checkout button and speed through your online shopping experiences with a single account that can be used across all your devices.\r\n\r\n(Visa Checkout available only in UAE)"},{"id":10,"question":"Is Visa Checkout only support Visa cards?","answer":"You can add your Mastercard and Visa cards to Visa Checkout account."},{"id":11,"question":"How do I place an order on Rocket App?","answer":"Follow these simple instructions:\r\n\r\nCreate an account or login using your Facebook account. You can also order without an account using \u201cُExpress Checkout\u201d. Note: logged in users benefit from a personalized experience.\r\n\r\nSelect your area and optionally choose a cuisine you're craving and hit \"Find Restaurants\".\r\n\r\nYou'll see a wide variety of restaurants in your area, choose your favorite restaurant from the list or use sort, filter, cuisines or search to find exactly what you're looking for.\r\n\r\nClick on a restaurant to view its menu, search the menu, read reviews. Add items directly to your cart and proceed to checkout.\r\n\r\nSelect or enter your address, review your order, select a delivery time, apply voucher code in checkout if applicable and hit \"Place Order\".\r\n\r\nYour order will then be placed and routed to the restaurant immediately. Note: if you are using our service for the first time we will call your mobile number for confirmation purposes."},{"id":12,"question":"Can I place an order without creating an account?","answer":"Yes. You can place an \u2018Express Checkout\u2019 order which is an easier way for newcomers to experience our online service. \r\n\r\nTo order without an account, continue with your ordering journey and when prompted to login choose \"Express Checkout\" option. Provide your name, mobile number and your address at the checkout page!\r\n\r\nThe purpose of this service is to give a chance to all newcomers to test our service and experience its simplicity, efficiency and reliability.\r\n\r\nOnce familiar with our service, you can simply create an account to access all the added value features and benefit from a personalized ordering experience."}]
     */

    private int code;
    private String message;
    private List<FaqsBean> faqs;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FaqsBean> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<FaqsBean> faqs) {
        this.faqs = faqs;
    }

    public static class FaqsBean {
        /**
         * id : 1
         * question : What is Rocket App?
         * answer : Rocket App is a leading online food delivery service that operates in Kuwait, Saudi Arabia, UAE, Bahrain, Oman, Qatar and Jordan.
         */

        private int id;
        private String question;
        private String answer;
        private  boolean isExpanded=false;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }
    }
}
