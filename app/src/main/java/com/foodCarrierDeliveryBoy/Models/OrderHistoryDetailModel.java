package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class OrderHistoryDetailModel {


    /**
     * code : 200
     * message : success
     * orders : [{"id":2387,"sub_total":180,"placed_at":"16 October 2020 16:29 PM","order_id":"1016-2387","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73881,"item_name":"White Sauce Pasta","item_addons":[{"id":483253,"addon_title":"cheesy "},{"id":483265,"addon_title":"Rock salt"},{"id":483266,"addon_title":"Green chilli"},{"id":483254,"addon_title":"pepsi 250ml"},{"id":483255,"addon_title":"Half"},{"id":483256,"addon_title":"Full"}]}]},{"id":2386,"sub_total":180,"placed_at":"16 October 2020 16:28 PM","order_id":"1016-2386","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73881,"item_name":"White Sauce Pasta","item_addons":[{"id":483253,"addon_title":"cheesy "},{"id":483265,"addon_title":"Rock salt"},{"id":483266,"addon_title":"Green chilli"},{"id":483254,"addon_title":"pepsi 250ml"},{"id":483255,"addon_title":"Half"},{"id":483256,"addon_title":"Full"}]}]},{"id":2385,"sub_total":50,"placed_at":"16 October 2020 16:24 PM","order_id":"1016-2385","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73850,"item_name":"Peppy Paneer","item_addons":[]}]},{"id":2384,"sub_total":180,"placed_at":"16 October 2020 15:45 PM","order_id":"1016-2384","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73881,"item_name":"White Sauce Pasta","item_addons":[{"id":483253,"addon_title":"cheesy "},{"id":483265,"addon_title":"Rock salt"},{"id":483266,"addon_title":"Green chilli"},{"id":483254,"addon_title":"pepsi 250ml"},{"id":483255,"addon_title":"Half"},{"id":483256,"addon_title":"Full"}]}]},{"id":2383,"sub_total":140,"placed_at":"16 October 2020 12:15 PM","order_id":"1016-2383","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73845,"item_name":"Margherita","item_addons":[{"id":483236,"addon_title":"red bell peppers"},{"id":483237,"addon_title":"black olives"},{"id":483244,"addon_title":"Large"},{"id":483245,"addon_title":"Medium"},{"id":483246,"addon_title":"Small"}]}]},{"id":2382,"sub_total":140,"placed_at":"16 October 2020 12:13 PM","order_id":"1016-2382","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73845,"item_name":"Margherita","item_addons":[{"id":483236,"addon_title":"red bell peppers"},{"id":483237,"addon_title":"black olives"},{"id":483244,"addon_title":"Large"},{"id":483245,"addon_title":"Medium"},{"id":483246,"addon_title":"Small"}]}]},{"id":2381,"sub_total":140,"placed_at":"16 October 2020 11:53 AM","order_id":"1016-2381","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73845,"item_name":"Margherita","item_addons":[{"id":483236,"addon_title":"red bell peppers"},{"id":483237,"addon_title":"black olives"},{"id":483244,"addon_title":"Large"},{"id":483245,"addon_title":"Medium"},{"id":483246,"addon_title":"Small"}]}]},{"id":2380,"sub_total":140,"placed_at":"16 October 2020 11:48 AM","order_id":"1016-2380","order_delivery_status":"cancelled","item_count":1,"menu_item":[{"id":73845,"item_name":"Margherita","item_addons":[{"id":483236,"addon_title":"red bell peppers"},{"id":483237,"addon_title":"black olives"},{"id":483244,"addon_title":"Large"},{"id":483245,"addon_title":"Medium"},{"id":483246,"addon_title":"Small"}]}]},{"id":2369,"sub_total":210,"placed_at":"15 October 2020 19:07 PM","order_id":"1015-2369","order_delivery_status":"cancelled","item_count":2,"menu_item":[{"id":73845,"item_name":"Margherita","item_addons":[{"id":483236,"addon_title":"red bell peppers"},{"id":483237,"addon_title":"black olives"},{"id":483244,"addon_title":"Large"},{"id":483245,"addon_title":"Medium"},{"id":483246,"addon_title":"Small"}]},{"id":73850,"item_name":"Peppy Paneer","item_addons":[]}]},{"id":2368,"sub_total":210,"placed_at":"15 October 2020 19:01 PM","order_id":"1015-2368","order_delivery_status":"cancelled","item_count":2,"menu_item":[{"id":73845,"item_name":"Margherita","item_addons":[{"id":483236,"addon_title":"red bell peppers"},{"id":483237,"addon_title":"black olives"},{"id":483244,"addon_title":"Large"},{"id":483245,"addon_title":"Medium"},{"id":483246,"addon_title":"Small"}]},{"id":73850,"item_name":"Peppy Paneer","item_addons":[]}]}]
     */

    private int code;
    private String message;
    /**
     * id : 2387
     * sub_total : 180.0
     * placed_at : 16 October 2020 16:29 PM
     * order_id : 1016-2387
     * order_delivery_status : cancelled
     * item_count : 1
     * menu_item : [{"id":73881,"item_name":"White Sauce Pasta","item_addons":[{"id":483253,"addon_title":"cheesy "},{"id":483265,"addon_title":"Rock salt"},{"id":483266,"addon_title":"Green chilli"},{"id":483254,"addon_title":"pepsi 250ml"},{"id":483255,"addon_title":"Half"},{"id":483256,"addon_title":"Full"}]}]
     */

    private List<OrdersBean> orders;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OrdersBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersBean> orders) {
        this.orders = orders;
    }

    public static class OrdersBean {
        private int id;
        private double sub_total;
        private String placed_at;
        private String order_id;
        private String order_delivery_status;
        private int item_count;
        private boolean isIs_rejected;
        /**
         * id : 73881
         * item_name : White Sauce Pasta
         * item_addons : [{"id":483253,"addon_title":"cheesy "},{"id":483265,"addon_title":"Rock salt"},{"id":483266,"addon_title":"Green chilli"},{"id":483254,"addon_title":"pepsi 250ml"},{"id":483255,"addon_title":"Half"},{"id":483256,"addon_title":"Full"}]
         */

        private List<MenuItemBean> menu_item;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getSub_total() {
            return sub_total;
        }

        public void setSub_total(double sub_total) {
            this.sub_total = sub_total;
        }

        public String getPlaced_at() {
            return placed_at;
        }

        public void setPlaced_at(String placed_at) {
            this.placed_at = placed_at;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_delivery_status() {
            return order_delivery_status;
        }

        public void setOrder_delivery_status(String order_delivery_status) {
            this.order_delivery_status = order_delivery_status;
        }

        public int getItem_count() {
            return item_count;
        }

        public void setItem_count(int item_count) {
            this.item_count = item_count;
        }

        public List<MenuItemBean> getMenu_item() {
            return menu_item;
        }

        public void setMenu_item(List<MenuItemBean> menu_item) {
            this.menu_item = menu_item;
        }

        public boolean isIs_rejected() {
            return isIs_rejected;
        }

        public void setIs_rejected(boolean is_rejected) {
            isIs_rejected = is_rejected;
        }

        public static class MenuItemBean {
            private int id;
            private String item_name;
            /**
             * id : 483253
             * addon_title : cheesy
             */

            private List<ItemAddonsBean> item_addons;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public List<ItemAddonsBean> getItem_addons() {
                return item_addons;
            }

            public void setItem_addons(List<ItemAddonsBean> item_addons) {
                this.item_addons = item_addons;
            }

            public static class ItemAddonsBean {
                private int id;
                private String addon_title;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getAddon_title() {
                    return addon_title;
                }

                public void setAddon_title(String addon_title) {
                    this.addon_title = addon_title;
                }
            }
        }
    }
}
