package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class AllTicketModel {


    /**
     * code : 200
     * message : success
     * issues_list : [{"id":7,"message":"hiii this is first","status":"open","order_id":null,"image":"","issue_created_at":"23:05 PM 10 September ,2020"},{"id":8,"message":"hiii this is first","status":"open","order_id":null,"image":"","issue_created_at":"23:09 PM 10 September ,2020"}]
     */

    private int code;
    private String message;
    private List<IssuesListBean> issues_list;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IssuesListBean> getIssues_list() {
        return issues_list;
    }

    public void setIssues_list(List<IssuesListBean> issues_list) {
        this.issues_list = issues_list;
    }

    public static class IssuesListBean {
        /**
         * id : 7
         * message : hiii this is first
         * status : open
         * order_id : null
         * image :
         * issue_created_at : 23:05 PM 10 September ,2020
         */

        private int id;
        private String message;
        private String status;
        private String order_id;
        private String image;
        private String issue_created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIssue_created_at() {
            return issue_created_at;
        }

        public void setIssue_created_at(String issue_created_at) {
            this.issue_created_at = issue_created_at;
        }
    }
}