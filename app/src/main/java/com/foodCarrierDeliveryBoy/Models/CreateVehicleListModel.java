package com.foodCarrierDeliveryBoy.Models;

public class CreateVehicleListModel {


    /**
     * code : 200
     * message : success
     * vehicle_detail : {"id":null,"vehicle_maker":"Rohit","registration_number":"12345weer12","dl_number":"14323454","dl_expiry":"15-09-2020","dl_front_image":"","dl_back_image":"","user_id":842,"vehicle_id":1}
     */

    private int code;
    private String message;
    private VehicleDetailBean vehicle_detail;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public VehicleDetailBean getVehicle_detail() {
        return vehicle_detail;
    }

    public void setVehicle_detail(VehicleDetailBean vehicle_detail) {
        this.vehicle_detail = vehicle_detail;
    }

    public static class VehicleDetailBean {
        /**
         * id : null
         * vehicle_maker : Rohit
         * registration_number : 12345weer12
         * dl_number : 14323454
         * dl_expiry : 15-09-2020
         * dl_front_image :
         * dl_back_image :
         * user_id : 842
         * vehicle_id : 1
         */

        private Object id;
        private String vehicle_maker;
        private String registration_number;
        private String dl_number;
        private String dl_expiry;
        private String dl_front_image;
        private String dl_back_image;
        private int user_id;
        private int vehicle_id;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public String getVehicle_maker() {
            return vehicle_maker;
        }

        public void setVehicle_maker(String vehicle_maker) {
            this.vehicle_maker = vehicle_maker;
        }

        public String getRegistration_number() {
            return registration_number;
        }

        public void setRegistration_number(String registration_number) {
            this.registration_number = registration_number;
        }

        public String getDl_number() {
            return dl_number;
        }

        public void setDl_number(String dl_number) {
            this.dl_number = dl_number;
        }

        public String getDl_expiry() {
            return dl_expiry;
        }

        public void setDl_expiry(String dl_expiry) {
            this.dl_expiry = dl_expiry;
        }

        public String getDl_front_image() {
            return dl_front_image;
        }

        public void setDl_front_image(String dl_front_image) {
            this.dl_front_image = dl_front_image;
        }

        public String getDl_back_image() {
            return dl_back_image;
        }

        public void setDl_back_image(String dl_back_image) {
            this.dl_back_image = dl_back_image;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getVehicle_id() {
            return vehicle_id;
        }

        public void setVehicle_id(int vehicle_id) {
            this.vehicle_id = vehicle_id;
        }
    }
}
