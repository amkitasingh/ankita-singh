package com.foodCarrierDeliveryBoy.Models;

public class AddTicketModel {


    /**
     * code : 200
     * message : success
     * ticket : {"id":15,"message":"hiii this is first","status":"open","order_id":null,"image":"","issue_created_at":"16:38 PM 11 September ,2020"}
     */

    private int code;
    private String message;
    private TicketBean ticket;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TicketBean getTicket() {
        return ticket;
    }

    public void setTicket(TicketBean ticket) {
        this.ticket = ticket;
    }

    public static class TicketBean {
        /**
         * id : 15
         * message : hiii this is first
         * status : open
         * order_id : null
         * image :
         * issue_created_at : 16:38 PM 11 September ,2020
         */

        private int id;
        private String message;
        private String status;
        private Object order_id;
        private String image;
        private String issue_created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getOrder_id() {
            return order_id;
        }

        public void setOrder_id(Object order_id) {
            this.order_id = order_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getIssue_created_at() {
            return issue_created_at;
        }

        public void setIssue_created_at(String issue_created_at) {
            this.issue_created_at = issue_created_at;
        }
    }
}