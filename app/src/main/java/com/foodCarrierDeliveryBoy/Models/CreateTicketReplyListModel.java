package com.foodCarrierDeliveryBoy.Models;


public class CreateTicketReplyListModel {


    /**
     * code : 200
     * message : success
     * reply : {"id":17,"reply":"fine","reply_by":null,"support_id":8,"user_id":842,"reply_created_at":"15:16 PM","image":null}
     */

    private int code;
    private String message;
    private ReplyBean reply;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReplyBean getReply() {
        return reply;
    }

    public void setReply(ReplyBean reply) {
        this.reply = reply;
    }

    public static class ReplyBean {
        /**
         * id : 17
         * reply : fine
         * reply_by : null
         * support_id : 8
         * user_id : 842
         * reply_created_at : 15:16 PM
         * image : null
         */

        private int id;
        private String reply;
        private Object reply_by;
        private int support_id;
        private int user_id;
        private String reply_created_at;
        private Object image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getReply() {
            return reply;
        }

        public void setReply(String reply) {
            this.reply = reply;
        }

        public Object getReply_by() {
            return reply_by;
        }

        public void setReply_by(Object reply_by) {
            this.reply_by = reply_by;
        }

        public int getSupport_id() {
            return support_id;
        }

        public void setSupport_id(int support_id) {
            this.support_id = support_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getReply_created_at() {
            return reply_created_at;
        }

        public void setReply_created_at(String reply_created_at) {
            this.reply_created_at = reply_created_at;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }
    }
}
