package com.foodCarrierDeliveryBoy.Models;

public class ModelSuccess {


    /**
     * code : 200
     * message : success
     * status : true
     */

    private int code;
    private String message;
    private boolean status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

