package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class ModelAcceptedOrders {


    /**
     * code : 200
     * message : success
     * branch_orders : [{"id":2514,"fname":"ajay ","lname":null,"address_type":"B-053, B Block, Sector 63, Noida, Uttar Pradesh 201301, India","block":null,"street":"b-52","building":null,"floor":null,"apartment_number":null,"additional_direction":"Noida","latitude":"28.616332918931327","longitude":"77.37724307924509","delivery_amount":6,"order_id":"1029-2514","order_delivery_status":"accepted"},{"id":2515,"fname":"ajay ","lname":null,"address_type":"B-053, B Block, Sector 63, Noida, Uttar Pradesh 201301, India","block":null,"street":"b-52","building":null,"floor":null,"apartment_number":null,"additional_direction":"Noida","latitude":"28.616332918931327","longitude":"77.37724307924509","delivery_amount":6,"order_id":"1029-2515","order_delivery_status":"accepted"},{"id":2522,"fname":"ajay ","lname":null,"address_type":"B-053, B Block, Sector 63, Noida, Uttar Pradesh 201301, India","block":null,"street":"b-52","building":null,"floor":null,"apartment_number":null,"additional_direction":"Noida","latitude":"28.616332918931327","longitude":"77.37724307924509","delivery_amount":6,"order_id":"1029-2522","order_delivery_status":"accepted"},{"id":2523,"fname":"Ankesh Kuma","lname":null,"address_type":"Swarn Jayanti Park, Sector 10, Rohini, Delhi 110085, India","block":null,"street":"b 23","building":null,"floor":null,"apartment_number":null,"additional_direction":"Rohini","latitude":"28.715121","longitude":"77.1155303","delivery_amount":6,"order_id":"1029-2523","order_delivery_status":"accepted"}]
     * branch : {"id":1,"address":"H Block Sector 63, Noida, Uttar Pradesh, India","latitude":"28.6177599","longitude":"77.3774354","restaurant_name":"Rishoos"}
     */

    private int code;
    private String message;
    /**
     * id : 1
     * address : H Block Sector 63, Noida, Uttar Pradesh, India
     * latitude : 28.6177599
     * longitude : 77.3774354
     * restaurant_name : Rishoos
     */

    private BranchBean branch;
    /**
     * id : 2514
     * fname : ajay
     * lname : null
     * address_type : B-053, B Block, Sector 63, Noida, Uttar Pradesh 201301, India
     * block : null
     * street : b-52
     * building : null
     * floor : null
     * apartment_number : null
     * additional_direction : Noida
     * latitude : 28.616332918931327
     * longitude : 77.37724307924509
     * delivery_amount : 6
     * order_id : 1029-2514
     * order_delivery_status : accepted
     */

    private List<BranchOrdersBean> branch_orders;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BranchBean getBranch() {
        return branch;
    }

    public void setBranch(BranchBean branch) {
        this.branch = branch;
    }

    public List<BranchOrdersBean> getBranch_orders() {
        return branch_orders;
    }

    public void setBranch_orders(List<BranchOrdersBean> branch_orders) {
        this.branch_orders = branch_orders;
    }

    public static class BranchBean {
        private int id;
        private String address;
        private String latitude;
        private String longitude;
        private String restaurant_name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }
    }

    public static class BranchOrdersBean {
        private int id;
        private String fname;
        private Object lname;
        private String address_type;
        private Object block;
        private String street;
        private Object building;
        private Object floor;
        private Object apartment_number;
        private String additional_direction;
        private String latitude;
        private String longitude;
        private int delivery_amount;
        private String order_id;
        private String order_delivery_status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public Object getLname() {
            return lname;
        }

        public void setLname(Object lname) {
            this.lname = lname;
        }

        public String getAddress_type() {
            return address_type;
        }

        public void setAddress_type(String address_type) {
            this.address_type = address_type;
        }

        public Object getBlock() {
            return block;
        }

        public void setBlock(Object block) {
            this.block = block;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public Object getBuilding() {
            return building;
        }

        public void setBuilding(Object building) {
            this.building = building;
        }

        public Object getFloor() {
            return floor;
        }

        public void setFloor(Object floor) {
            this.floor = floor;
        }

        public Object getApartment_number() {
            return apartment_number;
        }

        public void setApartment_number(Object apartment_number) {
            this.apartment_number = apartment_number;
        }

        public String getAdditional_direction() {
            return additional_direction;
        }

        public void setAdditional_direction(String additional_direction) {
            this.additional_direction = additional_direction;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public int getDelivery_amount() {
            return delivery_amount;
        }

        public void setDelivery_amount(int delivery_amount) {
            this.delivery_amount = delivery_amount;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_delivery_status() {
            return order_delivery_status;
        }

        public void setOrder_delivery_status(String order_delivery_status) {
            this.order_delivery_status = order_delivery_status;
        }
    }
}