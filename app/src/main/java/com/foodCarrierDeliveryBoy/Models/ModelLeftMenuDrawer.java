package com.foodCarrierDeliveryBoy.Models;

public class ModelLeftMenuDrawer {

    private  String titles;
    private  int icons;
    private boolean isExpanded =false;

    public ModelLeftMenuDrawer(String titles, int icons, boolean isExpanded){

        this.titles = titles;
        this.icons = icons;
        this.isExpanded = isExpanded;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public int getIcons() {
        return icons;
    }

    public void setIcons(int icons) {
        this.icons = icons;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
