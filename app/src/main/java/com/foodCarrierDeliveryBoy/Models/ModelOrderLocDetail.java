package com.foodCarrierDeliveryBoy.Models;

public class ModelOrderLocDetail {

    /**
     * code : 200
     * order : {"id":2223,"fname":"rohit","lname":"chauhan","address_type":"dhampur sugar mill","block":null,"street":null,"building":null,"floor":null,"apartment_number":null,"additional_direction":null,"delivery_amount":0,"delivery_distance":null,"delivery_time":"10-15"}
     * branch : {"id":1,"address":"Dhampur Sugar Mills Ltd., Unnamed Road, Dhampur, Uttar Pradesh 246761, India","restaurant_name":"Rishoos","restaurant_distance":null,"delivery_time":"5-10"}
     */

    private int code;
    private OrderBean order;
    private BranchBean branch;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public BranchBean getBranch() {
        return branch;
    }

    public void setBranch(BranchBean branch) {
        this.branch = branch;
    }

    public static class OrderBean {
        /**
         * id : 2223
         * fname : rohit
         * lname : chauhan
         * address_type : dhampur sugar mill
         * block : null
         * street : null
         * building : null
         * floor : null
         * apartment_number : null
         * additional_direction : null
         * delivery_amount : 0
         * delivery_distance : null
         * delivery_time : 10-15
         */

        private int id;
        private String fname;
        private String lname;
        private String address_type;
        private String block;
        private String street;
        private String building;
        private String floor;
        private String apartment_number;
        private String additional_direction;
        private int delivery_amount;
        private String delivery_distance;
        private String delivery_time;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getAddress_type() {
            return address_type;
        }

        public void setAddress_type(String address_type) {
            this.address_type = address_type;
        }

        public String getBlock() {
            return block;
        }

        public void setBlock(String block) {
            this.block = block;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getBuilding() {
            return building;
        }

        public void setBuilding(String building) {
            this.building = building;
        }

        public String getFloor() {
            return floor;
        }

        public void setFloor(String floor) {
            this.floor = floor;
        }

        public String getApartment_number() {
            return apartment_number;
        }

        public void setApartment_number(String apartment_number) {
            this.apartment_number = apartment_number;
        }

        public String getAdditional_direction() {
            return additional_direction;
        }

        public void setAdditional_direction(String additional_direction) {
            this.additional_direction = additional_direction;
        }

        public int getDelivery_amount() {
            return delivery_amount;
        }

        public void setDelivery_amount(int delivery_amount) {
            this.delivery_amount = delivery_amount;
        }

        public String getDelivery_distance() {
            return delivery_distance;
        }

        public void setDelivery_distance(String delivery_distance) {
            this.delivery_distance = delivery_distance;
        }

        public String getDelivery_time() {
            return delivery_time;
        }

        public void setDelivery_time(String delivery_time) {
            this.delivery_time = delivery_time;
        }
    }

    public static class BranchBean {
        /**
         * id : 1
         * address : Dhampur Sugar Mills Ltd., Unnamed Road, Dhampur, Uttar Pradesh 246761, India
         * restaurant_name : Rishoos
         * restaurant_distance : null
         * delivery_time : 5-10
         */

        private int id;
        private String address;
        private String restaurant_name;
        private String restaurant_distance;
        private String delivery_time;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getRestaurant_distance() {
            return restaurant_distance;
        }

        public void setRestaurant_distance(String restaurant_distance) {
            this.restaurant_distance = restaurant_distance;
        }

        public String getDelivery_time() {
            return delivery_time;
        }

        public void setDelivery_time(String delivery_time) {
            this.delivery_time = delivery_time;
        }
    }
}
