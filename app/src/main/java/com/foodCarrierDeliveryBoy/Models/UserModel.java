package com.foodCarrierDeliveryBoy.Models;

public class UserModel {


    /**
     * code : 200
     * user : {"id":872,"image":null,"name":"ankita singh","email":"anku@12345","contact":"6394508274","country_code":"91","session_token":"f2eVfiqnG94rprrEckHB","is_loggedIn":true,"is_signup":false,"selected_vehicle":{"id":3,"name":"car","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231090/offer/povnmwgvkuprngo6dsjr.png","range":25}}
     */

    private int code;
    /**
     * id : 872
     * image : null
     * name : ankita singh
     * email : anku@12345
     * contact : 6394508274
     * country_code : 91
     * session_token : f2eVfiqnG94rprrEckHB
     * is_loggedIn : true
     * is_signup : false
     * selected_vehicle : {"id":3,"name":"car","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231090/offer/povnmwgvkuprngo6dsjr.png","range":25}
     */

    private UserBean user;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public static class UserBean {
        private int id;
        private Object image;
        private String name;
        private String email;
        private String contact;
        private String country_code;
        private String session_token;
        private boolean is_loggedIn;
        private boolean is_signup;
        /**
         * id : 3
         * name : car
         * image : http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231090/offer/povnmwgvkuprngo6dsjr.png
         * range : 25.0
         */

        private SelectedVehicleBean selected_vehicle;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getSession_token() {
            return session_token;
        }

        public void setSession_token(String session_token) {
            this.session_token = session_token;
        }

        public boolean isIs_loggedIn() {
            return is_loggedIn;
        }

        public void setIs_loggedIn(boolean is_loggedIn) {
            this.is_loggedIn = is_loggedIn;
        }

        public boolean isIs_signup() {
            return is_signup;
        }

        public void setIs_signup(boolean is_signup) {
            this.is_signup = is_signup;
        }

        public SelectedVehicleBean getSelected_vehicle() {
            return selected_vehicle;
        }

        public void setSelected_vehicle(SelectedVehicleBean selected_vehicle) {
            this.selected_vehicle = selected_vehicle;
        }

        public static class SelectedVehicleBean {
            private String id;
            private String name;
            private String image;
            private double range;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public double getRange() {
                return range;
            }

            public void setRange(double range) {
                this.range = range;
            }
        }
    }
}
