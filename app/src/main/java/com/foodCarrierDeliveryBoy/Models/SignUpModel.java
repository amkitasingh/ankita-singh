package com.foodCarrierDeliveryBoy.Models;

public class SignUpModel {


    /**
     * code : 200
     * user : {"id":850,"name":"ankita","contact":"8726873050","country_code":"91","image":null,"title":"Miss","session_token":"FVcVbzzhUAkSNsh_XxDC"}
     */

    private int code;
    private UserBean user;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public static class UserBean {
        /**
         * id : 850
         * name : ankita
         * contact : 8726873050
         * country_code : 91
         * image : null
         * title : Miss
         * session_token : FVcVbzzhUAkSNsh_XxDC
         */

        private int id;
        private String name;
        private String contact;
        private String country_code;
        private Object image;
        private String title;
        private String session_token;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSession_token() {
            return session_token;
        }

        public void setSession_token(String session_token) {
            this.session_token = session_token;
        }
    }
}