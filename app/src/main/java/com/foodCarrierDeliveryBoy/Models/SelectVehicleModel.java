package com.foodCarrierDeliveryBoy.Models;

public class SelectVehicleModel {

    /**
     * code : 200
     * message : success
     * selected_vehicle : {"id":2,"name":"bike","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231062/offer/vpjgojbpswhlhs8bmydf.jpg","range":25}
     */

    private int code;
    private String message;
    private SelectedVehicleBean selected_vehicle;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SelectedVehicleBean getSelected_vehicle() {
        return selected_vehicle;
    }

    public void setSelected_vehicle(SelectedVehicleBean selected_vehicle) {
        this.selected_vehicle = selected_vehicle;
    }

    public static class SelectedVehicleBean {
        /**
         * id : 2
         * name : bike
         * image : http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231062/offer/vpjgojbpswhlhs8bmydf.jpg
         * range : 25.0
         */

        private String id;
        private String name;
        private String image;
        private double range;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public double getRange() {
            return range;
        }

        public void setRange(double range) {
            this.range = range;
        }
    }
}
