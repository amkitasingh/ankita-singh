package com.foodCarrierDeliveryBoy.Models;

public class ModelOnlineOffline {

    /**
     * code : 200
     * is_online : true
     */

    private int code;
    private boolean is_online;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isIs_online() {
        return is_online;
    }

    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }
}
