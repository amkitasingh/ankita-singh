package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class CreatedVehicleListModel {


    /**
     * code : 200
     * message : success
     * vehicle_list : [{"id":5,"name":"walk","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600232811/offer/m8chyl9sazuhfccb6ete.png","range":25},{"id":4,"name":"bicycle","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231189/offer/k4zybmgimcqlrdkeymmh.jpg","range":25}]
     * selected_vehicle : {"id":3,"name":"car","image":"http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231090/offer/povnmwgvkuprngo6dsjr.png","range":25}
     */

    private int code;
    private String message;
    private SelectedVehicleBean selected_vehicle;
    private List<VehicleListBean> vehicle_list;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SelectedVehicleBean getSelected_vehicle() {
        return selected_vehicle;
    }

    public void setSelected_vehicle(SelectedVehicleBean selected_vehicle) {
        this.selected_vehicle = selected_vehicle;
    }

    public List<VehicleListBean> getVehicle_list() {
        return vehicle_list;
    }

    public void setVehicle_list(List<VehicleListBean> vehicle_list) {
        this.vehicle_list = vehicle_list;
    }

    public static class SelectedVehicleBean {
        /**
         * id : 3
         * name : car
         * image : http://res.cloudinary.com/suyash-temprorary/image/upload/v1600231090/offer/povnmwgvkuprngo6dsjr.png
         * range : 25.0
         */

        private String id;
        private String name;
        private String image;
        private double range;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public double getRange() {
            return range;
        }

        public void setRange(double range) {
            this.range = range;
        }
    }

    public static class VehicleListBean {
        /**
         * id : 5
         * name : walk
         * image : http://res.cloudinary.com/suyash-temprorary/image/upload/v1600232811/offer/m8chyl9sazuhfccb6ete.png
         * range : 25.0
         */

        private int id;
        private String name;
        private String image;
        private double range;
        private  boolean isSelected= false;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public double getRange() {
            return range;
        }

        public void setRange(double range) {
            this.range = range;
        }


        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}