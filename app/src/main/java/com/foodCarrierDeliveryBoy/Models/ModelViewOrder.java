package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class ModelViewOrder {


    /**
     * code : 200
     * message : success
     * order_details : {"id":2223,"order_note":"N/A","restaurantTitle":"Rishoos",
     * "menu_item":[{"id":73758,"item_name":"Rasgulla","quantity":null,
     * "item_addons":[{"id":483257,"addon_title":"extra suger"}]}
     * ,{"id":73807,"item_name":"Jalabi","quantity":null,
     * "item_addons":[{"id":483257,"addon_title":"extra suger"}]}]}
     */

    private int code;
    private String message;
    private OrderDetailsBean order_details;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrderDetailsBean getOrder_details() {
        return order_details;
    }

    public void setOrder_details(OrderDetailsBean order_details) {
        this.order_details = order_details;
    }

    public static class OrderDetailsBean {
        /**
         * id : 2223
         * order_note : N/A
         * restaurantTitle : Rishoos
         * menu_item : [{"id":73758,"item_name":"Rasgulla","quantity":null,"item_addons":[{"id":483257,"addon_title":"extra suger"}]},
         * {"id":73807,"item_name":"Jalabi","quantity":null,"item_addons":[{"id":483257,"addon_title":"extra suger"}]}]
         */

        private int id;
        private String order_note;
        private String restaurantTitle;
        private List<MenuItemBean> menu_item;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOrder_note() {
            return order_note;
        }

        public void setOrder_note(String order_note) {
            this.order_note = order_note;
        }

        public String getRestaurantTitle() {
            return restaurantTitle;
        }

        public void setRestaurantTitle(String restaurantTitle) {
            this.restaurantTitle = restaurantTitle;
        }

        public List<MenuItemBean> getMenu_item() {
            return menu_item;
        }

        public void setMenu_item(List<MenuItemBean> menu_item) {
            this.menu_item = menu_item;
        }

        public static class MenuItemBean {
            /**
             * id : 73758
             * item_name : Rasgulla
             * quantity : null
             * item_addons : [{"id":483257,"addon_title":"extra suger"}]
             */

            private int id;
            private String item_name;
            private String quantity;
            private List<ItemAddonsBean> item_addons;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }



            public List<ItemAddonsBean> getItem_addons() {
                return item_addons;
            }

            public void setItem_addons(List<ItemAddonsBean> item_addons) {
                this.item_addons = item_addons;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public static class ItemAddonsBean {
                /**
                 * id : 483257
                 * addon_title : extra suger
                 */

                private int id;
                private String addon_title;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getAddon_title() {
                    return addon_title;
                }

                public void setAddon_title(String addon_title) {
                    this.addon_title = addon_title;
                }
            }
        }
    }
}
