package com.foodCarrierDeliveryBoy.Models;

import java.io.Serializable;

public class TransportCreatedModel {


    /**
     * code : 200
     * message : success
     * vehicle_detail : {"id":17,"vehicle_maker":"Rohit","registration_number":"12345weer12","dl_number":"14323454","dl_expiry":"15-09-2020","dl_front_image":null,"dl_back_image":null,"user_id":872,"vehicle_id":3}
     */

    private Integer code;
    private String message;
    private VehicleDetailBean vehicle_detail;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public VehicleDetailBean getVehicle_detail() {
        return vehicle_detail;
    }

    public void setVehicle_detail(VehicleDetailBean vehicle_detail) {
        this.vehicle_detail = vehicle_detail;
    }

    public static class VehicleDetailBean implements Serializable {
        /**
         * id : 17
         * vehicle_maker : Rohit
         * registration_number : 12345weer12
         * dl_number : 14323454
         * dl_expiry : 15-09-2020
         * dl_front_image : null
         * dl_back_image : null
         * user_id : 872
         * vehicle_id : 3
         */

        private Integer id;
        private String vehicle_maker;
        private String registration_number;
        private String dl_number;
        private String dl_expiry;
        private Object dl_front_image;
        private Object dl_back_image;
        private Integer user_id;
        private Integer vehicle_id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getVehicle_maker() {
            return vehicle_maker;
        }

        public void setVehicle_maker(String vehicle_maker) {
            this.vehicle_maker = vehicle_maker;
        }

        public String getRegistration_number() {
            return registration_number;
        }

        public void setRegistration_number(String registration_number) {
            this.registration_number = registration_number;
        }

        public String getDl_number() {
            return dl_number;
        }

        public void setDl_number(String dl_number) {
            this.dl_number = dl_number;
        }

        public String getDl_expiry() {
            return dl_expiry;
        }

        public void setDl_expiry(String dl_expiry) {
            this.dl_expiry = dl_expiry;
        }

        public Object getDl_front_image() {
            return dl_front_image;
        }

        public void setDl_front_image(Object dl_front_image) {
            this.dl_front_image = dl_front_image;
        }

        public Object getDl_back_image() {
            return dl_back_image;
        }

        public void setDl_back_image(Object dl_back_image) {
            this.dl_back_image = dl_back_image;
        }

        public Integer getUser_id() {
            return user_id;
        }

        public void setUser_id(Integer user_id) {
            this.user_id = user_id;
        }

        public Integer getVehicle_id() {
            return vehicle_id;
        }

        public void setVehicle_id(Integer vehicle_id) {
            this.vehicle_id = vehicle_id;
        }
    }
}
