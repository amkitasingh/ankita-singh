package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class EarningModel {

    /**
     * code : 200
     * message : success
     * today_earnings : {"id":5,"online_time":"00 hr:59 mins","distance":"0 km","earning_graph":[{"trip":"1","earning":""}],"total_earning":0,"total_trips":1,"avg_earning":0,"avg_time":"30 min","avg_distance":"4 km","wallet_amount":"100"}
     */

    private int code;
    private String message;
    private TodayEarningsBean today_earnings;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TodayEarningsBean getToday_earnings() {
        return today_earnings;
    }

    public void setToday_earnings(TodayEarningsBean today_earnings) {
        this.today_earnings = today_earnings;
    }

    public static class TodayEarningsBean {
        /**
         * id : 5
         * online_time : 00 hr:59 mins
         * distance : 0 km
         * earning_graph : [{"trip":"1","earning":""}]
         * total_earning : 0.0
         * total_trips : 1
         * avg_earning : 0.0
         * avg_time : 30 min
         * avg_distance : 4 km
         * wallet_amount : 100
         */

        private int id;
        private String online_time;
        private String distance;
        private double total_earning;
        private int total_trips;
        private double avg_earning;
        private String avg_time;
        private String avg_distance;
        private String wallet_amount;
        private List<EarningGraphBean> earning_graph;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOnline_time() {
            return online_time;
        }

        public void setOnline_time(String online_time) {
            this.online_time = online_time;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public double getTotal_earning() {
            return total_earning;
        }

        public void setTotal_earning(double total_earning) {
            this.total_earning = total_earning;
        }

        public int getTotal_trips() {
            return total_trips;
        }

        public void setTotal_trips(int total_trips) {
            this.total_trips = total_trips;
        }

        public double getAvg_earning() {
            return avg_earning;
        }

        public void setAvg_earning(double avg_earning) {
            this.avg_earning = avg_earning;
        }

        public String getAvg_time() {
            return avg_time;
        }

        public void setAvg_time(String avg_time) {
            this.avg_time = avg_time;
        }

        public String getAvg_distance() {
            return avg_distance;
        }

        public void setAvg_distance(String avg_distance) {
            this.avg_distance = avg_distance;
        }

        public String getWallet_amount() {
            return wallet_amount;
        }

        public void setWallet_amount(String wallet_amount) {
            this.wallet_amount = wallet_amount;
        }

        public List<EarningGraphBean> getEarning_graph() {
            return earning_graph;
        }

        public void setEarning_graph(List<EarningGraphBean> earning_graph) {
            this.earning_graph = earning_graph;
        }

        public static class EarningGraphBean {
            /**
             * trip : 1
             * earning :
             */

            private String trip;
            private String earning;

            public String getTrip() {
                return trip;
            }

            public void setTrip(String trip) {
                this.trip = trip;
            }

            public String getEarning() {
                return earning;
            }

            public void setEarning(String earning) {
                this.earning = earning;
            }
        }
    }
}
