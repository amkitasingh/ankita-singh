package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class NotificationModel {


    /**
     * code : 200
     * notifications : [{"id":9257,"message":"There are many variations of passages of lorem ipsum available,but the majority","notification_type":null,"status":false,"order_id":null,"created_at":"2020-09-12T15:16:32.000+05:30"},{"id":9256,"message":"There are many variations of passages of lorem ipsum available,but the majority","notification_type":null,"status":false,"order_id":null,"created_at":"2020-09-12T15:16:31.000+05:30"},{"id":9251,"message":"There are many variations of passages of lorem ipsum available,but the majority","notification_type":null,"status":false,"order_id":null,"created_at":"2020-09-12T12:58:41.000+05:30"}]
     */

    private int code;
    private List<NotificationsBean> notifications;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<NotificationsBean> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationsBean> notifications) {
        this.notifications = notifications;
    }

    public static class NotificationsBean {
        /**
         * id : 9257
         * message : There are many variations of passages of lorem ipsum available,but the majority
         * notification_type : null
         * status : false
         * order_id : null
         * created_at : 2020-09-12T15:16:32.000+05:30
         */

        private int id;
        private String message;
        private Object notification_type;
        private boolean status;
        private Object order_id;
        private String created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getNotification_type() {
            return notification_type;
        }

        public void setNotification_type(Object notification_type) {
            this.notification_type = notification_type;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public Object getOrder_id() {
            return order_id;
        }

        public void setOrder_id(Object order_id) {
            this.order_id = order_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
