package com.foodCarrierDeliveryBoy.Models;

import java.util.List;

public class ModelChatAllMessage {

    /**
     * code : 200
     * message : success
     * ticket : [{"id":63,"reply":"hiii this is first","user_id":871,"reply_created_at":"11:28 AM","is_image":false,"image":null},{"id":64,"reply":"https://res.cloudinary.com/dygrpgl2q/image/upload/v1600235892/ticket_image/agwe5e08dnscghabs8ev.png","user_id":871,"reply_created_at":"11:28 AM","is_image":true,"image":null}]
     */

    private int code;
    private String message;
    private List<TicketBean> ticket;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TicketBean> getTicket() {
        return ticket;
    }

    public void setTicket(List<TicketBean> ticket) {
        this.ticket = ticket;
    }

    public static class TicketBean {
        /**
         * id : 63
         * reply : hiii this is first
         * user_id : 871
         * reply_created_at : 11:28 AM
         * is_image : false
         * image : null
         */

        private int id;
        private String reply;
        private int user_id;
        private String reply_created_at;
        private boolean is_image;
        private Object image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getReply() {
            return reply;
        }

        public void setReply(String reply) {
            this.reply = reply;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getReply_created_at() {
            return reply_created_at;
        }

        public void setReply_created_at(String reply_created_at) {
            this.reply_created_at = reply_created_at;
        }

        public boolean isIs_image() {
            return is_image;
        }

        public void setIs_image(boolean is_image) {
            this.is_image = is_image;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }
    }
}
