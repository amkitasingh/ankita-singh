package com.foodCarrierDeliveryBoy.Models;

public class PasswordTokenModel {


    /**
     * code : 200
     * password_token : EUy8PUzTroKLm52Jicq8
     */

    private int code;
    private String password_token;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPassword_token() {
        return password_token;
    }

    public void setPassword_token(String password_token) {
        this.password_token = password_token;
    }
}

