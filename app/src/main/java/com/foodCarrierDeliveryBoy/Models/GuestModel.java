package com.foodCarrierDeliveryBoy.Models;

public class GuestModel {


    /**
     * code : 200
     * guest_token : idRceqyLSiqHPe7eatmB
     */

    private int code;
    private String guest_token;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getGuest_token() {
        return guest_token;
    }

    public void setGuest_token(String guest_token) {
        this.guest_token = guest_token;
    }
}
