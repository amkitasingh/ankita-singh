package com.foodCarrierDeliveryBoy.Fragments;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodCarrierDeliveryBoy.Adapters.OrderListSecondAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Models.ModelViewOrder;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityPickUpOrderItemBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FragmentPickUpOrderItem extends BaseFragment {
    private ActivityPickUpOrderItemBinding binding;
    private Context context;
    Callback callback;
    private String order_id;
    ArrayList<ModelViewOrder.OrderDetailsBean.MenuItemBean> list=new ArrayList<>();



    public FragmentPickUpOrderItem(Context  context, Callback callback,String order_id) {

        this.context = context;
        this.callback = callback;
        this.order_id = order_id;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_pick_up_order_item, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        binding.constraintLayout8.setBackgroundColor(getResources().getColor(R.color.white));
        binding.viewOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickedOnPickUp( order_id);
            }
        });
        hitApiViewOrder();

    }

    private void hitApiViewOrder() {
     /*   if (!isFinishing() && !dialog.isShowing()) {
            dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }*/
        HashMap<String, String> map = new HashMap<>();
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("order_id",order_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ;

        Authentication.object(context, UrlFactory.generateUrlWithVersion(AppConstants.URL_VIEW_ORDER),this,jsonObject);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if(responseObj instanceof ModelViewOrder){
            ModelViewOrder modelViewOrder= (ModelViewOrder) responseObj;
            list.clear();
            list.addAll(modelViewOrder.getOrder_details().getMenu_item());
            OrderListSecondAdapter orderListSecondAdapter = new OrderListSecondAdapter(context,list,"pickUpOrderItem");
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerView.setAdapter(orderListSecondAdapter);
        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
    public interface  Callback{
        void  onClickedOnPickUp(String order_id);
    }
}
