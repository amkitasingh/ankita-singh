package com.foodCarrierDeliveryBoy.Fragments;


import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Models.EarningModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.FragmentWeekBinding;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentWeek extends BaseFragment {
FragmentWeekBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_week, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiHitEarning("This week");
    }

    private void apiHitEarning(String value) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("keyword",value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Authentication.object(getActivity(), UrlFactory.generateUrlWithVersion(AppConstants.URL_EARNINGS),this,jsonObject);
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof EarningModel){
            EarningModel earningModel= (EarningModel) responseObj;
            /*   if (earningModel.getToday_earnings().toString().equals("{}")){}*/

                binding.totalEarning.setText("$"+earningModel.getToday_earnings().getTotal_earning());
                binding.tview2.setText(earningModel.getToday_earnings().getOnline_time());
                binding.tview4.setText(earningModel.getToday_earnings().getDistance());
                binding.tview6.setText("$"+earningModel.getToday_earnings().getAvg_earning());
                binding.tview8.setText(earningModel.getToday_earnings().getAvg_time());
                binding.tview7.setText(earningModel.getToday_earnings().getAvg_distance());
                binding.textView18.setText("$"+earningModel.getToday_earnings().getWallet_amount());
             // binding.totalDeliveries.setText(earningModel.getToday_earnings(.get);


        }
    }

}
