package com.foodCarrierDeliveryBoy.Fragments;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Activity.ActivityReachUS;
import com.foodCarrierDeliveryBoy.Adapters.OrderListAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Models.ModelAcceptedOrders;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityFoodDeliveredListBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentFoodDeliveredList extends BaseFragment implements View.OnClickListener, OrderListAdapter.Callback {
    private ActivityFoodDeliveredListBinding binding;
    ArrayList<ModelAcceptedOrders.BranchOrdersBean> list =new ArrayList<>();
    private Context context;
    private final String order_id;

    public FragmentFoodDeliveredList(Context context,String order_id){
        this.context = context;
        this.order_id = order_id;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_food_delivered_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        binding.orderScreen.setBackgroundColor(getResources().getColor(R.color.white));
        getAllAcceptedOrders();
    }

    @Override
    public void onClick( View view) {
        switch (view.getId()) {
            case R.id.userImageChatMsg:
                context.startActivity(new Intent(getActivity(), ActivityReachUS.class));
                break;
            case R.id.viewLoginBtn:
                orderDelivered(order_id);
                //  context.startActivity(new Intent(getActivity(), ActivityOrdersDeliverd.class));
                break;
        }

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof ModelAcceptedOrders){
            list.addAll(((ModelAcceptedOrders) responseObj).getBranch_orders());
            if (list.size()==1){
                binding.viewDeliveredButton.setVisibility(View.VISIBLE);
            }else {
                binding.viewDeliveredButton.setVisibility(View.GONE);

            }
            OrderListAdapter orderListAdapter =new OrderListAdapter(context,list,this,"delivered");
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerView.setAdapter(orderListAdapter);
        }
        if (responseObj instanceof String){
            if (((String) responseObj).equalsIgnoreCase("deliveredsuccessfully")){
                Toast.makeText(context,"Order Delivered Successfully",Toast.LENGTH_SHORT).show();
                context.startActivity(IntentHelper.getDeliveredScreen(context).putExtra("order_id",order_id));
            }
        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    private void getAllAcceptedOrders() {

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("keyword","pickedup");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Authentication.object(context,UrlFactory.generateUrlWithVersion(AppConstants.URL_ALL_ACCEPTED_ORDERS),this,jsonObject);

    }


    void  orderDelivered(String orderId){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("order_id",orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Authentication.object(context, UrlFactory.generateUrlWithVersion(AppConstants.URL_ORDER_DELIVERED),this,jsonObject);
    }

    @Override
    public void onClickonViewOrder(String support_id) {

    }
}
