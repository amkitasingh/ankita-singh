package com.foodCarrierDeliveryBoy.Fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Adapters.OrderListAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Dialogs.PopUpStackOrderDialog;
import com.foodCarrierDeliveryBoy.Models.ModelAcceptedOrders;
import com.foodCarrierDeliveryBoy.Models.ModelOrderLocDetail;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;

import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityOrderScreenBinding;
import com.ebanx.swipebtn.OnActiveListener;

import java.util.ArrayList;
import java.util.HashMap;


public class FragmentOrderScreen extends BaseFragment implements View.OnClickListener,
        PopUpStackOrderDialog.Callback, OrderListAdapter.Callback {
    private ActivityOrderScreenBinding binding;
    private ArrayList<ModelAcceptedOrders.BranchOrdersBean> list = new ArrayList<>();
    private Context context;
    Callback callback;
    int counter, progress;
    private String orderID;
    CountDownTimer countDownTimer;
    ModelAcceptedOrders modelAcceptedOrders;
    ModelOrderLocDetail modelOrderLocDetail;

    public FragmentOrderScreen(Context context, String orderID) {
        this.context = context;
        this.callback = callback;
        this.orderID = orderID;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_order_screen, null);
        binding = DataBindingUtil.bind(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.viewOrderBtnn.setOnClickListener(this);
        binding.viewRestBtnn.setOnClickListener(this);
        // apiHitOrderDetail();
        getAllAcceptedOrders();
        binding.orderScreen.setBackgroundColor(getResources().getColor(R.color.white));
        binding.cancelButtonn.setOnActiveListener(new OnActiveListener() {
            @Override
            public void onActive() {
                apiHitCancelOrder();
                countDounTimer();

            }
        });
    }

    private void apiHitCancelOrder() {
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderID);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_CANCEL_ORDER),
                UrlFactory.getAppHeaders(), this);
    }


    private void getAllAcceptedOrders() {
        HashMap<String, String> map = new HashMap<>();
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ALL_ACCEPTED_ORDERS),
                UrlFactory.getAppHeaders(), this);

    }

    private void hitApiAtRestaurant() {
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderID);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_TRANSPORTER_AT_RESTURANT),
                UrlFactory.getAppHeaders(), this);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof ModelSuccess) {
            ModelSuccess ModelCancel = (ModelSuccess) responseObj;
            if (ModelCancel.getCode() == 200) {
                startActivity(IntentHelper.getDashboard(context));
            }
        } else if (responseObj instanceof ModelAcceptedOrders) {
            modelAcceptedOrders = (ModelAcceptedOrders) responseObj;
            list.addAll(modelAcceptedOrders.getBranch_orders());
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            OrderListAdapter orderListAdapter = new OrderListAdapter(context, list, this, "fragmentOrder");
            binding.recyclerView.setAdapter(orderListAdapter);
            if (list.size() == 1) {
                binding.estTIme.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getRestaurant_name()));
                binding.tvTime.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getAddress()));
                binding.viewOrderBtnn.setVisibility(View.VISIBLE);

            } else if (modelAcceptedOrders == null && String.valueOf(modelAcceptedOrders.getBranch_orders().size()) == null &&
                    !String.valueOf(modelAcceptedOrders.getBranch_orders()).equalsIgnoreCase("")) {
                startActivity(IntentHelper.getDashboard(context));

            } else {
                openDialog();
                binding.estTIme.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getRestaurant_name()));
                binding.tvTime.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getAddress()));
                binding.cancelButtonn.setVisibility(View.VISIBLE);
                binding.viewOrderBtnn.setVisibility(View.INVISIBLE);
                binding.viewRestBtnn.setVisibility(View.VISIBLE);

            }

        } else if (responseObj instanceof String) {
            if (((String) responseObj).equalsIgnoreCase("AtRestaurant")) {
                Toast.makeText(context, "At Restaurant", Toast.LENGTH_SHORT).show();
                startActivity(IntentHelper.getPickOrderScreen(context));
            }
        }

    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    void openDialog() {
        new PopUpStackOrderDialog(context, this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.viewOrderBtnn:
                startActivity(IntentHelper.getOrderDetailScreen(context)
                        .putExtra("order_id", orderID));
                break;

            case R.id.viewRestBtnn:
                hitApiAtRestaurant();
                break;

        }
    }

    @Override
    public void onNeedToSendSignUp() {}

    @Override
    public void onSessionExpire() {}


    @Override
    public void onClickOnAcceptButton() {
        callback.onClickonAcceptOrder("orderScreen");
    }

    @Override
    public void onClickonViewOrder(String support_id) {
        startActivity(IntentHelper.getOrderDetailScreen(context));

    }


    public interface Callback {
        void onClickonCancelOrder(String fromScreen);
        void onClickonAcceptOrder(String fromScreen);
        void onClickOnAtRestaurant();
    }


    void countDounTimer() {
        counter = 60;
        progress = 0;
        countDownTimer = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {

                counter--;
            }

            @Override
            public void onFinish() {
                binding.cancelButtonn.setVisibility(View.GONE);
                binding.viewRestBtnn.setVisibility(View.VISIBLE);
            }

        }.start();
    }
}
