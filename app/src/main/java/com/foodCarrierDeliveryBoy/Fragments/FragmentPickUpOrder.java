package com.foodCarrierDeliveryBoy.Fragments;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Adapters.OrderListAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Models.ModelAcceptedOrders;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityPickUpOrderBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FragmentPickUpOrder extends BaseFragment implements View.OnClickListener, OrderListAdapter.Callback {
    private ActivityPickUpOrderBinding binding;
    private Context context;
    private final Callback callback;
    private String order_id;
    ModelAcceptedOrders modelAcceptedOrders;
    ArrayList<ModelAcceptedOrders.BranchOrdersBean> list=new ArrayList();


    public FragmentPickUpOrder(Context context, Callback callback,String order_id){

        this.context = context;
        this.callback = callback;
        this.order_id = order_id;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_pick_up_order, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        binding.orderScreen.setBackgroundColor(getResources().getColor(R.color.white));

        binding.viewPickUpButton.setOnClickListener(this);

        getAllAcceptedOrders();


    }
    private void getAllAcceptedOrders() {
        HashMap<String, String> map = new HashMap<>();
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ALL_ACCEPTED_ORDERS),
                UrlFactory.getAppHeaders(), this);

    }
    private void pickUpOrder() {

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("order_id",order_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Authentication.object(context, UrlFactory.generateUrlWithVersion(AppConstants.URL_PICK_UP_ORDER),this,jsonObject);

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.viewPickUpButton:
            pickUpOrder();


                break;

        }

    }
    private void orderDetail() {
        binding.estTIme.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getRestaurant_name()));
        binding.tvTime.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getAddress()));
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof ModelAcceptedOrders) {
            modelAcceptedOrders = (ModelAcceptedOrders) responseObj;
            list.addAll(modelAcceptedOrders.getBranch_orders());
            orderDetail();
            OrderListAdapter orderListAdapter=new OrderListAdapter(context,list,this,"pickUp");
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerView.setAdapter(orderListAdapter);
        }
        if (responseObj instanceof String){
            if (((String) responseObj).equalsIgnoreCase("pickedUpSuccessfully")){
                Toast.makeText(context,"picked up successfully",Toast.LENGTH_SHORT).show();
                startActivity(IntentHelper.getOrderDetailScreen(context));
               /* callback.onClickonPickUpItem(order_id);*/
            }
        }
    }

    @Override
    public void onClickonViewOrder(String support_id) {
        startActivity(IntentHelper.getOrderDetailScreen(context));

    }

    public  interface  Callback{
        void  onClickonPickUpItem(String order_id);
    }

}
