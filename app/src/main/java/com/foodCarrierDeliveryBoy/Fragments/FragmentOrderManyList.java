package com.foodCarrierDeliveryBoy.Fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodCarrierDeliveryBoy.Adapters.OrderListAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Models.ModelAcceptedOrders;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityOrderManyListBinding;
import com.ebanx.swipebtn.OnActiveListener;

import java.util.ArrayList;
import java.util.HashMap;

public class FragmentOrderManyList extends BaseFragment implements View.OnClickListener, OrderListAdapter.Callback {
    private ActivityOrderManyListBinding binding;
    private Context context;
    private final Callback callback;
    int counter, progress;
    CountDownTimer countDownTimer;
    ModelAcceptedOrders modelAcceptedOrders;
    ArrayList<ModelAcceptedOrders.BranchOrdersBean> list=new ArrayList();
    public FragmentOrderManyList(Context context, Callback callback){

        this.context = context;
        this.callback = callback;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate( R.layout.activity_order_many_list,null);
        binding= DataBindingUtil.bind(view);

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.viewRestBtnn.setOnClickListener(this);
        binding.orderScreen.setBackgroundColor(getResources().getColor(R.color.white));
        getAllAcceptedOrders();
        binding.cancelButton.setOnActiveListener(new OnActiveListener() {
            @Override
            public void onActive() {

                countDounTimer();


            }
        });
    }
    private void getAllAcceptedOrders() {
        HashMap<String, String> map = new HashMap<>();
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ALL_ACCEPTED_ORDERS),
                UrlFactory.getAppHeaders(), this);

    }
    private void orderDetail() {
        binding.estTIme.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getRestaurant_name()));
        binding.tvTime.setText(ValidationHelper.optional(modelAcceptedOrders.getBranch().getAddress()));
    }
    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof ModelAcceptedOrders){
            modelAcceptedOrders= (ModelAcceptedOrders) responseObj;
            list.addAll(modelAcceptedOrders.getBranch_orders());
            orderDetail();
            OrderListAdapter orderListAdapter=new OrderListAdapter(context,list,this,"manyList");
            binding.recyclerview.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerview.setAdapter(orderListAdapter);


        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewRestBtnn:
        callback.onClickOnAtRestaurant();
                break;
        }
    }


    @Override
    public void onClickonViewOrder(String support_id) {
        startActivity(IntentHelper.getOrderDetailScreen(context));

    }

    public interface  Callback{
        void onClickOnAtRestaurant();


    }
    void countDounTimer() {
        counter = 20;
        progress = 0;
        countDownTimer = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {

                counter--;
            }

            @Override
            public void onFinish() {
                binding.cancelButton.setVisibility(View.GONE);
            }

        }.start();
    }
}
