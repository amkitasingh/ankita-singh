package com.foodCarrierDeliveryBoy.Fragments;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Dialogs.PopUpDialog;
import com.foodCarrierDeliveryBoy.Models.UserModel;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.databinding.FragmentHomeBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {
   private Context context;
    FragmentHomeBinding binding;
    UserModel model;
    private PopUpDialog.Callback callback;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        binding = DataBindingUtil.bind(v);
        context =getActivity();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("aaaaa","aaaaa");
        binding.viewVehicleName.setText("Use " + PrefManager.getInstance(context).getModeOfTransport());

        /* if (model.getUser().getSelected_vehicle().getId() != null && !model.getUser().getSelected_vehicle().getId().equals("")) {
            binding.viewVehicleName.setText("Use " + PrefManager.getInstance(context).getUserDetail().getUser().getSelected_vehicle().getName());
        }else {
            binding.viewVehicleName.setText("Use Bycycle");
        }

         */

        binding.viewTransportName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    context.startActivity(IntentHelper.getModeTransport(context));
                }
            });


    }


    public void changeScreen() {
        binding.viewOffline.setText("You are online");
    }
    public void changeScreenOffline() {
        binding.viewOffline.setText("You`re offline , go online to receive orders");
    }
}

