package com.foodCarrierDeliveryBoy.Fragments;


import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Models.EarningModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.FragmentTodayBinding;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentToday extends BaseFragment {
    private FragmentTodayBinding binding;
    Context context;
    LineChart lineChart;
    LineData lineData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_today, container, false);
        return binding.getRoot();


    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiHitTodayEarning("Today");

        // mChart = view.findViewById(R.id.chart);
        // binding.chart.setOnChartGestureListener(FragmentToday.this);
        // binding.chart.setOnChartValueSelectedListener(FragmentToday.this);

    }

    private void apiHitTodayEarning(String value) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("keyword",value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Authentication.object(getActivity(), UrlFactory.generateUrlWithVersion(AppConstants.URL_EARNINGS),this,jsonObject);
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        Log.d("sissss",responseObj.toString());
        if (responseObj instanceof EarningModel) {
            EarningModel earningModel = (EarningModel) responseObj;
            Log.d("sissss", earningModel.toString());
            /*    if (!ValidationHelper.isNull(earningModel.getToday_earnings().getAvg_distance())){}*/
            binding.Earning.setText("$" + earningModel.getToday_earnings().getTotal_earning());
            binding.tview2.setText(earningModel.getToday_earnings().getOnline_time());
            binding.tview4.setText(earningModel.getToday_earnings().getDistance());
            binding.tview6.setText("$" + earningModel.getToday_earnings().getAvg_earning());
            binding.tview8.setText(earningModel.getToday_earnings().getAvg_time());
            binding.tview7.setText(earningModel.getToday_earnings().getAvg_distance());
            binding.textView18.setText("$" + earningModel.getToday_earnings().getWallet_amount());

        /*
            Log.d("sixeee",String.valueOf(earningModel.getToday_earnings().getEarning_graph().size()));
            Log.d("sixeee",String.valueOf("asd"));
            setChart(earningModel.getToday_earnings().getEarning_graph());
            binding.textView16.setText(earningModel.getToday_earnings().getTotal_trips()+"Trips");
            binding.textView17.setText("$"+earningModel.getToday_earnings().getTotal_earning());
            }

         */

        }
        }


    private void setChart(List<EarningModel.TodayEarningsBean.EarningGraphBean> earning_graph) {
        binding.chart.setDragEnabled(false);
        binding.chart.setScaleEnabled(false);

        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i=0;i<earning_graph.size();i++){
            yValues.add(new Entry(Integer.parseInt(earning_graph.get(i).getTrip()),
                    Float.parseFloat(earning_graph.get(i).getEarning())));
        }
        LineDataSet set1 = new LineDataSet(yValues, "");

        set1.setFillAlpha(110);


        ArrayList<ILineDataSet> datasets = new ArrayList<>();
        datasets.add(set1);

        set1.setColor(R.color.orange);
        set1.setValueTextColor(Color.BLACK);
        set1.setValueTextSize(14f);


        LineData lineData = new LineData(datasets);

        binding.chart.setData(lineData);
        binding.chart.getAxisLeft().setDrawGridLines(false);
        binding.chart.getDescription().setEnabled(false);
        binding.chart.getXAxis().setDrawGridLines(false);
        binding.chart.getAxisLeft().isDrawGridLinesEnabled();

    }
}
