package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.foodCarrierDeliveryBoy.Models.ModelViewOrder;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ItemListAdapterBinding;
import java.util.List;

public class OrderListThirdAdapter extends RecyclerView.Adapter<OrderListThirdAdapter.Holder> {
    List<ModelViewOrder.OrderDetailsBean.MenuItemBean> list;
    private ItemListAdapterBinding binding;
    private Context context;



    public OrderListThirdAdapter(Context context, List<ModelViewOrder.OrderDetailsBean.MenuItemBean> list) {
        this.list = list;
        this.context = context;
    }


    @NonNull
    @Override
    public OrderListThirdAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_adapter, parent, false);
        //View view  = layoutInflater.inflate(R.layout.common_item_adadter_rows, parent, false);
        binding = DataBindingUtil.bind(view);
        return new OrderListThirdAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListThirdAdapter.Holder holder, int position) {
        if (list.size()>0) {
            binding.tvDishName.setText(list.get(position).getItem_name());
            if (!ValidationHelper.isNull(String.valueOf(list.get(position).getQuantity()))) {
                binding.tvQuantityViewOrder.setText(String.valueOf(list.get(position).getQuantity()));
            } else {
                binding.tvQuantityViewOrder.setText("0");
            }

            for (int i = 0; i < list.get(position).getItem_addons().size(); i++) {
                binding.tvAddOns.append(list.get(position).getItem_addons().get(i).getAddon_title());
                binding.tvAddOns.append("\n");

            }
        }
/*
        if (screenName.equals("pickUpOrderItem")){


        }else if (screenName.equals("viewOrder")){
            binding.tvQuantity.setVisibility(View.GONE);
            binding.checkBox.setVisibility(View.GONE);
            binding.tvQuantityViewOrder.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class Holder extends RecyclerView.ViewHolder {
        Holder(View view){
            super(view);

        }
    }









}
