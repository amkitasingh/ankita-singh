package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;

import com.foodCarrierDeliveryBoy.Models.NotificationModel;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.NotificationAdapterBinding;

import java.util.List;

public class NotificationAdapter extends BaseAdapter {

    private NotificationAdapterBinding binding;
    private List<NotificationModel.NotificationsBean> list;
    private Context context;


    public NotificationAdapter(Context context, List<NotificationModel.NotificationsBean> list) {
        this.context = context;
        this.list = list;


    }

    @Override
    public int getCount() {

        if (list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.notification_adapter, parent, false);
        view = binding.getRoot();

        binding.viewNotification.setText(ValidationHelper.optional(list.get(position).getMessage()));
        binding.viewDateTime.setText(ValidationHelper.optional(list.get(position).getCreated_at()));
        binding.imageView6.setVisibility(View.GONE);

        return view;


    }


    public void onMoreDataReq(List<NotificationModel.NotificationsBean> notifications) {

    }

    public void refresh(List<NotificationModel.NotificationsBean> notifications) {

    }
}
