package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;


import com.foodCarrierDeliveryBoy.Models.OrderHistoryDetailModel;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.OrderHistoryDetailBinding;

import java.util.List;

public class OrderHistoryAdapter extends BaseAdapter {

    private OrderHistoryDetailBinding binding;
    private  List<OrderHistoryDetailModel.OrdersBean> list;
    private Context context;
    private  Callback callback;


    public OrderHistoryAdapter(Context context, List<OrderHistoryDetailModel.OrdersBean> list) {
        this.context = context;
        this.list = list;
        this.callback=callback;
        
    }

    @Override
    public int getCount() {

        if (list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.order_history_detail, parent, false);
        view = binding.getRoot();

        if (list.get(position).isIs_rejected()) {
            binding.viewCancel.setVisibility(View.VISIBLE);
            binding.viewCost.setVisibility(View.GONE);
            binding.viewOrderCode.setText(ValidationHelper.optional(String.valueOf(list.get(position).getId())));
            binding.viewDateTime.setText(ValidationHelper.optional(list.get(position).getPlaced_at()));
            binding.viewItemNo.setText(ValidationHelper.optional(String.valueOf(list.get(position).getItem_count())) + " Item ");
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(String.valueOf(list.get(position).getId()),list.get(position).getPlaced_at(),
                            String.valueOf(list.get(position).getItem_count()));


                }
            });

        } else {
            binding.viewCancel.setVisibility(View.GONE);
            binding.viewCost.setVisibility(View.VISIBLE);
            binding.viewOrderCode.setText(ValidationHelper.optional(String.valueOf(list.get(position).getId())));
            binding.viewDateTime.setText(ValidationHelper.optional(list.get(position).getPlaced_at()));
            binding.viewItemNo.setText(ValidationHelper.optional(String.valueOf(list.get(position).getItem_count())) + " Item ");

        }

        return view;


    }
    public void refresh(List<OrderHistoryDetailModel.OrdersBean> orders) {
        
    }

    public void onMoreDataReq(List<OrderHistoryDetailModel.OrdersBean> orders) {
        
    }
    public interface Callback {
        void onItemClick(String id, String issueDate,String  order_item_no);

    }
}



