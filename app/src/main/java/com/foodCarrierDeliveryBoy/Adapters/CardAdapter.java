package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;

import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.CardAdapterBinding;

import java.util.List;

public class CardAdapter extends BaseAdapter {

    private CardAdapterBinding binding;
    private List<String> list;
    private Context context;

    public CardAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {

        if (list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.card_adapter, parent, false);
        view = binding.getRoot();



        return view;


    }

}
