package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.foodCarrierDeliveryBoy.Models.ModelChatAllMessage;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;


import java.util.ArrayList;
import java.util.List;

public class AllChatMessagesAdapter extends BaseAdapter {

    ArrayList<ModelChatAllMessage.TicketBean> list;
    Context context;
    public AllChatMessagesAdapter(Context context, ArrayList<ModelChatAllMessage.TicketBean> list){
        this.list=list;
        this.context=context;
    }


    @Override
    public int getCount() {
        if (list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }


    @Override
    public Object getItem(int position) {
        return list.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(list.get(position).getUser_id()== PrefManager.getInstance(context).getUserDetail().getUser().getId()) {
            convertView = LayoutInflater.from(context).inflate(R.layout.all_chat_message_row_left, null);//reverse
        }
        else  {
            convertView = LayoutInflater.from(context).inflate(R.layout.all_chat_message_row_right, null);
        }


        TextView conversationDate =convertView.findViewById(R.id.tv_conversation_header);

        conversationDate.setVisibility(View.GONE);

    /*    if (position > 0) {


            if (Formatter.conversationDate(Long.parseLong(list.get(position).getTimestamp())).equalsIgnoreCase(Formatter.conversationDate(Long.parseLong(list.get(position - 1).getTimestamp())))) {

                conversationDate.setVisibility(View.GONE);
            } else {

                conversationDate.setVisibility(View.VISIBLE);
            }
        } else {
            conversationDate.setVisibility(View.VISIBLE);
        }
*/




        TextView message=convertView.findViewById(R.id.tv_message);
        TextView time=convertView.findViewById(R.id.message_time);

        ImageView userImage= convertView.findViewById(R.id.iv_role_profile_pic);
        if(list.get(position).getUser_id()== PrefManager.getInstance(context).getUserDetail().getUser().getId()) {
            Glide.with(context)
                    .load(PrefManager.getInstance(context).getUserDetail().getUser().getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.user_icon))
                    .into(userImage);
            message.setText(ValidationHelper.optional(list.get(position).getReply()));
        }



     // time.setText(Formatter.messageTiming(Long.parseLong(list.get(position).getTimestamp())));
        time.setText(list.get(position).getReply_created_at());
        return convertView;
    }

    public void refresh(List<ModelChatAllMessage.TicketBean> list1) {
        list.clear();
        list.addAll(list1);
        notifyDataSetChanged();
    }
}
