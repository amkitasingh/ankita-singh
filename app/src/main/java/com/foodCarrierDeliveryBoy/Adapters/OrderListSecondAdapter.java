package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodCarrierDeliveryBoy.Models.ModelViewOrder;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.OrderListRowsBinding;


import java.util.ArrayList;
import java.util.List;

public class OrderListSecondAdapter extends RecyclerView.Adapter<OrderListSecondAdapter.Holder> {
    List<ModelViewOrder.OrderDetailsBean.MenuItemBean> list;
    LayoutInflater layoutInflater;
    private Callback callback;
    OrderListRowsBinding binding;
    String screenName;
    Context context;
    private final RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();


    public OrderListSecondAdapter(Context context, ArrayList<ModelViewOrder.OrderDetailsBean.MenuItemBean> list, String screenName) {
        this.list = list;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.callback = callback;
        this.screenName = screenName;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_rows, parent, false);
        //View view  = layoutInflater.inflate(R.layout.common_item_adapter_rows, parent, false);
        binding = DataBindingUtil.bind(view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(binding.rvChild.getContext()
                , LinearLayoutManager.VERTICAL, false);


        OrderListThirdAdapter OrderListThirdAdapter = new OrderListThirdAdapter(context, list);
        binding.rvChild.setLayoutManager(layoutManager);
        binding.rvChild.setAdapter(OrderListThirdAdapter);
        binding.rvChild.setRecycledViewPool(viewPool);


    }

    @Override
    public int getItemCount() {

        return list.size();
    }


    public class Holder extends RecyclerView.ViewHolder {
        private TextView ParentItemTitle;
        private RecyclerView ChildRecyclerView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ParentItemTitle = itemView.findViewById(R.id.viewMenuOrderItem);
            ChildRecyclerView = itemView.findViewById(R.id.rv_child);

        }
    }

    public interface Callback {
        void onClickonViewOrder();
    }
}
