package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.foodCarrierDeliveryBoy.Models.ModelAcceptedOrders;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.DeliveredOrderRowsBinding;


import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.Holder> {
    ArrayList<ModelAcceptedOrders.BranchOrdersBean> list;
    LayoutInflater layoutInflater;
    private Callback callback;
    DeliveredOrderRowsBinding binding;
    String screenName;
    Context context;


  /*  public OrderListAdapter(Context context, ArrayList<String> list,Callback callback,String screenName){
        this.list=list;
        this.context=context;
        layoutInflater=LayoutInflater.from(context);
        this.callback = callback;
        this.screenName=screenName;
    }*/

    public OrderListAdapter(Context context, ArrayList<ModelAcceptedOrders.BranchOrdersBean> list, Callback callback, String screenName) {
        this.list = list;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.callback = callback;
        this.screenName = screenName;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivered_order_rows, parent, false);
        //View view  = layoutInflater.inflate(R.layout.common_item_adadter_rows, parent, false);
        binding = DataBindingUtil.bind(view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        binding.viewOrderCode.setText(ValidationHelper.optional(String.valueOf(list.get(position).getOrder_id())));
        binding.estTIme1.setText(ValidationHelper.optional(String.valueOf(list.get(position).getApartment_number())));
        binding.tvTime1.setText(ValidationHelper.optional(String.valueOf(list.get(position).getAddress_type())));
        binding.viewStatusdetail.setText(ValidationHelper.optional(list.get(position).getOrder_delivery_status()));
        binding.tvTime2.setText(ValidationHelper.optional(String.valueOf(list.get(position).getDelivery_amount())));
        binding.viewOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickonViewOrder(String.valueOf(list.get(position).getOrder_id()));
            }
        });


        if (screenName.equals("delivered")) {

            if (list.size() == 1) {
                binding.viewDeliveredButton.setVisibility(View.GONE);

            }else
            {
                binding.viewDeliveredButton.setVisibility(View.VISIBLE);
            }

        } else if (screenName.equals("pickUp")) {
            binding.viewDeliveredButton.setVisibility(View.GONE);

        } else if (screenName.equals("fragmentOrder")) {
            if (list.size() == 1) {
                binding.viewDeliveredButton.setVisibility(View.GONE);
                binding.viewOrderBtn.setVisibility(View.GONE);
                binding.userImageChat1.setVisibility(View.GONE);
            }else
            {
                binding.viewDeliveredButton.setVisibility(View.GONE);
                binding.viewOrderBtn.setVisibility(View.VISIBLE);

            }
        }


      /*  binding.viewOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(IntentHelper.getOrderDetailScreen(context));
            }
        });
        */

        binding.viewDeliveredButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(IntentHelper.getDeliveredScreen(context));
            }
        });
    }

    @Override
    public int getItemCount() {

        if (list.size() > 0) {
            return list.size();
        } else
            return 0;
    }


    public static class Holder extends RecyclerView.ViewHolder {
        Holder(View view) {
            super(view);

        }
    }

    public interface Callback {
        void onClickonViewOrder(String support_id);
    }


}
