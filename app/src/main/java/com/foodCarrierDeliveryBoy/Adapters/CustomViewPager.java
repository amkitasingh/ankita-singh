package com.foodCarrierDeliveryBoy.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.foodCarrierDeliveryBoy.Fragments.FragmentToday;
import com.foodCarrierDeliveryBoy.Fragments.FragmentWeek;

public class CustomViewPager extends FragmentPagerAdapter {

    CharSequence Titles[];
    int NumbOfTabs;
    public CustomViewPager(FragmentManager fm, CharSequence Titles[], int NumbOfTabs) {
        super(fm);
        this.Titles = Titles;
        this.NumbOfTabs = NumbOfTabs;

    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            return new FragmentToday();
        }
        else  {
            return new FragmentWeek();
        }
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

}
