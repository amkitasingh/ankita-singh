package com.foodCarrierDeliveryBoy.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodCarrierDeliveryBoy.Models.ModelLeftMenuDrawer;
import com.foodCarrierDeliveryBoy.R;

import java.util.ArrayList;


public class DrawerItemCustomAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    String titles [];
    int icons [];
    private ArrayList<ModelLeftMenuDrawer> list;
    private Callback callback;


    public DrawerItemCustomAdapter(Context mContext, ArrayList<ModelLeftMenuDrawer> list, Callback callback) {
        this.list = list;
        this.callback = callback;

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return list.size();
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(R.layout.list_view_item_row, parent, false);
        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);
        final ImageView arrow = (ImageView) listItem.findViewById(R.id.ViewIcon);
        final TextView textViewChangePw = (TextView) listItem.findViewById(R.id.tvChangePassword);
        final TextView textViewBankAcc = (TextView) listItem.findViewById(R.id.tvBankAccountSetting);
            if (position==0){
                if (list.get(position).isExpanded()){
                    textViewBankAcc.setVisibility(View.VISIBLE);
                    textViewChangePw.setVisibility(View.VISIBLE);
                    arrow.setImageResource(R.drawable.back_icon);
                    arrow.setRotation(90);
                }else {
                    arrow.setImageResource(R.drawable.back_icon);
                    arrow.setRotation(270);
                    textViewBankAcc.setVisibility(View.GONE);
                    textViewChangePw.setVisibility(View.GONE);
                }
            }

        listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback.onClickonItemDrawer(position,list.get(position).isExpanded());
            }
        });
        textViewChangePw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickonLeftMenuChangePasswordSetting();
            }
        });
        textViewBankAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickonLeftMenuBankAccountSetting();

            }
        });

        imageViewIcon.setImageResource(list.get(position).getIcons());
        textViewName.setText(list.get(position).getTitles());
        if (position==7){
         arrow.setVisibility(View.INVISIBLE);
        }

        return listItem;
    }

    public interface  Callback{
        void  onClickonItemDrawer(int position,boolean isExpandable);
        void  onClickonLeftMenuBankAccountSetting();
        void  onClickonLeftMenuChangePasswordSetting();
    }

    public void expandCollapse(int position,boolean value){
        if (value){
            list.get(position).setExpanded(false);
        }else {
            list.get(position).setExpanded(true);
        }

        notifyDataSetChanged();


    }


}

