package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;


import com.foodCarrierDeliveryBoy.Activity.ActivityTicket;
import com.foodCarrierDeliveryBoy.CallBacks.PaginationCallback;
import com.foodCarrierDeliveryBoy.Models.AllTicketModel;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.TicketAdapterBinding;

import java.util.ArrayList;
import java.util.List;

public class TicketAdapter extends BaseAdapter {
    Callback callback;
    private TicketAdapterBinding binding;
    List<AllTicketModel.IssuesListBean> list = new ArrayList<>();
    private Context context;
    PaginationCallback paginationCallback;


    public TicketAdapter(ActivityTicket context, List<AllTicketModel.IssuesListBean> list, Callback callback,PaginationCallback paginationCallback) {
        this.context = context;
        this.callback=callback;
        this.list = list;
        this.paginationCallback = paginationCallback;
    }

    @Override
    public int getCount() {

        if (list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }



    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.ticket_adapter, parent, false);
        view = binding.getRoot();
        if(position+1==list.size()){
            paginationCallback.loadNextPage();  // Callback
        }

        binding.refrenceNo.setText(ValidationHelper.optional(String.valueOf(list.get(position).getId()))+". ");
        binding.viewOrderCode.setText(ValidationHelper.optional(" #" +list.get(position).getId()));
        binding.viewDateTime.setText(ValidationHelper.optional(list.get(position).getIssue_created_at()));
        binding.viewStatus.setText(ValidationHelper.optional(list.get(position).getStatus()));
        binding.viewMessage.setText(ValidationHelper.optional(list.get(position).getMessage()));
        binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClickonTicket(String.valueOf(list.get(position).getId()),
                        list.get(position).getIssue_created_at(),list.get(position).getStatus());
            }
        });
        return view;
    }
    public void refresh(List<AllTicketModel.IssuesListBean> issues_list) {}

    public void onMoreDataReq(List<AllTicketModel.IssuesListBean> issues_list) {}

    public void addData(ArrayList<AllTicketModel.IssuesListBean> issues_list) {
        if(issues_list!=null){
            list.addAll(issues_list);
        }
        notifyDataSetChanged();
    }

    public interface Callback{
        void onClickonTicket(String support_id, String issueDate, String status);
    }
}