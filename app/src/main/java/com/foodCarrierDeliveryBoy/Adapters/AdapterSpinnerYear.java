package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;


import com.foodCarrierDeliveryBoy.R;

import java.util.List;

public class AdapterSpinnerYear extends BaseAdapter implements SpinnerAdapter {

    private  Context context;
    private List<String> yearList;
    Typeface face2;

    public AdapterSpinnerYear(Context context, List<String> dropText) {
        this.yearList = dropText;
        this.context = context;
        //face1 = Typeface.createFromAsset(context.getAssets(),"font/opensansregular.ttf");
        // face2 = Typeface.createFromAsset(context.getAssets(), "font/opensanssemibold.ttf");
    }


    public int getCount() {
        return yearList.size();
    }

    public Object getItem(int i) {
        return yearList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        txt.setPadding(14, 14, 20, 14);
        txt.setTextSize(16);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(yearList.get(position));
        txt.setAllCaps(true);
        txt.setTextColor(Color.parseColor("#303030"));
        txt.setTypeface(face2);
        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(context);
        txt.setGravity(Gravity.LEFT);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setPadding(30, 0, 20, 0);
        txt.setTextSize(16);
        txt.setAllCaps(true);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
        txt.setCompoundDrawablePadding(30);
        txt.setText(yearList.get(i));
        txt.setTypeface(face2);
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }



}
