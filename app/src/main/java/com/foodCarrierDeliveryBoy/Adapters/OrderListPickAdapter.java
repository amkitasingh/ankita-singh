package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.foodCarrierDeliveryBoy.Models.ModelViewOrder;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.OrderListPickRowsBinding;

import java.util.ArrayList;

public class OrderListPickAdapter   extends RecyclerView.Adapter<OrderListPickAdapter.Holder> {
    ArrayList<ModelViewOrder.OrderDetailsBean.MenuItemBean> list;
    LayoutInflater layoutInflater;
    private Callback callback;
    OrderListPickRowsBinding binding;
    String screenName;
    Context context;


    public OrderListPickAdapter(Context context, ArrayList<ModelViewOrder.OrderDetailsBean.MenuItemBean> list,String screenName){
        this.list=list;
        this.context=context;
        layoutInflater=LayoutInflater.from(context);
        this.callback = callback;
        this.screenName=screenName;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_pick_rows, parent, false);
        //View view  = layoutInflater.inflate(R.layout.common_item_adadter_rows, parent, false);
        binding = DataBindingUtil.bind(view);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        if (list.size()>0) {
            binding.tvDishName.setText(list.get(position).getItem_name());
            if (!ValidationHelper.isNull(String.valueOf(list.get(position).getQuantity()))) {
                binding.tvQuantity.setText(String.valueOf(list.get(position).getQuantity()));
            } else {
                binding.tvQuantity.setText("0");
            }

            if (list.get(position).getItem_addons().size() > 0) {
                for (int i = 0; i < list.get(position).getItem_addons().size(); i++) {
                    binding.tvAddOns.append(list.get(position).getItem_addons().get(i).getAddon_title());
                    binding.tvAddOns.append("\n");
                }
            } else {
                binding.tvAddOns.setVisibility(View.GONE);
            }
        }

        if (screenName.equals("pickUpOrderItem")){


        }else if (screenName.equals("viewOrder")){
            binding.tvQuantity.setVisibility(View.GONE);
            binding.checkBox.setVisibility(View.GONE);
            binding.tvQuantityViewOrder.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return 0;
    }


    public static class Holder extends RecyclerView.ViewHolder {
        Holder(View view){
            super(view);

        }
    }

    public interface  Callback{
        void onClickonViewOrder();
    }
}
