package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.foodCarrierDeliveryBoy.Models.CreatedVehicleListModel;
import com.foodCarrierDeliveryBoy.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterTransportMode extends RecyclerView.Adapter<AdapterTransportMode.MyViewHolder> {
    ArrayList<CreatedVehicleListModel.VehicleListBean> mode;
    List<CreatedVehicleListModel.SelectedVehicleBean> model;
    int selectedPosition;
    Context context;
    TextView name;
    ImageView image;
    Callback callback;
    private String Id;
    boolean makeAllUnselect = false;
    private onClickOnItem onClickOnItem;


    public interface onClickOnItem {
        void click(int id,String name);
    }

    public AdapterTransportMode(Context context, ArrayList<CreatedVehicleListModel.VehicleListBean> mode,
                                Callback callback, onClickOnItem onClickOnItem) {
        this.context = context;
        this.mode = mode;
        this.callback = callback;
        this.onClickOnItem = onClickOnItem;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_transport_mode, parent, false);
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.name.setText(mode.get(position).getName());
        holder.range.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Under Development", Toast.LENGTH_SHORT).show();
               // context.startActivity(new Intent(context, ActivityAddTransportDetail.class));
            }
        });



        holder.image.setImageResource(R.drawable.walk_icon);
        Glide.with(context)
                .load(mode.get(position).getImage())
                .apply(new RequestOptions()
                        .transform(new CircleCrop(), new RoundedCorners(20))
                        .placeholder(R.drawable.logo_icon))
                .into(holder.image);
        holder.range.setText(String.valueOf(mode.get(position).getRange()));


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onClickonModeOfTransport(position);
                    selectedPosition = position;
                }
            }
        });
        if (mode.get(position).isSelected()) {
            Id = String.valueOf(mode.get(position).getId());
            if (onClickOnItem != null) {
                onClickOnItem.click(mode.get(position).getId(),mode.get(position).getName());

                holder.linearLayout.setBackgroundColor(context.getResources().getColor(R.color.orange));
                holder.name.setTextColor(context.getResources().getColor(R.color.white));
                holder.range.setTextColor(context.getResources().getColor(R.color.white));
            }
        } else {
            holder.linearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.name.setTextColor(context.getResources().getColor(R.color.orange));
            holder.range.setTextColor(context.getResources().getColor(R.color.orange));
        }
    }

    @Override
    public int getItemCount() {
        return mode.size();
    }

    public void updateBackgroundColor(int position) {
        for (int i = 0; i < mode.size(); ++i) {
            mode.get(i).setSelected(false);
        }
        mode.get(position).setSelected(true);
        notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView name, range;
        ImageView image;
        ConstraintLayout linearLayout;


        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            name = (TextView) itemView.findViewById(R.id.viewName);
            image = (ImageView) itemView.findViewById(R.id.image);
            range = (TextView) itemView.findViewById(R.id.name);
            linearLayout = (ConstraintLayout) itemView.findViewById(R.id.linearLayout);

        }
    }

    public interface Callback {
        void onClickonModeOfTransport(int id);
    }
}

