package com.foodCarrierDeliveryBoy.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.databinding.DataBindingUtil;


import com.foodCarrierDeliveryBoy.Models.Faqs;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ListGroupBinding;

import java.util.ArrayList;

public class FaqAdapter extends BaseAdapter {
    Callback callback;
    ListGroupBinding binding;
    ArrayList<Faqs.FaqsBean> list;
    Context context;

    public FaqAdapter(Context context, ArrayList<Faqs.FaqsBean> list,Callback callback) {
        this.list = list;
        this.callback=callback;
        this.context = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.list_group, null);
        binding= DataBindingUtil.bind(convertView);
        binding.tvQuestion.setText(list.get(position).getQuestion());
        binding.tvAnsswer.setText(list.get(position).getAnswer());


        if (list.get(position).isExpanded()){
            binding.imageIndicator.setImageResource(R.drawable.arrow_down_);
            binding.tvAnsswer.setVisibility(View.VISIBLE);
        }else {
            binding.imageIndicator.setImageResource(R.drawable.drop_down_icon);
            binding.tvAnsswer.setVisibility(View.GONE);
        }

        binding.tvQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    callback.isExpandedOrNot(position,list.get(position).isExpanded());
            }

        });

        return convertView;
    }

    public  interface Callback{
        void  isExpandedOrNot(int position, boolean isExpanded);
    }

    public void setExpandable(int position,boolean value){
        list.get(position).setExpanded(value);
        notifyDataSetChanged();
    }
}
