package com.foodCarrierDeliveryBoy.Activity;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.CountDownTimerView;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.UserModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityVerificationBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.DEVICE_TYPE_INPUT;
import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.key_input_role;

public class ActivityVerification extends BaseActivity implements  CountDownTimerView.Callbacks{

    private ActivityVerificationBinding binding;
    private Context context;
    private ProgressDialog dialog;
    private boolean isSignUp;
    private String countryCode;
    private String mVerificationId;
    private FirebaseAuth mAuth;
    private String contactNo;
    private String password;
    private String name;
    private String title;
    int counter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification);
        context = this;
        dialog = new ProgressDialog(context);
        init();
        binding.viewVerifyBtn.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();

        if (getIntent() != null) {
            isSignUp = getIntent().getBooleanExtra("isSignUp", false);
            Log.d("sdfghj", "onCreate: " + isSignUp);
        }
        if (getIntent() != null) {
            contactNo = getIntent().getStringExtra(AppConstants.key_contact);
        }
        if (getIntent() != null) {
            countryCode = getIntent().getStringExtra(AppConstants.key_country_code);
        }
        if (getIntent() != null) {
            name = getIntent().getStringExtra(AppConstants.key_name);
        }
        if (getIntent() != null) {
            title = getIntent().getStringExtra(AppConstants.key_title);
        }

        if (getIntent() != null) {
            password = getIntent().getStringExtra(AppConstants.key_password);
        }

        sendVerificationCode(contactNo);
        binding.viewTapHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificationCode(contactNo);
            }
        });
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void init() {


        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.viewEnterCode:
                verifyEnteredCode();
                break;

        }

    }

    private void sendVerificationCode(String contactNo) {
        final int Timeout = 60;
        String userCountryCode = countryCode;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                userCountryCode + contactNo,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }
    @Override
    public void onCountDownFinish() {
        binding.viewEnterCode.setVisibility(View.VISIBLE);
        binding.viewTapHere.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCountDownStart() {
        binding.viewEnterCode.setVisibility(View.VISIBLE);
        binding.viewTapHere.setVisibility(View.GONE);

    }

    // check kro
    private void verifyEnteredCode() {
        String code = binding.viewEnterCode.getOTP();
        if (code.isEmpty() || code.length() < 6) {
            Toast.makeText(context, "Enter valid code", Toast.LENGTH_SHORT).show();
        }
        //verifying the code entered manually
        verifyVerificationCode(code);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {

            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                binding.viewEnterCode.setOTP(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            binding.timer.startTimer();
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            //signing the user
            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            /*Toast toast = Toast.makeText(this, "Verification Code is wrong", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();*/
            e.printStackTrace();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(ActivityVerification.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //  Toast.makeText(context, "called", Toast.LENGTH_SHORT).show();
                        if (task.isSuccessful()) {
                            if (isSignUp) {
                                hitSignUpApi();
                            } else {
                                startActivity(IntentHelper.getResetPassword(context)
                                        .putExtra("contactNo", contactNo)
                                        .putExtra("countryCode", countryCode));
                            }

                        } else {
                            //verification unsuccessful.. display an error message
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(context, "Invalid code entered...", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
    }

    private void hitSignUpApi() {
        if (!isFinishing() && !dialog.isShowing()) {
            dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_title, title);
        map.put(AppConstants.key_name, name);
        map.put(AppConstants.key_contact, contactNo);
        map.put(AppConstants.key_country_code, countryCode);
        map.put(AppConstants.key_role, key_input_role);
        map.put(AppConstants.key_password, password);
        map.put(AppConstants.DEVICE_TYPE, DEVICE_TYPE_INPUT);
        map.put(AppConstants.DEVICE_ID, UrlFactory.getDeviceToken());
        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_SIGN_UP),
                UrlFactory.getDefaultHeaders(), this);

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (responseObj instanceof UserModel) {
            UserModel model = (UserModel) responseObj;

            new PrefManager(context).setUserDetail(model);
            new PrefManager(context).setUserLogin(true);
            startActivity(IntentHelper.getModeTransport(context));
        }
    }


    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }



}
