package com.foodCarrierDeliveryBoy.Activity;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodCarrierDeliveryBoy.Adapters.NotificationAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseFragment;
import com.foodCarrierDeliveryBoy.Dialogs.ErrorDialogSimple;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.NotificationModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityNotificationBinding;
import com.quentindommerc.superlistview.OnMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityNotification extends BaseFragment implements OnMoreListener {
    ActivityNotificationBinding binding;
    Context context;
    NotificationAdapter adapter;
    ProgressDialog progressDialog;
    private List<NotificationModel.NotificationsBean> NotificationsBean = new ArrayList<>();
    int count = 0;
    private String per_page = "10";
    private int page = 1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_notification, container, false);
        return binding.getRoot();

    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        context = getActivity();
         hitNotificationApi();

    }




    private void hitNotificationApi() {
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_page, String.valueOf(page));
        map.put(AppConstants.key_per_page, per_page);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants
                .URL_NOTIFICATION), UrlFactory.getAppHeaders(), this);
    }

    @Override
    public void onLoadMore(Object responseObj) {
        super.onLoadMore(responseObj);
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (!(responseObj instanceof NotificationModel)) {
            return;
        }
        NotificationModel model = (NotificationModel) responseObj;
        binding.notifyListView.hideMoreProgress();
        adapter.onMoreDataReq(model.getNotifications());
        if (binding.notifyListView.isLoadingMore()) {
            binding.notifyListView.setLoadingMore(false);
        }
        if (model.getNotifications().size() > 0 && model.getNotifications().size() < 9) {
            binding.notifyListView.removeMoreListener();
        }
        if (model.getNotifications().size() == 0)
            binding.notifyListView.removeMoreListener();
    }

    @Override
    public void onTaskSuccess(Object responseObj) {

        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (responseObj instanceof NotificationModel) {
            NotificationModel model = (NotificationModel) responseObj;
            if (NotificationsBean.size() > 0) {
                NotificationsBean.clear();
            }
            NotificationsBean.addAll(model.getNotifications());
            if (adapter == null) {
                adapter = new NotificationAdapter(context,model.getNotifications());
                binding.notifyListView.setAdapter(adapter);
            } else {
                adapter.refresh(model.getNotifications());
            }
            if (NotificationsBean != null && NotificationsBean.size() > 10) {
                binding.notifyListView.setupMoreListener(this, 1);
                if (binding.notifyListView.isLoadingMore()) {
                    binding.notifyListView.setLoadingMore(false);
                }
            } else {
                page = 1;
                binding.notifyListView.removeMoreListener();
            }

        }
    }


    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        ErrorDialogSimple.show(getActivity(), errorMsg);
    }



    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
    public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {

    }
}
