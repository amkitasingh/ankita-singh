package com.foodCarrierDeliveryBoy.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.foodCarrierDeliveryBoy.Adapters.AdapterTransportMode;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.CreatedVehicleListModel;
import com.foodCarrierDeliveryBoy.Models.SelectVehicleModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityModeOfTransportBinding;

import java.util.ArrayList;
import java.util.HashMap;

public class ActivityModeOfTransport extends BaseActivity implements AdapterTransportMode.Callback,
        com.foodCarrierDeliveryBoy.Adapters.AdapterTransportMode.onClickOnItem {
    private ActivityModeOfTransportBinding binding;
    private static final String TAG = "ActivityModeOfTransport";
    private Context context;
    private ProgressDialog progressDialog;
    private AdapterTransportMode AdapterTransportMode;
    private  String vahId;
    private String selectedName;
    private  String vahiSelectedId;

    ArrayList<CreatedVehicleListModel.VehicleListBean>  mode = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mode_of_transport);
        context = this;
        progressDialog = new ProgressDialog(context);


        binding.layout.imgBackArrow.setOnClickListener(this);
        binding.viewSubmitBtn.setOnClickListener(this);
        toolbarSetting();
        apiHitAllModes();
    }

    private void apiHitAllModes() {
        Authentication.get((AppConstants.URL_CREATE_TRANSPORT), this, false);
    }


    private void apiHitSelectVehicle() {
        if (!progressDialog.isShowing()) {
            progressDialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("vehicle_id", String.valueOf(vahId));
        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_SELECT_VEHICLE),
                UrlFactory.getAppHeaders(), this);
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (progressDialog.isShowing())progressDialog.dismiss();

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
            if (progressDialog.isShowing())progressDialog.dismiss();
        if (responseObj instanceof CreatedVehicleListModel) {
            mode.clear();

            CreatedVehicleListModel model = (CreatedVehicleListModel) responseObj;
            mode.addAll(model.getVehicle_list());
            if (model.getSelected_vehicle()!=null){
                vahiSelectedId = model.getSelected_vehicle().getId();

            }

           /*
            //ModelAllModes modelAllModes1 = new ModelAllModes();
           List<CreatedVehicleListModel.VehicleListBean> vehiclesBeanList = new ArrayList<CreatedVehicleListModel.VehicleListBean>();
            CreatedVehicleListModel.VehicleListBean vehiclesBean2 = new CreatedVehicleListModel.VehicleListBean();
            vehiclesBean2.setId(2);
            vehiclesBean2.setSelected(true);
            vehiclesBean2.setName("Bicycle");
            vehiclesBean2.setRange(25);
            vehiclesBeanList.add(vehiclesBean2);
            mode.addAll(vehiclesBeanList);*/

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            binding.recyclerView.setLayoutManager(gridLayoutManager);

            AdapterTransportMode = new AdapterTransportMode(ActivityModeOfTransport.this, mode, this,this);
            binding.recyclerView.setAdapter(AdapterTransportMode);

            for (int i=0; i<model.getVehicle_list().size(); i++ ){

                if ( vahiSelectedId!=null&&vahiSelectedId.equalsIgnoreCase(String.valueOf(model.getVehicle_list().get(i).getId()))) {
                    model.getVehicle_list().get(i).setSelected(true);
                    Log.d(TAG, "onTaskSuccess: "+i +"\n"+model.getVehicle_list().get(i).getId());
                }
            }

        } else if (responseObj instanceof SelectVehicleModel) {
            SelectVehicleModel modelSelect = (SelectVehicleModel) responseObj;
            if (modelSelect.getSelected_vehicle().getId() != null && !modelSelect.getSelected_vehicle().getId().equals("")) {
               PrefManager.getInstance(context).setModeOfTransport(selectedName);
                startActivity(IntentHelper.getDashboard(context));

            }
        }
    }


    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.viewSubmitBtn:
                if (vahId !=null && !vahId.equalsIgnoreCase("") ) {
                    apiHitSelectVehicle();
                    // context.startActivity(new Intent(this, ActivityRechargeWallet.class));
                    //finish();
                }
                break;

            case R.id.imgAdd:
                context.startActivity(new Intent(this, ActivityAddTransportDetail.class));
                break;

            case R.id.imgBackArrow:
                finish();
                break;
        }

    }


    private void toolbarSetting() {

        binding.layout.tvLayoutName.setVisibility(View.VISIBLE);
        binding.layout.tvLayoutName.setText("Mode Of Transport");
        binding.layout.imgAdd.setVisibility(View.VISIBLE);
        binding.layout.imgAdd.setOnClickListener(this);
        binding.layout.imgAdd.setImageResource(R.drawable.add_icon);
        binding.layout.imgBackArrow.setVisibility(View.GONE);

    }

    @Override
    public void onNeedToSendSignUp() {
        if (progressDialog.isShowing())progressDialog.dismiss();

    }

    @Override
    protected void onResume() {
        super.onResume();
        apiHitAllModes();
    }

    @Override
    public void onSessionExpire() {
        if (progressDialog.isShowing())progressDialog.dismiss();

    }
    @Override
    public void onClickonModeOfTransport(int id) {
        AdapterTransportMode.updateBackgroundColor(id);

    }

    @Override
    public void click(int id,String name) {
        this.vahId = String.valueOf(id);
        this.selectedName=name;
        Log.d(TAG, "click: "+id);
    }


}