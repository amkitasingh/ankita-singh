package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityChangePasswordBinding;

import java.util.HashMap;

public class ChangePasswordActivity extends BaseActivity {
    private ActivityChangePasswordBinding binding;
    Context context;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        context = this;
        progressDialog = new ProgressDialog(this);
        binding.viewResetBtn.setOnClickListener(this);
        binding.etPassword.requestFocus();
        binding.etPassword.setFocusableInTouchMode(true);
        toolbarSetting();
    }

    private void toolbarSetting() {

        binding.imgBack.setVisibility(View.VISIBLE);
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(int viewId, View v) {
        switch (v.getId()) {
            case R.id.viewResetBtn:
                if (validation(v))
                    hitChangePassApi();
                break;
        }

    }


    private void hitChangePassApi() {
        if (!isFinishing()) progressDialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_old_password, binding.etPassword.getText().toString());
        map.put(AppConstants.key_new_password, binding.etPassword1.getText().toString());
        map.put(AppConstants.key_role, "tranporter");

        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_CHANGE_PASSWORD),
                UrlFactory.getAppHeaders(), this);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (!isFinishing()) progressDialog.dismiss();
        if (responseObj instanceof ModelSuccess) {
            ModelSuccess modelSuccess = (ModelSuccess) responseObj;
            Toast.makeText(context, getString(R.string.txt_your_pass_has_been_changed), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ChangePasswordActivity.this, ActivityLogin.class));
            new PrefManager(context).setUserLogin(false);
            new PrefManager(context).clearPrefs();
            finishAffinity();
        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (!isFinishing()) progressDialog.dismiss();
    }

    public boolean validation(View view) {
        if (ValidationHelper.isNull(binding.etPassword.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, getString(R.string.txt_enter_old_password));
            return false;
        } else if (!ValidationHelper.isValidPassword(binding.etPassword.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Password should be in between 6 to 20 characters");
            return false;
        } else if (ValidationHelper.isNull(binding.etPassword1.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, getString(R.string.txt_enter_new_password));
            return false;
        } else if (!ValidationHelper.isValidPassword(binding.etPassword1.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Password should be in between 6 to 20 characters");
            return false;
        } else if (ValidationHelper.isNull(binding.etPasswd1.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, getString(R.string.txt_enter_confirm_password));
            return false;
        } else if (!binding.etPassword1.getText().toString().equals(binding.etPasswd1.getText().toString())) {
            ValidationHelper.showSnackBar(view, getString(R.string.txt_new_conf_pass_do_not_match));
            return false;
        }
        return true;
    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
