package com.foodCarrierDeliveryBoy.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;


import com.foodCarrierDeliveryBoy.Adapters.TicketAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.CallBacks.PaginationCallback;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.AllTicketModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityTicketBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityTicket extends BaseActivity implements TicketAdapter.Callback, PaginationCallback {

    private Context context;
    private TicketAdapter adapter;
    ActivityTicketBinding binding;
    AllTicketModel model;
    ProgressDialog progressDialog;
    private String per_page = "10";
    private int currentPage = 1;
    private List<AllTicketModel.IssuesListBean> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ticket);
        context = this;
        progressDialog =new ProgressDialog(this);
        toolbarSetting();



        binding.tvReportIssue.setOnClickListener(this);
        binding.toolbar.tvLayoutName.setText("Issues Tracker");
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.imgAdd.setOnClickListener(this);
        adapter = new TicketAdapter(this, list,this,this);
        binding.ticketListView.setAdapter(adapter);

    }

    private void toolbarSetting() {
        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(int viewId, View view) {
      //
        switch (viewId){
            case R.id.imgBackArrow:
                finish();
                break;
            case R.id.tvReportIssue:

                startActivity(IntentHelper.getCreateTicketScreen(this));
                break;
        }
    }

    void apiShowAllTicket() {
        if (!progressDialog.isShowing()) {
            progressDialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        JSONObject jsonObject =new JSONObject();

        try {
            jsonObject.put("page",currentPage);
            jsonObject.put("per_page",per_page);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Authentication.object(this, UrlFactory.generateUrlWithVersion(AppConstants.URL_ALL_TICKET),this,jsonObject);


    }

    @Override
    public void onLoadMore(Object responseObj) {
        super.onLoadMore(responseObj);
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (!(responseObj instanceof AllTicketModel)) {
            return;
        }
        model = (AllTicketModel) responseObj;
        binding.ticketListView.hideMoreProgress();
        adapter.onMoreDataReq(model.getIssues_list());
        if (binding.ticketListView.isLoadingMore()) {
            binding.ticketListView.setLoadingMore(false);
        }
        if (model.getIssues_list().size() > 0 && model.getIssues_list().size() < 9) {
            binding.ticketListView.removeMoreListener();
        }
        if (model.getIssues_list().size() == 0)
            binding.ticketListView.removeMoreListener();
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);

        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (responseObj instanceof AllTicketModel) {
            model = (AllTicketModel) responseObj;
            if ((model.getIssues_list().size()!=0)){
                adapter.addData((ArrayList<AllTicketModel.IssuesListBean>) model.getIssues_list());
                Log.d("TAG", String.valueOf(model.getIssues_list().size()));
            }


        }
    }


    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        //Toast.makeText(ActivityTicket.this, "Details not Saved Successfully.", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        apiShowAllTicket();
    }

    @Override
    public void loadNextPage() {
        ++currentPage;

        apiShowAllTicket();

    }




    @Override
    public void onClickonTicket(String support_id,String issueDate,String status) {

        context.startActivity(new Intent(context, ActivityChat.class)
                .putExtra("supportId",support_id)
                .putExtra("issueDate",issueDate)
                .putExtra("issueStatus",status));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}