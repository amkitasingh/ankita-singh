package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityIssuesTrackerBinding;

public class ActivityIssuesTracker extends BaseActivity {
private ActivityIssuesTrackerBinding binding;
Context context;
ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding= DataBindingUtil. setContentView(this,R.layout.activity_issues_tracker);
       context=this;
       progressDialog=new ProgressDialog(this);

       toolbarSetting();
    }

    private void toolbarSetting() {


        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Issues Tracker");
        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(int viewId, View view) {

    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
