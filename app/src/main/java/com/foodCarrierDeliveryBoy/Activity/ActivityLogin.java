package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.UserModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityLoginBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.DEVICE_TYPE_INPUT;
import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.key_input_role;


public class ActivityLogin extends BaseActivity implements View.OnClickListener  {
    private Context context;
    private ActivityLoginBinding binding;
    private ProgressDialog dialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
        }


        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        context = this;
        dialog = new ProgressDialog(this);

        binding.viewMobile.setFocusableInTouchMode(true);
        binding.viewMobile.requestFocus();

        FirebaseApp.initializeApp(context);
        binding.ccp.registerPhoneNumberTextView(binding.viewMobile);
        binding.viewSignBtn.setOnClickListener(this);
        binding.viewLoginBtn.setOnClickListener(this);
        binding.viewForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {

            case R.id.viewLoginBtn:
                if (validation(view)) {
                    hitLoginApi();
                }
                break;
            case R.id.viewForgotPassword:
                startActivity(IntentHelper.getForgotPassword(context));
                break;
            case R.id.viewSignBtn:
                startActivity(IntentHelper.getSignUp(context));
                break;

        }
    }


    private void hitLoginApi() {
        if (!isFinishing() && !dialog.isShowing()) {
            dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_country_code, binding.ccp.getSelectedCountryCodeWithPlus());
        map.put(AppConstants.key_contact, binding.viewMobile.getText().toString());
        map.put(AppConstants.key_password, binding.etPassword.getText().toString());
        map.put(AppConstants.key_role, key_input_role);
        map.put(AppConstants.DEVICE_TYPE, DEVICE_TYPE_INPUT);
        map.put(AppConstants.DEVICE_ID, FirebaseInstanceId.getInstance().getToken());
        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_SIGN_IN),
                UrlFactory.getDefaultHeaders(), ActivityLogin.this);
    }

    public void apiHitUpdateId() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.DEVICE_TYPE, "android");
            jsonObject.put(AppConstants.DEVICE_ID, FirebaseInstanceId.getInstance().getToken());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("ankitaaa", FirebaseInstanceId.getInstance().getToken());
        Authentication.object(this, UrlFactory.generateUrlWithVersion(AppConstants.URL_UPDATE_DEVICE_TOKEN), this, jsonObject);
    }


    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (responseObj instanceof UserModel) {
            UserModel model = (UserModel) responseObj;
            PrefManager.getInstance(context).setUserDetail(model);
            PrefManager.getInstance(context).setKeyIsLoggedIn(true);
            PrefManager.getInstance(context).setModeOfTransport(model.getUser().getSelected_vehicle().getName());
            apiHitUpdateId();
            /* */  if (model.getUser().getSelected_vehicle()!= null && model.getUser().getSelected_vehicle().getId() != null && !model.getUser().getSelected_vehicle().getId().equals("")) {
                startActivity(IntentHelper.getDashboard(context));
            }else
                startActivity(IntentHelper.getModeTransport(context));
                finish();
        }
        if (responseObj instanceof String) {
            if (responseObj.equals("success")) {
                finish();
            }
        }
    }

    public boolean validation(View view) {
        if (ValidationHelper.isValidNumber(binding.viewMobile.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Please Enter Mobile Number");
            binding.viewMobile.requestFocus();
            return false;
        } else if (!binding.ccp.isValid()) {
            ValidationHelper.showSnackBar(view, "Enter valid country code");
            return false;
        } else if (ValidationHelper.isNull(binding.etPassword.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Please Enter Password");
            return false;

        } else if (!ValidationHelper.isValidPassword(binding.etPassword.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Password should be in between 6 to 20 characters");
            return false;
        }
        return true;
    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }


}


