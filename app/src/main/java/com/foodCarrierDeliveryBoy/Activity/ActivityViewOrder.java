package com.foodCarrierDeliveryBoy.Activity;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.foodCarrierDeliveryBoy.Adapters.OrderListPickAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Models.ModelViewOrder;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityOrderDetailBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActivityViewOrder extends BaseActivity {
    ArrayList<ModelViewOrder.OrderDetailsBean.MenuItemBean> list = new ArrayList<>();
    ActivityOrderDetailBinding binding;
    String orderID;

    @Override
    public void onClick(int viewId, View view) {
        switch (viewId) {
            case R.id.imgBackArrow:
                finish();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);
        hitApiViewOrder();
        binding.toolbar.imgBackArrow.setOnClickListener(this);

        if (getIntent() != null) {
            orderID = getIntent().getStringExtra("order_id");
        }
    }

    private void hitApiViewOrder() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.Order_id, orderID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Authentication.object(this, UrlFactory.generateUrlWithVersion(AppConstants.URL_VIEW_ORDER), this, jsonObject);
    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        dismissLoader();
        if (responseObj instanceof ModelViewOrder) {
            ModelViewOrder modelViewOrder = (ModelViewOrder) responseObj;
            list.clear();
            list.addAll(modelViewOrder.getOrder_details().getMenu_item());

            OrderListPickAdapter OrderListPickAdapter = new OrderListPickAdapter(this, list, "viewOrder");
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            binding.recyclerView.setAdapter(OrderListPickAdapter);
        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        dismissLoader();
    }
}
