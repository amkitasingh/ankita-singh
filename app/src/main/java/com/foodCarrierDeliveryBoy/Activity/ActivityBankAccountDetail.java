package com.foodCarrierDeliveryBoy.Activity;


import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityBankAccountDetailBinding;


public class ActivityBankAccountDetail extends BaseActivity  {
    private ActivityBankAccountDetailBinding binding;
    private Context context ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_bank_account_detail);
        context = this;
        toolbarSetting();


        binding.viewSubmit.setOnClickListener(this);
        binding.include.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

    }

    @Override
    public void onClick(int viewId, View view) {

        Intent intent = new Intent(this, BankAddedSuccessfully.class);
        startActivity(intent);

    }

    private void toolbarSetting() {

        binding.include.imgBackArrow.setVisibility(View.VISIBLE);
        binding.include.tvLayoutName.setVisibility(View.VISIBLE);
        binding.include.tvLayoutName.setText("Bank Accounts");
        binding.include.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
