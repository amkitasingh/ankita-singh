package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Adapters.CardAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityAddCardBinding;

import java.util.ArrayList;
import java.util.List;


public class ActivityAddCard extends BaseActivity {
private ActivityAddCardBinding binding;
private Context context;
ProgressDialog progressDialog;
    private List<String> str = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_add_card);
        context=this;
        progressDialog=new ProgressDialog(this);

        toolbarSetting();


        progressDialog = new ProgressDialog(this);
        context =this;
        str.add("");
        str.add("");

        CardAdapter adapter = new CardAdapter(context, str);
        binding.cardListView.setAdapter(adapter);

        binding.viewAddNewBtn.setOnClickListener(this);
        binding.toolbar.imgBackArrow.setOnClickListener(this);
    }


    private void toolbarSetting() {
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Cards");
        binding.toolbar.imgAdd.setVisibility(View.VISIBLE);
        binding.toolbar.imgAdd.setImageResource(R.drawable.cards_icon);

    }
    @Override
    public void onClick(int viewId, View view) {
switch (view.getId()){
    case R.id.viewAddNewBtn:
        context.startActivity(new Intent(this,ActivityAddCardDetail.class));
        break;
    case R.id.imgBackArrow:
        finish();
        break;
}

    }



    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}

