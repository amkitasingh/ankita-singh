package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Adapters.AdapterActiveCard;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;

import com.foodCarrierDeliveryBoy.databinding.ActivityAddMoneyBinding;
import com.quentindommerc.superlistview.SuperListview;

import java.util.ArrayList;
import java.util.List;

public class ActivityAddMoney extends BaseActivity {

    private ActivityAddMoneyBinding binding;
    private Context context;
    private ProgressDialog dialog;
    private List<String> str = new ArrayList<>();
    private AdapterActiveCard adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_money);
        context = this;
        dialog = new ProgressDialog(this);//SessionToken

        binding.viewAddMoney.setOnClickListener(this);
        toolbarSetting();

        str.add("");
        str.add("");
        str.add("");


        SuperListview listview = (SuperListview) findViewById(R.id.bankWalletListView);
        adapter = new AdapterActiveCard(context, str);
        listview.setAdapter(adapter);
        binding.toolbar.imgBackArrow.setOnClickListener(this);


    }



   @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.viewAddMoney:
                context.startActivity(new Intent(this, ActivityAddCard.class));
                break;
            case R.id.imgBackArrow:
                finish();
                break;
        }
    }

    private void toolbarSetting() {

        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Wallet");

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
