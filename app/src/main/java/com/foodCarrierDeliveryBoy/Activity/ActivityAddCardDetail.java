package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityAddCardDetailBinding;
import com.ebanx.swipebtn.OnStateChangeListener;

public class ActivityAddCardDetail extends BaseActivity {
private ActivityAddCardDetailBinding binding;
private Context context;
ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_add_card_detail);
        context=this;
        toolbarSetting();
    }

    private void toolbarSetting() {
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Add Cards");
        binding.toolbar.imgAdd.setVisibility(View.GONE);
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        binding.confirmButton.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {

            }
        });
    }

    @Override
    public void onClick(int viewId, View view) {
        switch (viewId){
            case R.id.imgBackArrow:
                finish();
                break;
        }
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
