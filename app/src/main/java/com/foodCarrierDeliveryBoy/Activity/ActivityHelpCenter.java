package com.foodCarrierDeliveryBoy.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.ContactUsModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityHelpCentreBinding;


public class ActivityHelpCenter extends BaseActivity {
    ActivityHelpCentreBinding binding;
    private ProgressDialog progressDialog;
    private Context context;
    ContactUsModel model;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            binding = DataBindingUtil.setContentView(this, R.layout.activity_help_centre);
            context = this;

            toolbarSetting();
            progressDialog = new ProgressDialog(context);
            hitContactApi();

            binding.tvSupport.setOnClickListener(this);
            binding.linearAbout.setOnClickListener(this);
            binding.linearTerm.setOnClickListener(this);
            binding.linearOrder.setOnClickListener(this);

        }

        @Override
        public void onClick(int viewId, View view) {

            switch (view.getId()) {

                case R.id.linearAbout:
                    Intent in = new Intent(context, ActivityAboutUs.class);
                    startActivity(in);
                    break;
                case R.id.tvSupport:
                    Intent i = new Intent(context, ActivityFAQ.class);
                    startActivity(i);
                    break;
                case R.id.viewTicket:
                    startActivity(new Intent(context, ActivityTicket.class));

                    break;

            }
        }

        private void toolbarSetting() {

            binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
            binding.toolbar.tvLayoutName.setText("Help Centre");
            binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
            binding.toolbar.viewTicket.setVisibility(View.VISIBLE);
            binding.toolbar.viewTicket.setOnClickListener(this);
            binding.toolbar.imgBackArrow.setImageResource(R.drawable.back_icon);
            binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        private void hitContactApi() {
            Authentication.get(AppConstants.URL_CONTACT_US, this, false);

        }

        @Override
        public void onTaskSuccess(Object responseObj) {
            super.onTaskSuccess(responseObj);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            if (responseObj instanceof ContactUsModel) {
                model = (ContactUsModel) responseObj;
                binding.viewCountryCode.setText(ValidationHelper.optional(model.getContact_us().getCountry_code()));
                binding.viewContact.setText(ValidationHelper.optional(model.getContact_us().getContact()));
                binding.viewAddress.setText(ValidationHelper.optional(model.getContact_us().getAddress()));
                binding.viewEmail.setText(ValidationHelper.optional(model.getContact_us().getEmail()));

            }
        }

        @Override
        public void onNeedToSendSignUp() {

        }

        @Override
        public void onSessionExpire() {

        }
    }
