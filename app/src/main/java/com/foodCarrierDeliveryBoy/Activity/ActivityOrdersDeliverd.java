package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityOrdersDeliverdBinding;

public class ActivityOrdersDeliverd extends BaseActivity {
ActivityOrdersDeliverdBinding binding;

    @Override
    public void onClick(int viewId, View view) {
        switch (viewId){
            case R.id.imageView8:
                finish();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_orders_deliverd);
        binding.toolbar.tvSubTitle.setText("Orders Delivered");
        binding.toolbar.imageView8.setOnClickListener(this);



    }




    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
