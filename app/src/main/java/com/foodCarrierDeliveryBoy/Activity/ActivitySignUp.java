package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.ModelContactExist;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.AppMethods;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivitySignUpBinding;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;

import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.key_input_role;

public class ActivitySignUp extends BaseActivity implements AdapterView.OnItemSelectedListener {
    private ActivitySignUpBinding binding;
    private Context context;
    private ProgressDialog dialog;
    String title;
    private GoogleApiClient googleApiClient;
    private static final int SIGN_IN=1;
    final String GOOGLE_ERROR = "Couldn't sign in with Google";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        context = this;
        dialog = new ProgressDialog(this);


        binding.ccp.registerPhoneNumberTextView(binding.viewMobile);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.numbers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinnerTitle.setAdapter(adapter);
        binding.spinnerTitle.setOnItemSelectedListener(this);
        title = binding.spinnerTitle.getSelectedItem().toString();

        binding.viewSignBtn.setOnClickListener(this);
        binding.viewGoogle.setOnClickListener(this);
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        AppMethods.showKeyboard();
        binding.viewName.requestFocus();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }


    @Override
    public void onClick(int viewId, View v) {
        switch (v.getId()) {
            case R.id.viewSignBtn:
                if (validation(v)) {
                    //hitSignUpApi();
                    hitCheckContactApi();
                }
                break;
            case R.id.viewGoogle:
               context.startActivity(new Intent(this,GoogleActivity.class));
               break;
            case R.id.imgBackArrow:
                finish();
                break;
        }
    }

    private void hitCheckContactApi() {
        if (!isFinishing() && !dialog.isShowing()) {
            dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_contact, binding.viewMobile.getText().toString());
        map.put(AppConstants.key_country_code, binding.ccp.getSelectedCountryCodeWithPlus());
        map.put(AppConstants.key_role, key_input_role);
        Authentication.objectRequestUserExist(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_CONTACT_EXIST),
                UrlFactory.getDefaultHeaders(), this);
    }

        /*private void hitSignUpApi() {
     if (!isFinishing() && !dialog.isShowing()) {
        dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
    }
    HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_title,title);
        map.put(AppConstants.key_name,binding.viewName.getText().toString());
        map.put(AppConstants.key_contact, binding.viewMobile.getText().toString());
        map.put(AppConstants.key_country_code, binding.ccp.getSelectedCountryCodeWithPlus());
        map.put(AppConstants.key_role,key_input_role);
        map.put(AppConstants.key_password, binding.etPassword.getText().toString());
        map.put(AppConstants.DEVICE_TYPE, DEVICE_TYPE_INPUT);
        map.put(AppConstants.DEVICE_ID, UrlFactory.getDeviceToken());
          Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_SIGN_UP),
                UrlFactory.getDefaultHeaders(), this);

}*/

    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }

            if (responseObj instanceof ModelContactExist){
                ModelContactExist model = (ModelContactExist) responseObj;
                if (model.getCode() == 404){
                    startActivity(IntentHelper.getOtpVerification(context)
                            .putExtra(AppConstants.key_contact, binding.viewMobile.getText().toString())
                            .putExtra(AppConstants.key_country_code, binding.ccp.getSelectedCountryCodeWithPlus())
                            .putExtra(AppConstants.key_name, binding.viewName.getText().toString())
                            .putExtra(AppConstants.key_name,title)
                            .putExtra(AppConstants.key_password, binding.etPassword.getText().toString())
                            .putExtra("isSignUp", true));
                }else {
                    Toast.makeText(context, model.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }


    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    public boolean validation(View view) {
       if (ValidationHelper.isNull(binding.viewName.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Please Enter Full  Name");
            return false;
        } else if (ValidationHelper.isValidNumber(binding.viewMobile.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Please Enter Mobile Number");
            binding.viewMobile.requestFocus();
            return false;
        } else if (!binding.ccp.isValid()) {
            ValidationHelper.showSnackBar(view, "Enter valid mobile number");
            return false;
        } else if (ValidationHelper.isNull(binding.etPassword.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Please Enter Password");
            return false;

        } else if (!ValidationHelper.isValidPassword(binding.etPassword.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Password should be in between 6 to 20 characters");
            return false;
        }

        return true;

    }



    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
