package com.foodCarrierDeliveryBoy.Activity;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;


import com.foodCarrierDeliveryBoy.Adapters.FaqAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Models.Faqs;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityFAQBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActivityFAQ extends BaseActivity implements FaqAdapter.Callback {
ActivityFAQBinding binding;
ArrayList<Faqs.FaqsBean> faqsBeans=new ArrayList<>();
    FaqAdapter adapter;

    @Override
    public void onClick(int viewId, View view) {
        switch (viewId){
            case R.id.imgBackArrow:
                finish();
                break;
            case R.id.imgAdd://actually a all ticket show button
                startActivity(IntentHelper.getAllTicketScreen(this));
                break;
            case R.id.buttonReachUs:
                startActivity(IntentHelper.getCreateTicketScreen(this));
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding= DataBindingUtil. setContentView(this,R.layout.activity_f_a_q);

toolbarSetting();
        binding.buttonReachUs.setOnClickListener(this);
        getFaqs();


    }

    private void getFaqs() {

        JSONObject jsonObject=new JSONObject();

        try {
            jsonObject.put("perPage","10");
            jsonObject.put("pageNo","1");
            jsonObject.put("type","user");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Authentication.object(this, UrlFactory.generateUrlWithVersion(AppConstants.URL_FAQ),this,jsonObject);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);

        if(responseObj instanceof Faqs){

            Faqs faq = (Faqs) responseObj;
            faqsBeans.addAll(faq.getFaqs());
           adapter=new FaqAdapter(this,faqsBeans,this);
            binding.listFaq.setAdapter(adapter);

        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    private void toolbarSetting() {
        binding.toolbar.imgBackArrow.setImageResource(R.drawable.back_arrow);
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("FAQ & Support");}

    @Override
    public void isExpandedOrNot(int position, boolean isExpanded) {
        if (isExpanded){
            adapter.setExpandable(position,false);
        }else {
            adapter.setExpandable(position,true);
        }

    }
}