package com.foodCarrierDeliveryBoy.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;


import com.foodCarrierDeliveryBoy.Adapters.AllChatMessagesAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Models.CreateTicketReplyListModel;
import com.foodCarrierDeliveryBoy.Models.ModelChatAllMessage;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.AppMethods;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityChatBinding;

import java.util.ArrayList;
import java.util.HashMap;

public class ActivityChat extends BaseActivity  {
    ActivityChatBinding binding;

    ArrayList<ModelChatAllMessage.TicketBean> allMessagesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        binding.toolbar.tvLayoutName.setText("Issue Id : "+getIntent().getStringExtra("supportId"));
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        binding.tvIssueDate.setText(getIntent().getStringExtra("issueDate"));
        binding.tvIssueStatus.setText(getIntent().getStringExtra("issueStatus"));
       // getPredefineMessages();
        allMessages();
        binding.sendButton.setOnClickListener(this);

        binding.etMessageBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    AppMethods.showKeyboard();

                }
            }
        });


    }

    @Override
    public void onClick(int viewId, View view) {
        switch (viewId) {
            case R.id.send_button:
                if (!ValidationHelper.isNull(binding.etMessageBox.getText().toString())) {
                    sendMessageApi(binding.etMessageBox.getText().toString());
                } else {
                    Toast.makeText(this, "Enter Message !", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imgBackArrow:
                finish();
                break;

        }
    }


        private void allMessages () {

            HashMap<String, String> hashMap = new HashMap<>();
            Toast.makeText(this, getIntent().getStringExtra("supportId"), Toast.LENGTH_SHORT).show();
            hashMap.put("support_id", getIntent().getStringExtra("supportId"));
            hashMap.put(AppConstants.kPerPage, "10");
            hashMap.put(AppConstants.kPage,"1");
            Log.d("requestt",hashMap.toString());
            Authentication.postWithAppVersion(AppConstants.URL_ALL_MESSAGES_CHAT, hashMap, this, false);

        }

        private void sendMessageApi (String message){
            binding.etMessageBox.setText("");

            HashMap<String, String> hashMap = new HashMap<>();
            //hashMap.put("support_id", getIntent().getStringExtra("supportId"));
            hashMap.put("support_id", getIntent().getStringExtra("supportId"));
            hashMap.put("reply", message);
            Authentication.postWithAppVersion(AppConstants.URL_SEND_MESSAGE_CHAT, hashMap, this, false);

        }


        @Override
        public void onTaskError (String errorMsg){
            super.onTaskError(errorMsg);

        }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
        public void onTaskSuccess (Object responseObj){
            super.onTaskSuccess(responseObj);

            if (responseObj instanceof ModelChatAllMessage) {
                allMessagesList.clear();
                ModelChatAllMessage modelChatAllMessage = (ModelChatAllMessage) responseObj;
                if (modelChatAllMessage.getTicket().size() > 0) {
                    allMessagesList.addAll(modelChatAllMessage.getTicket());
                }

                AllChatMessagesAdapter allChatMessagesAdapter = new AllChatMessagesAdapter(this, allMessagesList);
                //  binding.allChatListListview.setLayoutManager(new LinearLayoutManager(this));
                binding.allChatListListview.setDivider(null);


                binding.allChatListListview.setAdapter(allChatMessagesAdapter);
                if (allMessagesList.size() > 0) {

                    binding.allChatListListview.setSelection(allMessagesList.size() - 1);//use to focus the item with index
                    allChatMessagesAdapter.notifyDataSetChanged();
                }
                //binding.editboxMessage.requestFocus();
            }
            else if (responseObj instanceof CreateTicketReplyListModel) {
                CreateTicketReplyListModel model = (CreateTicketReplyListModel) responseObj;
                allMessages();
            }
    }

}

