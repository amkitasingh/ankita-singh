package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityBankAddedSuccessfullyBinding;

public class BankAddedSuccessfully extends BaseActivity {
    private ActivityBankAddedSuccessfullyBinding binding;
    private Context context ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding= DataBindingUtil. setContentView(this,R.layout.activity_bank_added_successfully);
        context = this;
        toolbarSetting();
    }

    private void toolbarSetting() {

        binding.include.imgBackArrow.setVisibility(View.VISIBLE);
        binding.include.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public void onClick(int viewId, View view) {

    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
