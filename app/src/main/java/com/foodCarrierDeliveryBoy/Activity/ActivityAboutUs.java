package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityAboutUsBinding;

public class ActivityAboutUs extends BaseActivity {
    private ActivityAboutUsBinding binding;
    Context context;

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView( this,R.layout.activity_about_us);
        context =this;
        progressDialog=new ProgressDialog(this);

        toolbarSetting();
        binding.viewAbout.setOnClickListener(this);
    }
    private void toolbarSetting() {


        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("About Us");
        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override

    public void onClick(int viewId, View view) {
        switch (view.getId()){
            case R.id.viewAbout:
                startActivity(IntentHelper.getViewWebsite(context));
                break;
        }
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}

