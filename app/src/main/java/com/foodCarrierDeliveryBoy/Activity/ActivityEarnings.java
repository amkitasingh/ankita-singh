package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.foodCarrierDeliveryBoy.Adapters.CustomViewPager;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityEarningsBinding;

import java.util.ArrayList;
import java.util.List;

public class ActivityEarnings extends BaseActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {
    private ActivityEarningsBinding binding;
    private Context context;
    ProgressDialog progressDialog;
    private CharSequence titles[];
    int numbOfTabs = 2;
    String title;
    List<String> dropText = new ArrayList<>();
    String[] graphRequestType;
    Spinner spinner;


    @Override
    public void onClick(int viewId, View view) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_earnings);
        context = this;
        progressDialog = new ProgressDialog(this);


        toolbarSetting();
        init();
    }

    void init() {
        binding.viewTodayTab.setOnClickListener(this);
        binding.viewWeekTab.setOnClickListener(this);


        CustomViewPager pagerAdapter = new CustomViewPager(getSupportFragmentManager(), titles, numbOfTabs);

        binding.viewpager.setAdapter(pagerAdapter);
        binding.viewpager.setCurrentItem(0);
        binding.viewpager.addOnPageChangeListener(this);

        binding.viewTodayTab.setTextColor(getResources().getColor(R.color.orange));
        binding.viewTodayTab.setBackground(getResources().getDrawable(R.drawable.week_active_btn));
        binding.viewWeekTab.setTextColor(getResources().getColor(R.color.white));
        binding.viewWeekTab.setBackground(getResources().getDrawable(R.drawable.today_nonactive_btn));
        binding.toolbar.imgBackArrow.setOnClickListener(this);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
    @Override
    public void onPageSelected(int position) {
        if (position == 0) {

            binding.viewTodayTab.setTextColor(getResources().getColor(R.color.orange));
            binding.viewTodayTab.setBackground(getResources().getDrawable(R.drawable.week_active_btn));
            binding.viewWeekTab.setTextColor(getResources().getColor(R.color.white));
            binding.viewWeekTab.setBackground(getResources().getDrawable(R.drawable.today_nonactive_btn));

        }

        else  if (position == 1) {

            binding.viewTodayTab.setTextColor(getResources().getColor(R.color.white));
            binding.viewTodayTab.setBackground(getResources().getDrawable(R.drawable.today_btn));
            binding.viewWeekTab.setTextColor(getResources().getColor(R.color.orange));
            binding.viewWeekTab.setBackground(getResources().getDrawable(R.drawable.week_btn));


        }
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void onClickNext(int tab) {
        binding.viewpager.setCurrentItem(tab);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.viewTodayTab:
                onClickNext(0);
                break;
            case R.id.viewWeekTab:
                binding.viewSpinner.setVisibility(View.INVISIBLE);
                binding.viewSpinner.performClick();

                onClickNext(1);
                break;
            case R.id.imgBackArrow:
                finish();
                break;

            default:
                break;
        }
    }

    private void toolbarSetting() {

        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setImageResource(R.drawable.back_arrow1);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Earnings");
        binding.toolbar.imgAdd.setVisibility(View.VISIBLE);
        binding.toolbar.imgAdd.setImageResource(R.drawable.earning_icon_);
        binding.viewWeekTab.setText(binding.viewSpinner.getSelectedItem().toString());
        binding.viewSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Spinner sp = adapterView.findViewById(R.id.viewSpinner);
                binding.viewWeekTab.setText(String.valueOf(sp.getSelectedItem()));
                binding.viewSpinner.setVisibility(View.GONE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

}