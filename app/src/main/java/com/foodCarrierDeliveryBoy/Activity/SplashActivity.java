package com.foodCarrierDeliveryBoy.Activity;


import android.Manifest;
import android.content.Context;
import android.content.Intent;

import android.os.Handler;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.foodCarrierDeliveryBoy.databinding.ActivitySplashBinding;
import com.google.firebase.iid.FirebaseInstanceId;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.GuestModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.GuestPrefrenceManager;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;


import java.util.HashMap;
import java.util.List;

import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.DEVICE_TYPE;
import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.DEVICE_TYPE_INPUT;


public class SplashActivity extends BaseActivity implements PermissionListener {
    private ActivitySplashBinding binding;
    private Context context;
    private int SPLASH_DISPLAY_LENGTH = 2000;
    private String device_id,notificationType;
    private Bundle bundle;
    ProgressDialog dialog;

    @Override
    public void onClick(int viewId, View view) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window w=getWindow();
        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            //  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
        }*/

        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        setContentView(R.layout.activity_splash);
        context = this;
        dialog = new ProgressDialog(context);
        permission();
    }



    void data() {


        if (PrefManager.getInstance(this).getKeyIsLoggedIn()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(context, ActivityDashboard.class));
                    finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, ActivityLogin.class);
                    startActivity(intent);
                    finishAffinity();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    public void permission() {
        TedPermission.with(this)
                .setPermissionListener(this)
                .setDeniedMessage(getString(R.string.txt_permission_text_dialog))
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        hitGuestToken();
    }

    @Override
    public void onPermissionDenied(List<String> deniedPermissions) {
        Toast.makeText(context, getString(R.string.txt_permission_denied), Toast.LENGTH_SHORT).show();
    }


    private void hitGuestToken(){
        if (!dialog.isShowing()) dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        HashMap<String, String> map = new HashMap<>();
        map.put(DEVICE_TYPE, DEVICE_TYPE_INPUT);
        map.put(AppConstants.DEVICE_ID, FirebaseInstanceId.getInstance().getToken());

        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_CREATE_GUEST),
                UrlFactory.getGuestHeaders(), SplashActivity.this);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (responseObj instanceof GuestModel){
            GuestModel model = (GuestModel) responseObj;
            new GuestPrefrenceManager(context).saveGuestToken((model.getGuest_token()));
            data();

        }

    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }


}
