package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Adapters.OrderHistoryAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ErrorDialogSimple;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.OrderHistoryDetailModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.databinding.ActivityOrderLeftMenuBinding;

import com.quentindommerc.superlistview.OnMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityOrderLeftMenu extends BaseActivity implements OnMoreListener{
    private ActivityOrderLeftMenuBinding binding;
    ProgressDialog progressDialog;
    private List<OrderHistoryDetailModel.OrdersBean> OrdersBean = new ArrayList<>();
    private Context context;
    private OrderHistoryAdapter adapter;
    String per_page="10";
    int page = 1;


    @Override
    public void onClick(int viewId, View view) {
        switch (viewId){
            case R.id.imgBackArrow:
                finish();
                break;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_left_menu);
        context = this;
        toolbarSetting();

        hitOrderHistory();


        binding.toolbar.imgBackArrow.setOnClickListener(this);
    }



    private void toolbarSetting() {

        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setImageResource(R.drawable.back_arrow);
        binding.toolbar.tvLayoutName.setText("Orders History");
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);


    }

    private void hitOrderHistory() {
        HashMap<String, String> map = new HashMap<>();

        map.put(AppConstants.key_page, String.valueOf(page));
        map.put(AppConstants.key_per_page, per_page);
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants
                .URL_ORDER_HISTORY), UrlFactory.getAppHeaders(), this);
    }


    @Override
    public void onLoadMore(Object responseObj) {
        super.onLoadMore(responseObj);
        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (!(responseObj instanceof OrderHistoryDetailModel)) {
            return;
        }
        OrderHistoryDetailModel  model = (OrderHistoryDetailModel) responseObj;
        binding.orderListView.hideMoreProgress();
        adapter.onMoreDataReq(model.getOrders());
        if (binding.orderListView.isLoadingMore()) {
            binding.orderListView.setLoadingMore(false);
        }
        if (model.getOrders().size() > 0 && model.getOrders().size() < 9) {
            binding.orderListView.removeMoreListener();
        }
        if (model.getOrders().size() == 0)
            binding.orderListView.removeMoreListener();
    }

    @Override
    public void onTaskSuccess(Object responseObj) {

        if (responseObj instanceof OrderHistoryDetailModel) {
            OrderHistoryDetailModel model = (OrderHistoryDetailModel) responseObj;
            if (OrdersBean.size() > 0) {
                OrdersBean.clear();
            }
            OrdersBean.addAll(model.getOrders());
            if (adapter == null) {
                adapter = new OrderHistoryAdapter(context, model.getOrders());
                binding.orderListView.setAdapter(adapter);
            } else {
                adapter.refresh(model.getOrders());
            }
            if (OrdersBean != null && OrdersBean.size() > 10) {
                binding.orderListView.setupMoreListener(this, 1);
                if (binding.orderListView.isLoadingMore()) {
                    binding.orderListView.setLoadingMore(false);
                }
            } else {
                page = 1;
                binding.orderListView.removeMoreListener();
            }
        }
    }
    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        ErrorDialogSimple.show(this, errorMsg);
    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    @Override
    public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {

    }
}
