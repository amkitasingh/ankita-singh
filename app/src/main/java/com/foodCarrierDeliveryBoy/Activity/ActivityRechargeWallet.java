package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityRechargeWalletBinding;


public class ActivityRechargeWallet extends BaseActivity {
private ActivityRechargeWalletBinding binding;
private Context context;
private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_recharge_wallet);
        context=this;
        dialog= new ProgressDialog(this);

        binding.imageWallet.setOnClickListener(this);
        binding.viewSkip.setOnClickListener(this);
    }
    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()){
            case R.id.imageWallet:
                context.startActivity(new Intent(this, ActivityAddMoney.class));
                break;
            case R.id.viewSkip:
                context.startActivity(new Intent(this,ActivityDashboard.class));
                break;
        }

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
