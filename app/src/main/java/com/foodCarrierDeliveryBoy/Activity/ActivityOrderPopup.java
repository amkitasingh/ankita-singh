package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Fragments.FragmentOrderScreen;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityOrderPopupBinding;

public class ActivityOrderPopup extends BaseActivity {
 private ActivityOrderPopupBinding binding;
 private Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding= DataBindingUtil.setContentView( this,R.layout.activity_order_popup);
         context=this;


         binding.mdDetails.acceptButton.setOnClickListener(this);
    }

    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()){
            case R.id.accept__button:
                context.startActivity(new Intent(this, FragmentOrderScreen.class));
                break;
        }

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
