package com.foodCarrierDeliveryBoy.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityViewWebsiteBinding;


public class ActivityViewWebsite extends AppCompatActivity {
    private WebView webview;
    private ActivityViewWebsiteBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_view_website);

        binding.webView.setWebViewClient(new WebViewClient());
        binding.webView.loadUrl("http://www.goggle.com");


    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }

}
