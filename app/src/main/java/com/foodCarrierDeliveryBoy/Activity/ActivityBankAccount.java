package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Adapters.BankAccountAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityBankAccountBinding;

import java.util.ArrayList;
import java.util.List;

public class ActivityBankAccount extends BaseActivity implements View.OnClickListener {
    private List<String> str = new ArrayList<>();
    private Context context;
    private BankAccountAdapter adapter;
    private ActivityBankAccountBinding binding;
    private ProgressDialog progressDialog;

    @Override
    public void onClick(int viewId, View view) {
        switch (viewId){
            case R.id.imgBackArrow:
                finish();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil. setContentView(this,R.layout.activity_bank_account);
        context = this;
        progressDialog=new ProgressDialog(this);
        toolbarSetting();
        str.add("anku");



        adapter = new BankAccountAdapter(context, str);
        binding.bankListView.setAdapter(adapter);
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        binding.toolbar.imgBackArrow.setOnClickListener(this);


        binding.viewAllNewBankAccount.setOnClickListener(this);


    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.viewAllNewBankAccount:
                context.startActivity(new Intent(context, ActivityBankAccountDetail.class));
                break;
            case R.id.imgBackArrow:
                finish();
                break;
        }
    }
    private void toolbarSetting() {

        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setImageResource(R.drawable.back_arrow1);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Bank Accounts");
        binding.toolbar.imgBackArrow.setOnClickListener(this);
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
