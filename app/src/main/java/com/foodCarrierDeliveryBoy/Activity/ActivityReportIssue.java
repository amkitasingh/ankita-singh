package com.foodCarrierDeliveryBoy.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.CallBacks.AddSupportTicketCallback;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.AddTicketModel;
import com.foodCarrierDeliveryBoy.Network.RetrofitClient;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityReportIssueBinding;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class ActivityReportIssue extends BaseActivity implements
        AddSupportTicketCallback, PermissionListener {
    private ActivityReportIssueBinding binding;
    private Context context;
    private ProgressDialog progressDialog;
    public static final int INTENT_REQUEST_GET_IMAGES = 13;
    private Uri resultUri;
    File compressedImageFile;
    private String imagePath,ticketId,Datetime;
    AddTicketModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        if (getIntent() != null) {
            ticketId = getIntent().getStringExtra("ticketId");
        }
        if (getIntent() != null) {
            Datetime = getIntent().getStringExtra("Date");
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_report_issue);
        context = this;
        progressDialog = new ProgressDialog(this);
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        toolbarSetting();

      /* Calendar calendar = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance().format(calendar.getTime());

        binding.viewTime.setText(currentDate);
        String currentTimeString = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());
        binding.viewTimeD.setText(currentTimeString);
*/
        //binding.viewTime.setText(ValidationHelper.optional(model.getTicket().getIssue_created_at()));
        // binding.viewOrdernumber.setText(ValidationHelper.optional(String.valueOf(model.getTicket().getOrder_id())));
        binding.ivWriteUs.setOnClickListener(this);
        binding.viewSubmitBtn.setOnClickListener(this);
        binding.viewUpload.setOnClickListener(this);

    }

    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.viewSubmitBtn:
                if (!ValidationHelper.isNull(binding.etWriteMessage.getText().toString())) {
                    HitAddTicketApi();
                } else {
                    Toast.makeText(context, "Please write issue details.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.view_upload:
                TedPermission.with(this)
                        .setPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .setPermissionListener(this)
                        .check();
                break;
            case R.id.imgBackArrow:
                finish();
                break;
        }

    }


    private void toolbarSetting() {
        binding.toolbar.tvLayoutName.setText("Report an issue");
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.imgBackArrow.setImageResource(R.drawable.back_arrow);
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void HitAddTicketApi() {
        if (!isFinishing() && !progressDialog.isShowing()) {
            progressDialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        String msg = binding.etWriteMessage.getText().toString();
        // startActivity(new Intent(this,ActivityFeedback.class));
     addSupportTicket( this,msg, imagePath, this);
    }



    @Override
    public void onLoadSuccess(AddTicketModel result) {
        if (!isFinishing() && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Toast.makeText(context, "Ticket Created Successfully", Toast.LENGTH_SHORT).show();
        binding.etWriteMessage.getText().clear();
        finish();
       // startActivity(new Intent(this,ActivityFeedback.class));

        //finish();
    }

    @Override
    public void onError(String s) {
        if (!isFinishing() && progressDialog.isShowing()) {
            Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }

    @Override
    public void onPermissionGranted() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(ActivityReportIssue.this);
    }
    @Override
    public void onPermissionDenied(List<String> deniedPermissions) {}


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INTENT_REQUEST_GET_IMAGES && resultCode == Activity.RESULT_OK) {

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                if (resultUri != null) {

                    try {
                        imagePath = resultUri.getPath();

                        Glide.with(context)
                                .load(imagePath)
                                .apply(new RequestOptions().placeholder(R.drawable.upload_image))
                                .into(binding.ivWriteUs);


                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    public static void addSupportTicket(Context context, String msg, String image,
                                        final AddSupportTicketCallback addSupportTicketCallback) {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("message", msg);


        if (image != null && !image.isEmpty()) {
            File leftFileImage = new File(image);
            builder.addFormDataPart("image", leftFileImage.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), leftFileImage));
        }

        final MultipartBody requestBody;
        requestBody = builder.build();

        Log.e("requestBody", new Gson().toJson(requestBody));
        Call<AddTicketModel> call = RetrofitClient.getClient().createTicket(requestBody);
        Log.w("PQST", new Gson().toJson(call.request()));


        call.enqueue(new Callback<AddTicketModel>() {

            @Override
            public void onResponse(Call<AddTicketModel> call, retrofit2.Response<AddTicketModel> response) {
                Log.d("responsee",String.valueOf(response.body().getCode()));
                try {
                    if (response != null && response.body() != null && response.body().getCode() == 200) {
                        addSupportTicketCallback.onLoadSuccess(response.body());
                        Log.d("Result", "onResponse===> " + response.body().getMessage());
                    } else {
                        addSupportTicketCallback.onError(response.toString());
                        Log.d("Result", "onResponse===> " + response.toString());
                    }
                } catch (Exception e) {
                    e.getLocalizedMessage();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddTicketModel> call, Throwable error) {
                addSupportTicketCallback.onError(error.toString());
                Log.d("Exception", "onFailure: " + error.toString());
            }
        });
    }


    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}