package com.foodCarrierDeliveryBoy.Activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.foodCarrierDeliveryBoy.Adapters.DrawerItemCustomAdapter;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.PopUpStackOrderDialog;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.FirebaseNotification.NotificationsBroadCast;
import com.foodCarrierDeliveryBoy.Fragments.FragmentFoodDeliveredList;
import com.foodCarrierDeliveryBoy.Fragments.FragmentOrderScreen;
import com.foodCarrierDeliveryBoy.Fragments.FragmentPickUpOrder;
import com.foodCarrierDeliveryBoy.Fragments.FragmentPickUpOrderItem;
import com.foodCarrierDeliveryBoy.Fragments.HomeFragment;
import com.foodCarrierDeliveryBoy.Models.LogoutModel;
import com.foodCarrierDeliveryBoy.Models.ModelLeftMenuDrawer;
import com.foodCarrierDeliveryBoy.Models.ModelOnlineOffline;
import com.foodCarrierDeliveryBoy.Models.ModelOrderLocDetail;
import com.foodCarrierDeliveryBoy.Models.ModelStatus;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.Network.GraphRequest;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Services.BackgroundLocationService;
import com.foodCarrierDeliveryBoy.Services.GetAssignOrderFirebase;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityDashboardBinding;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityDashboard extends BaseActivity implements FragmentOrderScreen.Callback,
        FragmentPickUpOrder.Callback, FragmentPickUpOrderItem.Callback, PopUpStackOrderDialog.Callback,
        DrawerItemCustomAdapter.Callback, GetAssignOrderFirebase {

    private static final String TAG = "ActivityDashboard";
    private ActivityDashboardBinding binding;
    Context context;
    int counter;
    int progress;
    CountDownTimer countDownTimer;
    private ProgressDialog progressDialog;
    private ArrayList<ModelLeftMenuDrawer> list = new ArrayList<>();
    private String backStateName;
    private static ActivityDashboard dashboard = null;
    private DrawerLayout mDrawerLayout;
    private String[] mNavigationDrawerItemTitles;
    private ListView mDrawerList;
    private LinearLayout drawerContainer;
    private boolean statusOnline = false;
    DrawerItemCustomAdapter adapter;
    private ImageView menuUImage;
    private int backCounter = 2;
    private String orderID;
    private DatabaseReference mDataBaseRef;
    TextView tvPhone, tvMerchantName, tvMerchantId;
    ImageView tvProfile;
    NotificationsBroadCast notificationsBroadCastReceiver;
    private int driverId;
    ModelOrderLocDetail modelOrder;
    public static RequestQueue requestQueue;
    private BackgroundLocationService service;
    public static GraphRequest graphRequest;
    Animation animSlideDown;
    private String notificationType;


    public static ActivityDashboard getDashboard() {
        return dashboard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        context = this;
        progressDialog = new ProgressDialog(this);

        dashboard = this;
        driverId = PrefManager.getInstance(context).getUserDetail().getUser().getId();
        Log.d(TAG, "onCreate_driverId " + driverId);
        loadHomeFragment();
        apiCheckIsOnline();
        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.viewDrawer);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        drawerContainer = (LinearLayout) findViewById(R.id.drawerContainer);
        menuUImage = (ImageView) findViewById(R.id.action_bar_date);
        binding.toolbarDash.tvOnlineBtn.setOnClickListener(this);
        binding.asdasdada.acceptButton.setOnClickListener(this);
        requestQueue = Volley.newRequestQueue(this);

        graphRequest = new GraphRequest(requestQueue, context);

        mDataBaseRef = FirebaseDatabase.getInstance(UrlFactory.getFireBaseUrl()).getReference()
                .child("Order_request_" + driverId);

        notificationsBroadCastReceiver = new NotificationsBroadCast();
        notificationsBroadCastReceiver.addListener(this);

        context.registerReceiver(notificationsBroadCastReceiver,
                new IntentFilter(AppConstants.FILTER_NOTIFICATION_BROADCAST));


        if (getIntent() != null) {
            orderID = getIntent().getStringExtra("order_id");
            notificationType = getIntent().getStringExtra("notification_type");
            if (notificationType != null && orderID != null) {
                apiHitOrderDetail(orderID);

            }
        }
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View headerView = inflater.inflate(R.layout.drawer_header, null, false);
        tvPhone = headerView.findViewById(R.id.viewPhone);
        ImageView img = (ImageView) headerView.findViewById(R.id.crossBtn);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(drawerContainer);
            }
        });

        tvMerchantName = headerView.findViewById(R.id.UserName);
        tvMerchantId = headerView.findViewById(R.id.viewMerchantId);
        tvProfile = headerView.findViewById(R.id.viewProfile);

        tvMerchantName.setText(ValidationHelper.optional(PrefManager.getInstance(context).getUserDetail().getUser().getName()));
        tvPhone.setText(ValidationHelper.optional(PrefManager.getInstance(context).getUserDetail().getUser().getContact()));
        tvMerchantId.setText(ValidationHelper.optional(String.valueOf(PrefManager.getInstance(context).getUserDetail().getUser().getId())));

        Glide.with(context).load(PrefManager.getInstance(context).getUserDetail().getUser().getImage())
                .apply(new RequestOptions().placeholder(R.drawable.edit_profile_icon)
                        .transforms(new CircleCrop(), new RoundedCorners(25))).into(tvProfile);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, ActivityEditProfile.class));

            }
        });
        mDrawerList.addHeaderView(headerView); //Add view to list view as header view
        binding.viewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOutDialog(context);
            }
        });
        registerActivityForGPS();
        registerActivityForInternet();
        registerActivityForNotification();
        bindService();
        loadHomeFragment();

        String title[] = {"Account Settings", "Earnings", "Orders History", "Wallet", "Mode of Transport", "Chareot News", "Help centre"};
        int icons[] = {R.drawable.app_settings, R.drawable.earning_icon, R.drawable.order_icon, R.drawable.wallet_icon1, R.drawable.mode_of_transport, R.drawable.notification_icon,
                R.drawable.help_centre};

        for (int i = 0; i < title.length; i++) {
            ModelLeftMenuDrawer modelLeftMenuDrawer = new ModelLeftMenuDrawer(title[i], icons[i], false);
            list.add(modelLeftMenuDrawer);
        }
        adapter = new DrawerItemCustomAdapter(this, list, this);
        mDrawerList.setAdapter(adapter);
        menuUImage.setOnClickListener(this);


        binding.asdasdada.buttonRight.setOnTouchListener(new OnSwipeTouchListener(context) {
            public void onSwipeRight() {
                //  Toast.makeText(context, "swipeRight", Toast.LENGTH_SHORT).show();
                hitAcceptOrder();

            }

            public void onSwipeLeft() {
                // Toast.makeText(context, "swipeLeft", Toast.LENGTH_SHORT).show();
                hitRejectOrder();


            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference().child("Order_Request_" + driverId);

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("ankitData", dataSnapshot.getKey());
                if (dataSnapshot.exists()) {

                    if (dataSnapshot.getKey().toLowerCase().contains("order_id")) {
                        String value = String.valueOf(dataSnapshot.getValue());
                        Log.d("ankitaaaa", "anki" + value);
                        apiHitOrderDetail(value);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("ankit", String.valueOf(PrefManager.getInstance(context).getUserDetail().getUser().getId()));
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("anknnnnn", String.valueOf(PrefManager.getInstance(context).getUserDetail().getUser().getId()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ankaaaaaaaaa", String.valueOf(PrefManager.getInstance(context).getUserDetail().getUser().getId()));
            }
        });
    }


    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.action_bar_date:
                toggleDrawer();
                break;

            case R.id.tvOnlineBtn:
                apiHitOnlineOffline();
                break;

            case R.id.accept__button:
                hitAcceptOrder();
                break;
        }


    }


    private void bindService() {
        Intent intent = new Intent(this, BackgroundLocationService.class);
        this.bindService(intent, locationServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unBindService() {
        try {
            unbindService(locationServiceConnection);
            locationServiceConnection = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unBindService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private ServiceConnection locationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder mService) {
            setCallback(mService);

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };


    private void setCallback(IBinder mService) {
        service = ((BackgroundLocationService.LocalBinder) mService).getService();
        service.setCallbacks(this);

    }

    private void animationScreenPopup() {
        animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        // Slide Down
        binding.asdasdada.layoutBg.setVisibility(View.GONE);
        binding.toolbarDash.tvOnlineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAG", "online ");
                //   new PopUpDialog(context, modelOrder, this,R.style.DialogSlide,"slide_up");
                binding.asdasdada.layoutBg.setVisibility(View.VISIBLE);
                binding.asdasdada.layoutBg.animate().translationYBy(binding.contentFrame.getMeasuredHeight()).setDuration(1000);
                // binding.contentFrame.setBackgroundColor(getResources().getColor(R.color.grey));
                binding.asdasdada.layoutBg.setOnClickListener(null);
                // countDounTimer();
                binding.viewDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }
        });


    }


    private void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(drawerContainer)) {
            mDrawerLayout.closeDrawer(drawerContainer);
        } else {
            mDrawerLayout.openDrawer(drawerContainer);
        }
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }

    public GraphRequest getBookingRequest() {
        return graphRequest;
    }

    @Override
    public void onClickonAcceptOrder(String fromScreen) {
        if (fromScreen.equals("popUpDialog")) {
            loadFragment(new FragmentOrderScreen(this, orderID));
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    @Override
    public void onClickonCancelOrder(String fromScreen) {
        //   loadFragment(new FragmentPickUpOrder(this,this));

    }

    @Override
    public void onClickonPickUpItem(String order_id) {
        binding.toolbarDash.toolbarNormal.setVisibility(View.GONE);
        binding.toolbarDash.largeToolbar.setVisibility(View.VISIBLE);
        loadFragment(new FragmentPickUpOrderItem(this, this, order_id));
    }

    @Override
    public void onClickOnAtRestaurant() {
        // if we click on particular restaurant
        loadFragment(new FragmentPickUpOrder(this, this, "2224"));

    }

    @Override
    public void onClickedOnPickUp(String order_id) {
        //finally pickup button
        binding.toolbarDash.toolbarNormal.setVisibility(View.VISIBLE);
        binding.toolbarDash.largeToolbar.setVisibility(View.GONE);
        loadFragment(new FragmentFoodDeliveredList(context, order_id));
    }


    /*/ just for testing

    @Override
    public void onDriverAssigned() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onDriverReached() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onRideStarted() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onRideCompleted(String totalCost) {
        new PopUpDialog(this, this);
    }

    @Override
    public void onRideCancelledByDriver() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onStatusUnknown() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onDriverLocationUpdated(LatLng driverCurrentLocation) {
        new PopUpDialog(this, this);
    }

    @Override
    public void onTrackError(String errorMessage) {
        new PopUpDialog(this, this);
    }

    @Override
    public void onReferEarn() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onRideTransferred() {
        new PopUpDialog(this, this);
    }

    @Override
    public void onRideExtended(String rideID) {
        new PopUpDialog(this, this);
    }*/

    @Override
    public void onClickonItemDrawer(int position, boolean isExpandable) {

        if (position == 0) {
            adapter.expandCollapse(position, isExpandable);
        } else {
            toggleDrawer();
            selectItem(position);
        }
    }

    @Override
    public void onClickonLeftMenuBankAccountSetting() {
        startActivity(IntentHelper.getBankAccountScreen(this));

    }

    @Override
    public void onClickonLeftMenuChangePasswordSetting() {
        startActivity(new Intent(ActivityDashboard.this, ChangePasswordActivity.class));

    }


    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {


            case 0:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 250);


                break;

            case 1:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(IntentHelper.getEarningScreen(context));
                    }
                }, 250);

                break;
            case 2:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(IntentHelper.getOrderHistoryScreen(context));
                    }
                }, 250);

                break;
            case 3:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(ActivityDashboard.this, ActivityAddMoney.class));
                    }
                }, 250);


                break;

            case 4:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        binding.commonToolbar.imgBackArrow.setVisibility(View.VISIBLE);
                        context.startActivity(new Intent(ActivityDashboard.this, ActivityModeOfTransport.class));
                    }
                }, 250);


                break;
            case 5:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.toolbarDash.tvOrder.setVisibility(View.VISIBLE);
                                binding.toolbarDash.tvOrder.setText("Chareot News");
                                loadFragment(new ActivityNotification());
                            }
                        }, 250);
                    }
                }, 250);

                break;


            case 6:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(IntentHelper.getHelpScreen(context));
                    }
                }, 250);

                break;
            case 7:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        logOutDialog(context);

                    }
                }, 225);
                break;


            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("ActivityHome", "Error in creating fragment");
        }

    }

    private void loadHomeFragment() {
        binding.toolbarDash.toolbarNormal.setVisibility(View.VISIBLE);
        binding.toolbarDash.largeToolbar.setVisibility(View.GONE);
        loadFragment(new HomeFragment());

    }


    private void orderDetail() {
        binding.asdasdada.userName.setText(ValidationHelper.optional(modelOrder.getBranch().getDelivery_time()));
        binding.asdasdada.estTIme.setText(ValidationHelper.optional(modelOrder.getBranch().getRestaurant_name()));
        binding.asdasdada.tvAddress.setText(ValidationHelper.optional(modelOrder.getBranch().getAddress()));
        binding.asdasdada.estDistance.setText(ValidationHelper.optional(modelOrder.getOrder().getDelivery_time()));
        binding.asdasdada.tvAddressDetail.setText(ValidationHelper.optional(modelOrder.getOrder().getAddress_type()));
        binding.asdasdada.estAddressName.setText(ValidationHelper.optional(modelOrder.getOrder().getBuilding()));
        binding.asdasdada.tvCost.setText(ValidationHelper.optional(String.valueOf(modelOrder.getOrder().getDelivery_amount())));
    }

    void apiHitOrderDetail(String orderId) {
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, orderId);
        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ORDER_LOC_DETAIL),
                UrlFactory.getAppHeaders(), this);
    }

    private void apiCheckIsOnline() {
        Authentication.get((AppConstants.URL_CHECK_ONLINE), this, false);
    }

    void apiHitOnlineOffline() {
        HashMap<String, String> map = new HashMap<>();
        if (modelOrder != null && String.valueOf(modelOrder.getOrder().getId()) != null &&
                !String.valueOf(modelOrder.getOrder().getId()).equalsIgnoreCase("")) {
            binding.toolbarDash.tvOnlineBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            ActivityDashboard.this);
                    builder.setTitle("Message Alert");
                    builder.setMessage("Please complete your running deliveries to go offline.");
                    builder.setPositiveButton("OK",

                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    //   Toast.makeText(getApplicationContext(),"Yes is clicked",Toast.LENGTH_LONG).show();
                                    // binding.asdasdada.layoutBg.animate().translationYBy(binding.contentFrame.getMeasuredHeight()).setDuration(1000);
                                    // loadFragment(new FragmentOrderScreen(context,orderID));
                                }
                            });
                    builder.show();
                }
            });
        } else {
            if (statusOnline) {
                map.put("status", "false");
                Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ONLINE_OFFLINE),
                        UrlFactory.getAppHeaders(), this);
            } else {
                map.put("status", "true");
                Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ONLINE_OFFLINE),
                        UrlFactory.getAppHeaders(), this);
            }
        }

    }


    void hitAcceptOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, String.valueOf(modelOrder.getOrder().getId()));
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_ACCEPT_ORDER),
                UrlFactory.getAppHeaders(), this);
    }

    void hitRejectOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.Order_id, String.valueOf(modelOrder.getOrder().getId()));
        Authentication.objectRequestRCSTwo(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_REJECT_ORDER),
                UrlFactory.getAppHeaders(), this);
    }
    void openDialog() {
        new PopUpStackOrderDialog(context, this);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        dismissLoader();
        if (responseObj instanceof ModelOnlineOffline) {
            ModelOnlineOffline modelOnlineOffline = (ModelOnlineOffline) responseObj;
            if (modelOnlineOffline.isIs_online()) {
                statusOnline = true;
                binding.toolbarDash.tvOnlineBtn.setImageDrawable(getResources().getDrawable(R.drawable.online_with_text));
                // animationScreenPopup();
                HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                homeFragment.changeScreen();

                Log.d("TAG", "Online ho gye ");

            } else {
                binding.toolbarDash.tvOnlineBtn.setImageDrawable(getResources().getDrawable(R.drawable.offline_with_text));
                HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                homeFragment.changeScreenOffline();
                Log.d("TAG", "Offline ho gye ");

            }
        } else if (responseObj instanceof ModelStatus) {
            ModelStatus modelStatus = (ModelStatus) responseObj;
            if (modelStatus.isIs_online()) {
                binding.toolbarDash.tvOnlineBtn.setImageDrawable(getResources().getDrawable(R.drawable.online_with_text));
                HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                homeFragment.changeScreen();
                if (modelOrder != null && String.valueOf(modelOrder.getOrder().getId()) != null &&
                        !String.valueOf(modelOrder.getOrder().getId()).equalsIgnoreCase("")){
                    loadFragment(new FragmentOrderScreen(this,orderID));
                }
            } else {
                loadHomeFragment();
                binding.toolbarDash.tvOnlineBtn.setImageDrawable(getResources().getDrawable(R.drawable.offline_with_text));
                HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                homeFragment.changeScreenOffline();
            }
        } else if (responseObj instanceof ModelOrderLocDetail) {
            modelOrder = (ModelOrderLocDetail) responseObj;

            Log.d(TAG, "onTaskSuccessAddress: " + modelOrder.getBranch().getAddress());
            if (modelOrder.getCode() == 200) {

                    this.orderID = String.valueOf(modelOrder.getOrder().getId());
                    binding.asdasdada.layoutBg.setVisibility(View.VISIBLE);
                    binding.asdasdada.layoutBg.animate().translationYBy(binding.contentFrame.getMeasuredHeight()).setDuration(1000);
                    binding.contentFrame.setBackgroundColor(getResources().getColor(R.color.grey));
                    binding.asdasdada.layoutBg.setOnClickListener(null);
                    //countDounTimer();
                    binding.viewDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    orderDetail();

            }
        } else if (responseObj instanceof LogoutModel) {
            LogoutModel Modelogout = (LogoutModel) responseObj;
            if (Modelogout.getCode() == 200) {
                PrefManager.getInstance(context).clearPrefs();
                startActivity(new Intent(ActivityDashboard.this, ActivityLogin.class));
                finishAffinity();
            }
        } else if (responseObj instanceof ModelSuccess) {
            ModelSuccess ModelAccept = (ModelSuccess) responseObj;
            if (ModelAccept.getCode() == 200) {
                //  Toast.makeText(context, "OrderAccepted", Toast.LENGTH_SHORT).show();
                binding.asdasdada.layoutBg.animate().translationYBy((binding.contentFrame.getMeasuredHeight()) -
                        (2 * (binding.contentFrame.getMeasuredHeight()))).setDuration(1000);
                binding.contentFrame.setBackgroundColor(getResources().getColor(R.color.white));
                loadFragment(new FragmentOrderScreen(this, orderID));

            }
        } else if (responseObj instanceof String) {
            // Toast.makeText(context, "OrderRejected", Toast.LENGTH_SHORT).show();
            binding.asdasdada.layoutBg.animate().translationYBy((binding.contentFrame.getMeasuredHeight()) -
                    (2 * (binding.contentFrame.getMeasuredHeight()))).setDuration(1000);
            binding.contentFrame.setBackgroundColor(getResources().getColor(R.color.white));

        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        dismissLoader();
        Toast.makeText(context, "" + errorMsg, Toast.LENGTH_SHORT).show();
    }

    private void logOutDialog(final Context context) {
        this.context = context;

        new iOSDialogBuilder(context)
                .setTitle(context.getString(R.string.app_name))
                .setSubtitle(getString(R.string.txt_want_to_logout))
                .setBoldPositiveLabel(false)
                .setCancelable(false)
                .setPositiveListener(getString(R.string.txt_ok), new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        dialog.dismiss();
                        Authentication.get(AppConstants.URL_LOGOUT, ActivityDashboard.this, false);
                    }
                })
                .setNegativeListener(getString(R.string.txt_cancel), new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        dialog.dismiss();
                    }
                })
                .build().show();
    }

    private void loadFragment(Fragment fragment) {
        backStateName = fragment.getClass().getSimpleName();
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(binding.contentFrame.getId(), fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {

        if ("ActivityNotification".equals(backStateName)) {
            binding.toolbarDash.tvOrder.setText("");

        }

        if ("FragmentPickUpOrderItem".equals(backStateName)) {
            binding.toolbarDash.toolbarNormal.setVisibility(View.VISIBLE);
            binding.toolbarDash.largeToolbar.setVisibility(View.GONE);
        }
        if ("FragmentFoodDeliveredList".equals(backStateName)) {
            binding.toolbarDash.toolbarNormal.setVisibility(View.GONE);
            binding.toolbarDash.largeToolbar.setVisibility(View.VISIBLE);
            getSupportFragmentManager().popBackStack();
            loadFragment(new FragmentPickUpOrderItem(this, this, String.valueOf(modelOrder.getOrder().getId())));
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            if (backCounter > 1) {
                backCounter--;
                Toast.makeText(this, "Press again to Exit", Toast.LENGTH_SHORT).show();
                startCounterToSetToDefault();

            } else {
                finishAffinity();
            }
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                backStateName = fragment.getClass().getSimpleName();
            }
        }
    }

    private void startCounterToSetToDefault() {
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                backCounter = 2;
            }
        }.start();
    }

    @Override
    public void onOrderCreated(String orderID, String title) {

    }

    @Override
    public void onOrderMerchantCancel(String orderID, String title) {

    }

    @Override
    public void onOrderMerchantAccept(String orderID, String title) {

    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    public boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void getAssignOrderFirebase(String orderID) {
        if (isApplicationSentToBackground(context)) {
            if (!orderID.equals("")) {
                Intent homeIntent = new Intent(context, ActivityDashboard.class);
                homeIntent.putExtra("order_id", orderID);
                homeIntent.putExtra("notification_type", "transporter_assigned");
                homeIntent.setAction(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivity(homeIntent);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService();
        this.registerReceiver(notificationsBroadCastReceiver,
                new IntentFilter(AppConstants.FILTER_NOTIFICATION_BROADCAST));

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            this.unregisterReceiver(notificationsBroadCastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClickOnAcceptButton() {

    }

    /*
       private void showImage() {
          orderDetail();

           binding.asdasdada.acceptButton.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {

                   //   if (callback != null) { }
                   // callback.onClickonAcceptOrder("popUpDialog");
                   hitAcceptOrder();
                   dialog.cancel();


               }
           });
           binding.rejectButton.setOnStateChangeListener(new OnStateChangeListener() {
               @Override
               public void onStateChange(boolean active) {
                   if (active) {
                       callback.onClickonRejectOrder("home");
                       dialog.cancel();
                   }
               }
           });
           // binding.progressbar.setMaxProgress(20);
           countDounTimer();
       }
   */

   /* void countDounTimer() {
        counter = 20;
        progress = 0;
        countDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                    if (progress<21){
                        progress = progress + 1;
                        binding.asdasdada.progressTimeTV.setText(counter + "s Left to Accept");
                        binding.asdasdada.progressbar.setProgress(progress);
                        counter--;
                    }
            }

            @Override
            public void onFinish() {
              binding.tvButtonResendOtp.setText(getResources().getString(R.string.send_otp));
                binding.otpCountDownTimer.setVisibility(View.GONE);
                binding.tvButtonResendOtp.setVisibility(View.VISIBLE);


                  binding.asdasdada.layoutBg.animate().translationYBy(binding.contentFrame.getMeasuredHeight() -
                        (2 * (binding.contentFrame.getMeasuredHeight()))).setDuration(2000);
                binding.contentFrame.setBackgroundColor(getResources().getColor(R.color.white));
                loadFragment(new HomeFragment());
            }

        }.start();
    }*/

    class OnSwipeTouchListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener(Context ctx) {
            gestureDetector = new GestureDetector(ctx, new OnSwipeTouchListener.GestureListener());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 200;
            private static final int SWIPE_VELOCITY_THRESHOLD = 700;


            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                            result = true;
                        }
                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        result = true;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }

        public void onSwipeRight() {

        }

        public void onSwipeLeft() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }
    }
}