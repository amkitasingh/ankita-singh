package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.databinding.ActivityUnsuccessfulPaymentBinding;

public class ActivityUnsuccessfulPayment extends BaseActivity {
    private ActivityUnsuccessfulPaymentBinding binding;
    private Context context;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_unsuccessful_payment);
        context=this;
        progressDialog=new ProgressDialog(this);

        binding.viewTry.setOnClickListener(this);

    }

    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()){

            case R.id.viewTry:
                context.startActivity(new Intent(this,ActivityBankAccountDetail.class));
                break;
        }

    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
