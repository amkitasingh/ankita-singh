package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.foodCarrierDeliveryBoy.Adapters.VehicleCategoryAdapter;
import com.foodCarrierDeliveryBoy.Models.ModelAllModes;
import com.foodCarrierDeliveryBoy.Models.TransportCreatedModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;

import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityAddTransportDetailBinding;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class ActivityAddTransportDetail extends BaseActivity implements AdapterView.OnItemSelectedListener {
    private ActivityAddTransportDetailBinding binding;
    private Context context;
    String selectedDate;
    ProgressDialog dialog;
    ArrayList<ModelAllModes.VehiclesBean> vehicleCategoryArrayList =new ArrayList<>();
    private String pathOne = null;
    String categoryName, categoryId;
    private String pathTwo = null;
    private Uri image_uris;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_transport_detail);
        context = this;
        dialog = new ProgressDialog(this);
        toolbarSetting();
        apiHitAllModes();
        binding.spinnerType.setOnItemSelectedListener(this);
        binding.viewSubmitBtn.setOnClickListener(this);
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Add Basic Details");



        binding.frontCons.setOnClickListener(this);
        binding.frontCons1.setOnClickListener(this);
        binding.viewExpiryDate.setOnClickListener(this);
        binding.viewExpiryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    calender();
                }
            }
        });

    }

    private void apiHitAllModes() {
        Authentication.get((AppConstants.URL_ALL_MODES),this,false);
    }


    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.viewSubmitBtn:
                if (isValid()) {
                    if (  binding.btnFrontImage != null &&  binding.btnBackImage != null) {

                        Authentication.addCar(this, UrlFactory.generateUrlWithVersion(AppConstants
                                .URL_ADD_VEHICLE), categoryId, binding.viewVehicleMaker.getText().toString(), binding.viewVehicleRegisText
                                .getText().toString(), binding.viewVehicleDLNo.getText().toString(), binding.viewExpiryDate
                                .getText().toString(), pathOne, pathTwo, this);

                    } else {
                        Toast.makeText(context, "Please change image and required details", Toast.LENGTH_SHORT).show();
                    }
                    /* context.startActivity(new Intent(this, com.chareotdelivery.Activity.ActivityRechargeWallet.class));*/
                }

                break;
            case R.id.frontCons:
                binding.btnFrontImage.setSelected(true);
                binding.btnBackImage.setSelected(false);
                captureImage();
                break;

            case R.id.frontCons1:
                binding.btnFrontImage.setSelected(false);
                binding.btnBackImage.setSelected(true);
                captureImage();
                break;

            case R.id.imgBackArrow:
                finish();
                break;

        }
    }

    boolean isValid(){
        if (ValidationHelper.isNull(binding.viewVehicleMaker.getText().toString())){
            Toast.makeText(this,this.getResources().getText(R.string.enter_vehicle_maker),Toast.LENGTH_SHORT).show();
            return  false;
        }else   if (ValidationHelper.isNull(binding.viewVehicleRegisText.getText().toString())){
            Toast.makeText(this,this.getResources().getText(R.string.enter_registration_number),Toast.LENGTH_SHORT).show();
            return  false;
        }
        else   if (ValidationHelper.isNull(binding.viewVehicleDLNo.getText().toString())){
            Toast.makeText(this,this.getResources().getText(R.string.enter_dl_no),Toast.LENGTH_SHORT).show();
            return  false;
        }
        else   if (ValidationHelper.isNull(binding.viewExpiryDate.getText().toString())){
            Toast.makeText(this,this.getResources().getText(R.string.enter_expiry_date),Toast.LENGTH_SHORT).show();
            return  false;
        }
        else   if (ValidationHelper.isNull(binding.btnFrontImage.getText().toString())){
            Toast.makeText(this,this.getResources().getText(R.string.select_image),Toast.LENGTH_SHORT).show();
            return  false;
        }
        else   if (ValidationHelper.isNull(binding.btnBackImage.getText().toString())){
            Toast.makeText(this,this.getResources().getText(R.string.select_image),Toast.LENGTH_SHORT).show();
            return  false;
        }
        return  true;
    }

    private void toolbarSetting() {

        binding.toolbar.imgBackArrow.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Add Basic Details");
        binding.toolbar.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {}

    private void captureImage() {
        TedPermission.with(ActivityAddTransportDetail.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        CropImage.activity(image_uris)
                                //.setGuidelines(CropImageView.Guidelines.ON).setCropShape(CropImageView.CropShape.OVAL)
                                .start(ActivityAddTransportDetail.this);
                    }
                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        finish();
                    }
                })
                .setDeniedMessage("denied")
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {

                    if (binding.btnFrontImage.isSelected()){
                        pathOne = result.getUri().getPath();

                        Glide.with(ActivityAddTransportDetail.this)
                                .load(pathOne)
                                .apply(new RequestOptions()
                                        .transform(new RoundedCorners(10)).placeholder(R.drawable.logo_icon))
                                .into(binding.imgRightF);
                    }else {
                        pathTwo = result.getUri().getPath();
                        Glide.with(ActivityAddTransportDetail.this)
                                .load(pathTwo)
                                .apply(new RequestOptions()
                                        .transform(new RoundedCorners(10)).placeholder(R.drawable.logo_icon))
                                .into(binding.imgRightB);
                    }

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void calender() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int mYear,mMonth,mDay;
        mYear = year;
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        Calendar minAdultAge = new GregorianCalendar();
                        minAdultAge.add(Calendar.YEAR, 0);
                        selectedDate = DateFormat.format("yyyy-MM-dd", c.getTimeInMillis()).toString();
                        binding.viewExpiryDate.setText(DateFormat.format("dd-MM-yyyy", c.getTimeInMillis()).toString());


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void onNeedToSendSignUp(){}

    @Override
    public void onSessionExpire() {}

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof ModelAllModes){
            ModelAllModes modelVehicleCategory= (ModelAllModes) responseObj;
            vehicleCategoryArrayList.addAll(modelVehicleCategory.getVehicles())  ;
            VehicleCategoryAdapter vehicleCategoryAdapter = new VehicleCategoryAdapter
                    (this, android.R.layout.simple_spinner_dropdown_item, vehicleCategoryArrayList);
            binding.spinnerType.setAdapter(vehicleCategoryAdapter);
            binding.spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    ModelAllModes.VehiclesBean model = (ModelAllModes.VehiclesBean)adapterView.getItemAtPosition(position);
                    categoryName=model.getName();
                    categoryId=String.valueOf(model.getId());
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapter) {  }
            });
        }
        if (responseObj instanceof TransportCreatedModel){
            Toast.makeText(this,this.getResources().getText(R.string.successfully_added_vehicle),Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
    }
}
