package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Models.UserModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.PrefManager;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityEditProfileBinding;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.List;

import id.zelory.compressor.Compressor;

public class ActivityEditProfile extends BaseActivity implements PermissionListener {
    ActivityEditProfileBinding binding;
    private Context context;
    private Uri resultUri;
    String firstName,lastName;
    private String imagePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        context = this;
        binding.toolbar.imgBackArrow.setOnClickListener(this);
        binding.viewUpdateBtn.setOnClickListener(this);
        binding.viewProfileImage.setOnClickListener(this);
        toolbarSetting();
        setUserDetails(PrefManager.getInstance(context).getUserDetail());
    }


    @Override
    public void onClick(int viewId, View view) {
        switch (view.getId()) {
            case R.id.viewProfileImage:
                TedPermission.with(this)
                        .setPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .setPermissionListener(this)
                        .check();
                break;
            case R.id.viewUpdateBtn:
                if (validation(view)) {
                    apiEditProfile();
                }
                break;

            case R.id.imgBackArrow:
                finish();
                break;


        }

    }

    private void apiEditProfile() {
        String driverId= String.valueOf(binding.viewMerchantId.getId());
        firstName = binding.lstName.getText().toString();
        lastName = binding.frtName.getText().toString();
        String Email=binding.viewEmail.getText().toString();
        String mobile = binding.viewMobile.getText().toString();
        String url = AppConstants.URL_UPDATE_PROFILE;
        Authentication.updateUserDetails(url,  firstName, lastName,Email, imagePath, this);
    }


    private  void fullName( String firstName, String lastName) {
    String fullName = "";
    int idx = fullName.lastIndexOf(' ');
    if (idx == -1)
        throw new IllegalArgumentException("Only a single name: " + fullName);
    firstName = fullName.substring(0, idx);
    lastName = fullName.substring(idx + 1);

    }

    private boolean validation(View view) {
        if (!ValidationHelper.isNull(binding.lstName.getText().toString())) {
            return true;
        } else {
            Toast.makeText(context, R.string.txt_enter_user_name, Toast.LENGTH_SHORT).show();
        }
        if (!ValidationHelper.isNull(binding.lstName.getText().toString())) {
            return true;
        } else {
            Toast.makeText(context, R.string.txt_enter_user_name, Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof UserModel) {
            UserModel model = (UserModel) responseObj;
            PrefManager.getInstance(context).setUserDetail(model);
            finish();

        }


    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
     if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                if (resultUri != null) {
                    try {
                        imagePath = resultUri.getPath();
                        try {
                            imagePath = String.valueOf(new Compressor(this).compressToFile(
                                    new File(resultUri.getPath())));
                            Glide.with(context)
                                    .load(imagePath)
                                    .apply(new RequestOptions()
                                            .transform(new RoundedCorners(20))
                                            .placeholder(R.drawable.edit_profile_icon))
                                    .into(binding.viewProfileImage);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    private void setUserDetails(UserModel model) {
        binding.lstName.setText(ValidationHelper.optional(model.getUser().getName()));

        String fullname =PrefManager.getInstance(context).getUserDetail().getUser().getName();
        String[] fname = fullname.split(" ");
        if (fname.length==1){
            Toast.makeText(this,fname[0],Toast.LENGTH_LONG).show();
            binding.lstName.setText(fname[0]);
        }else {
            binding.lstName.setText(fname[0]);
            binding.frtName.setText(fname[1]);
        }

        binding.viewEmail.setText(ValidationHelper.optional(model.getUser().getEmail()));
        binding.viewMerchantId.setText(ValidationHelper.optional(String.valueOf(model.getUser().getId())));
        binding.viewMobile.setText(ValidationHelper.optional(model.getUser().getContact()));


        Glide.with(this).load(model.getUser().getImage())
                .apply(new RequestOptions().placeholder(R.drawable.edit_profile_icon)
                        .transforms(new CenterCrop(), new RoundedCorners(20))).into(binding.viewProfileImage);


    }

    private void toolbarSetting() {
        binding.toolbar.tvLayoutName.setVisibility(View.VISIBLE);
        binding.toolbar.tvLayoutName.setText("Edit Details");
    }

    @Override
    public void onResume() {
        super.onResume();

        setUserDetails(PrefManager.getInstance(this).getUserDetail());
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onPermissionGranted() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onPermissionDenied(List<String> deniedPermissions) {

    }

}