package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.ModelSuccess;
import com.foodCarrierDeliveryBoy.Models.PasswordTokenModel;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.Logger;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityResetPasswordBinding;

import java.util.HashMap;

import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.key_input_role;

public class ActivityResetPassword extends BaseActivity implements View.OnClickListener {

    private ActivityResetPasswordBinding binding;
    private Context context;
    private ProgressDialog dialog;
    private String contactNum,countrtCode;
    private String passwordToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        context = this;
        dialog = new ProgressDialog(this);
        binding.viewResetBtn.setOnClickListener(this);
        if (getIntent() !=null){
            contactNum = getIntent().getStringExtra("contactNo");
            countrtCode = getIntent().getStringExtra("countryCode");
            Toast.makeText(context, ""+countrtCode+"\n"+contactNum, Toast.LENGTH_SHORT).show();
        }
        binding.toolbar.imgBackArrow.setOnClickListener(this);
binding.etPassword.setFocusableInTouchMode(true);
binding.etPassword.requestFocus();
        hitApiForTokenReset();


    }

    private void hitApiForTokenReset() {
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("country_code", countrtCode);
        map.put("contact",contactNum);
        map.put(AppConstants.key_role,  key_input_role);


        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_FORGET_PASSWORD_TOKEN),
                UrlFactory.getDefaultHeaders(), this);

    }

    @Override
    public void onClick(int viewId, View v) {
        switch (v.getId()) {
            case R.id.viewResetBtn:
                if (ValidationHelper.isValidPassword(binding.etPassword.getText().toString())) {
                    if (ValidationHelper.isValidPassword(binding.etPassword1.getText().toString())) {
                        if (binding.etPassword.getText().toString().equals(binding.etPassword1.getText().toString())) {
                            apiSetPassword();
                        } else {
                            Logger.showSneckbar(v, getString(R.string.txt_old_pass_and_confirm_not_match));
                        }
                    } else {
                        Logger.showSneckbar(v, getString(R.string.txt_confirm_password_notnull));
                    }
                } else {
                    Logger.showSneckbar(v, getString(R.string.txt_new_pass_notnull));
                }
                break;
            case R.id.imgBackArrow:
                finish();
                break;
        }

    }



    private void apiSetPassword() {
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("password_token", passwordToken);
        map.put(AppConstants.key_role, key_input_role);
        map.put("password",binding.etPassword.getText().toString().trim());

        Authentication.objectRequestRCS(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_PASSWORD_RESET),
                UrlFactory.getDefaultHeaders(), this);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (responseObj instanceof ModelSuccess) {
            ModelSuccess model = (ModelSuccess) responseObj;
            Toast.makeText(context, "Your Password has been recovered, please login", Toast.LENGTH_SHORT).show();
            //new PrefManager(context).setUserLogin(false);
            startActivity(new Intent(ActivityResetPassword.this, ActivityLogin.class));
            finish();
        }else if (responseObj instanceof PasswordTokenModel){
            PasswordTokenModel passwordTokenModel =(PasswordTokenModel)responseObj;
            passwordToken = passwordTokenModel.getPassword_token();
            Toast.makeText(context, ""+passwordTokenModel.getPassword_token(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
