package com.foodCarrierDeliveryBoy.Activity;

import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.foodCarrierDeliveryBoy.Base.BaseActivity;
import com.foodCarrierDeliveryBoy.Dialogs.ProgressDialog;
import com.foodCarrierDeliveryBoy.Models.ModelContactExist;
import com.foodCarrierDeliveryBoy.Network.Authentication;
import com.foodCarrierDeliveryBoy.R;
import com.foodCarrierDeliveryBoy.Utilities.AppConstants;
import com.foodCarrierDeliveryBoy.Utilities.IntentHelper;
import com.foodCarrierDeliveryBoy.Utilities.UrlFactory;
import com.foodCarrierDeliveryBoy.Utilities.ValidationHelper;
import com.foodCarrierDeliveryBoy.databinding.ActivityGoogleVerificationBinding;

import java.util.HashMap;

import static com.foodCarrierDeliveryBoy.Utilities.AppConstants.key_input_role;

public class ActivityGoogleVerification extends BaseActivity {
    private ActivityGoogleVerificationBinding binding;
    private Context context;
    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_google_verification);
        context = this;
        dialog = new ProgressDialog(this);


        binding.ccp.registerPhoneNumberTextView(binding.viewMobile);

        binding.viewLoginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(int viewId, View v) {
        switch (v.getId()) {
            case R.id.viewLoginBtn:
                if (validation(v)) {
                    //hitSignUpApi();
                    hitCheckContactApi();
                }
                break;
        }
    }

    private void hitCheckContactApi() {
        if (!isFinishing() && !dialog.isShowing()) {
            dialog.showDialog(ProgressDialog.DIALOG_CENTERED);
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(AppConstants.key_contact, binding.viewMobile.getText().toString());
        map.put(AppConstants.key_country_code, binding.ccp.getSelectedCountryCodeWithPlus());
        map.put(AppConstants.key_role, key_input_role);
        Authentication.objectRequestUserExist(context, map, UrlFactory.generateUrlWithVersion(AppConstants.URL_CONTACT_EXIST),
                UrlFactory.getDefaultHeaders(), this);
    }

    @Override
    public void onTaskSuccess(Object responseObj) {
        super.onTaskSuccess(responseObj);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (responseObj instanceof ModelContactExist){
            ModelContactExist model = (ModelContactExist) responseObj;
            if (model.getCode() == 404){
                startActivity(IntentHelper.getModeTransport(context)
                        .putExtra(AppConstants.key_contact, binding.viewMobile.getText().toString())
                        .putExtra(AppConstants.key_country_code, binding.ccp.getSelectedCountryCodeWithPlus()));
            }else {
                Toast.makeText(context, model.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }

    }



    @Override
    public void onTaskError(String errorMsg) {
        super.onTaskError(errorMsg);
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    public boolean validation(View view) {
        if (ValidationHelper.isValidNumber(binding.viewMobile.getText().toString().trim())) {
            ValidationHelper.showSnackBar(view, "Please Enter Mobile Number");
            binding.viewMobile.requestFocus();
            return false;
        } else if (!binding.ccp.isValid()) {
            ValidationHelper.showSnackBar(view, "Enter valid mobile number");
            return false;
        }


        return true;
    }

    @Override
    public void onNeedToSendSignUp() {

    }

    @Override
    public void onSessionExpire() {

    }
}
